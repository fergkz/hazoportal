-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.5.32-0ubuntu0.13.04.1 - (Ubuntu)
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_autor
CREATE TABLE IF NOT EXISTS `pag_autor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `pessoa_id` int(10) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `pessoa_id_UNIQUE` (`pessoa_id`),
  KEY `fk_pag_autor_hazo_user1_idx` (`user_id`),
  KEY `fk_pag_autor_pes_pessoa1_idx` (`pessoa_id`),
  CONSTRAINT `fk_pag_autor_hazo_user1` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_autor_pes_pessoa1` FOREIGN KEY (`pessoa_id`) REFERENCES `pes_pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_autor: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_autor` DISABLE KEYS */;
REPLACE INTO `pag_autor` (`id`, `user_id`, `pessoa_id`, `status`) VALUES
	(1, 1534, 652, 'A'),
	(2, 1538, 640, 'I'),
	(3, 3, 650, 'A');
/*!40000 ALTER TABLE `pag_autor` ENABLE KEYS */;


-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_autor_editoria
CREATE TABLE IF NOT EXISTS `pag_autor_editoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `editoria_id` int(11) NOT NULL,
  `autor_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pag_autor_editoria_pag_editoria1_idx` (`editoria_id`),
  KEY `fk_pag_autor_editoria_pag_autor1_idx` (`autor_id`),
  CONSTRAINT `fk_pag_autor_editoria_pag_autor1` FOREIGN KEY (`autor_id`) REFERENCES `pag_autor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_autor_editoria_pag_editoria1` FOREIGN KEY (`editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_autor_editoria: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_autor_editoria` DISABLE KEYS */;
REPLACE INTO `pag_autor_editoria` (`id`, `editoria_id`, `autor_id`) VALUES
	(8, 14, 1),
	(9, 18, 1);
/*!40000 ALTER TABLE `pag_autor_editoria` ENABLE KEYS */;


-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_editoria
CREATE TABLE IF NOT EXISTS `pag_editoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'R',
  `parent_editoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_pag_editoria_parent` (`parent_editoria_id`),
  CONSTRAINT `fk_pag_editoria_pag_editoria1` FOREIGN KEY (`parent_editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_editoria: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_editoria` DISABLE KEYS */;
REPLACE INTO `pag_editoria` (`id`, `nome`, `slug`, `status`, `parent_editoria_id`) VALUES
	(10, 'Ciência', 'ciencia', 'A', NULL),
	(11, 'Física', 'fisica', 'A', NULL),
	(12, 'Matemática', 'matematica', 'A', NULL),
	(13, 'Astronomia', 'astronomia', 'A', 10),
	(14, 'Cometas', 'cometas', 'A', 13),
	(15, 'Economia', 'economia', 'A', NULL),
	(16, 'Política', 'politica', 'R', NULL),
	(17, 'Exemplos', 'exemplos', 'A', NULL),
	(18, 'Blog', 'blog', 'A', 17);
/*!40000 ALTER TABLE `pag_editoria` ENABLE KEYS */;


-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_pagina
CREATE TABLE IF NOT EXISTS `pag_pagina` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `pub_data_inicio` datetime NOT NULL,
  `pub_data_final` datetime DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'R',
  `cad_data` datetime NOT NULL,
  `cad_user_id` int(10) NOT NULL,
  `mod_data` datetime DEFAULT NULL,
  `mod_user_id` int(10) DEFAULT NULL,
  `parent_pagina_id` int(10) unsigned DEFAULT NULL,
  `editoria_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `conteudo_json` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_pag_pagina_slug` (`slug`),
  UNIQUE KEY `uk_pag_pagina_titulo` (`titulo`),
  KEY `fk_pag_pagina_hazo_user1_idx` (`cad_user_id`),
  KEY `fk_pag_pagina_hazo_user2_idx` (`mod_user_id`),
  KEY `fk_pag_pagina_pag_pagina1_idx` (`parent_pagina_id`),
  KEY `fk_pag_pagina_pag_editoria1_idx` (`editoria_id`),
  KEY `fk_pag_pagina_pag_template1_idx` (`template_id`),
  CONSTRAINT `fk_pag_pagina_hazo_user1` FOREIGN KEY (`cad_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_hazo_user2` FOREIGN KEY (`mod_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_editoria1` FOREIGN KEY (`editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_pagina1` FOREIGN KEY (`parent_pagina_id`) REFERENCES `pag_pagina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_template1` FOREIGN KEY (`template_id`) REFERENCES `pag_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_pagina: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_pagina` DISABLE KEYS */;
REPLACE INTO `pag_pagina` (`id`, `titulo`, `slug`, `pub_data_inicio`, `pub_data_final`, `status`, `cad_data`, `cad_user_id`, `mod_data`, `mod_user_id`, `parent_pagina_id`, `editoria_id`, `template_id`, `conteudo_json`) VALUES
	(25, 'Queixa na web não melhora serviços', 'queixa-na-web-nao-melhora-servicos', '2013-01-01 00:00:00', NULL, 'A', '2013-08-19 09:21:43', 3, NULL, NULL, NULL, 16, 1, '{"titulo":"<p>Queixa na web n&atilde;o melhora servi&ccedil;os<\\/p>\\r\\n","data_left":"<p>Quem reclama na internet pode at&eacute; resolver seu problema. Mas, se n&atilde;o levar o caso a &oacute;rg&atilde;os oficiais, perde a chance de educar a empresa<\\/p>\\r\\n","data_left2":"<p style=\\"margin-right:10px\\"><strong>Retalia&ccedil;&atilde;o - Ap&oacute;s reclama&ccedil;&atilde;o, empresa &ldquo;bloqueia&rdquo; consumidora<\\/strong><\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\">A estudante de Direito Viviane Beatriz Costa, 27 anos, obteve dois resultados opostos ao usar o Facebook para resolver pendengas com empresas. Em uma situa&ccedil;&atilde;o, consultou a f&aacute;brica de sorvetes Paviloche enquanto procurava um copo promocional que faltava para sua cole&ccedil;&atilde;o. Recebeu um convite para conhecer a empresa e a promessa de que receberia o item de brinde.<\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\">Mais recentemente, acabou bloqueada pelo administrador da fanpage do Spa das Sobrancelhas ap&oacute;s reclamar do servi&ccedil;o de depila&ccedil;&atilde;o da franquia curitibana da rede. &ldquo;Fiquei chateada porque reclamei tranquilamente. N&atilde;o fui grossa, n&atilde;o falei palavra feia, apenas reclamei que paguei R$ 25 por um servi&ccedil;o malfeito&rdquo;, conta ela. O s&oacute;cio do sal&atilde;o, Fernando Oliveira, afirma que prefere que clientes fa&ccedil;am contato privado com a empresa.<\\/p>\\r\\n","data_right":"<p style=\\"margin-right:10px\\"><span style=\\"font-family:georgia,serif\\">Quem busca um canal para reclamar direitos tem&nbsp;na internet uma forma e tanto de&nbsp;constranger empresas e marcas. O Reclame Aqui, um dos sites voltados a divulgar queixas de consumidores no Brasil, recebe cerca de 10 mil casos por dia, dos quais 300 envolvem moradores do Paran&aacute;, segundo dados de julho. Assim como as redes sociais, os sites se mostram uma pr&aacute;tica ferramenta para resolver problemas urgentes.<\\/span><\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\"><span style=\\"font-family:georgia,serif\\">Mas o que beneficia o indiv&iacute;duo pode se tornar um problema coletivo, dizem especialistas da &aacute;rea. Isso porque apenas reclama&ccedil;&otilde;es levadas a &oacute;rg&atilde;os oficiais &ndash; como Procon, ag&ecirc;ncias reguladoras e Vigil&acirc;ncia Sanit&aacute;ria &ndash; contribuem para fiscalizar e punir empresas que desrespeitam clientes. Ao deixar de informar o caso a um &oacute;rg&atilde;o oficial, o consumidor perde oportunidade de direcionar a fiscaliza&ccedil;&atilde;o e de educar empresas que escorregam.<\\/span><\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\"><span style=\\"font-family:georgia,serif\\">&ldquo;O Procon, por exemplo, &eacute; eficiente, mas n&atilde;o para interesses individuais. Com as reclama&ccedil;&otilde;es que recebe, o &oacute;rg&atilde;o pode tomar medidas administrativas em defesa da sociedade. Mas n&atilde;o tem poder coercitivo para obrigar que o mau fornecedor atenda &agrave; expectativa de um consumidor prejudicado&rdquo;, avalia o advogado Sandro Mansuri Gibran. Ou seja: o ideal &eacute; buscar as duas vias.<\\/span><\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\"><span style=\\"font-family:georgia,serif\\">No auge do Twitter, em 2011, o site Folha.com calculou que reclamar pelo microblog era 8,4 mil vezes mais eficaz dos que procurar um Procon. Por enquanto, dados do tipo n&atilde;o derrubaram o n&uacute;mero de atendimentos do servi&ccedil;o paranaense, que cresceu 18% em orienta&ccedil;&otilde;es e processos abertos na compara&ccedil;&atilde;o entre 2011 e 2012. O temor de que a internet seja um substituto das reclama&ccedil;&otilde;es oficiais, no entanto, j&aacute; mobilizou os &oacute;rg&atilde;os estaduais. &ldquo;Consumidor, reclamar nas redes sociais sem oficializar a reclama&ccedil;&atilde;o ajuda, mas n&atilde;o resolve&rdquo;, j&aacute; escreveu o Procon do Acre em sua timeline no Facebook.<\\/span><\\/p>\\r\\n\\r\\n<p style=\\"margin-right:10px\\"><span style=\\"font-family:georgia,serif\\">&Eacute; poss&iacute;vel ver vantagem nessa concorr&ecirc;ncia? O advogado Jean Carlos de Albuquerque Gomes vislumbra uma oportunidade para os servi&ccedil;os de defesa ao consumidor serem mais incisivos. &ldquo;Acredito que deveria haver mais rigor nas aplica&ccedil;&otilde;es das multas e nas comunica&ccedil;&otilde;es realizadas ao Minist&eacute;rio P&uacute;blico, que atua para o coletivo&rdquo;, diz.<\\/span><\\/p>\\r\\n"}'),
	(26, 'teste conteudo titulo 2', 'slug-01-teste-02', '2015-05-05 00:00:00', NULL, 'A', '2013-08-15 16:36:06', 3, NULL, NULL, NULL, NULL, NULL, '[]'),
	(31, 'teste conteudo titulo 3', 'slug-01-teste-03', '2013-08-15 16:06:47', NULL, 'A', '2013-08-15 16:06:47', 3, NULL, NULL, NULL, NULL, NULL, '[]'),
	(32, 'Exemplo', 'exemplo', '2013-05-01 00:00:00', NULL, 'A', '2013-08-19 09:15:27', 3, NULL, NULL, NULL, 14, 1, '{"titulo":"","data_left":"","data_right":""}'),
	(33, 'Maycow', 'maycow', '2013-08-16 17:36:00', NULL, 'A', '2013-08-19 09:14:49', 3, NULL, NULL, NULL, 14, 1, '{"titulo":"CONTENT SAMPLES","data_left":"<p style=\\"text-align: justify;\\"><span style=\\"font-family:georgia,serif\\"><span style=\\"background-color:rgb(255, 255, 255); color:rgb(34, 34, 34); font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel l<\\/span><\\/span><\\/p>\\r\\n","data_left2":"<p style=\\"text-align:justify\\"><span style=\\"font-size:12px\\"><span style=\\"font-family:georgia,serif\\"><span style=\\"background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel l<\\/span><\\/span><\\/span><\\/p>\\r\\n","data_right":"<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra l&aacute; , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. M&eacute; faiz elementum girarzis, nisi eros vermeio, in elementis m&eacute; pra quem &eacute; amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p><span style=\\"font-family:georgia,serif\\"><span style=\\"font-size:12px\\">Suco de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.<\\/span><\\/span><\\/p>\\r\\n\\r\\n<p>&nbsp;<\\/p>\\r\\n"}'),
	(34, 'Depoimentos', 'depoimentos', '2013-01-01 00:00:00', NULL, 'A', '2013-08-19 09:58:22', 3, NULL, NULL, NULL, NULL, NULL, '[]'),
	(35, 'noticias', 'noticias', '2013-01-01 00:00:00', NULL, 'A', '2013-08-19 11:45:53', 3, NULL, NULL, NULL, NULL, NULL, '[]');
/*!40000 ALTER TABLE `pag_pagina` ENABLE KEYS */;


-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_pagina_bloco
CREATE TABLE IF NOT EXISTS `pag_pagina_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequence` int(11) DEFAULT NULL,
  `pagina_id` int(10) unsigned NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `titulo` varchar(150) NOT NULL,
  `conteudo_json` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pag_pagina_bloco_pag_pagina1_idx` (`pagina_id`),
  CONSTRAINT `fk_pag_pagina_bloco_pag_pagina1` FOREIGN KEY (`pagina_id`) REFERENCES `pag_pagina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_pagina_bloco: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_pagina_bloco` DISABLE KEYS */;
REPLACE INTO `pag_pagina_bloco` (`id`, `sequence`, `pagina_id`, `data_cadastro`, `status`, `titulo`, `conteudo_json`) VALUES
	(2, NULL, 34, '2013-08-19 11:16:48', 'A', 'ELOIR BELLORINI', '{"descricao":"\\"O site DiplomaNaWeb d\\u00e1 mais credibilidade aos diplomas e certificados dos candidatos recrutados por nossa empresa. Antes n\\u00f3s t\\u00ednhamos que acreditar no que o candidato colocava no curr\\u00edculo. Agora \\u00e9 poss\\u00edvel averiguar a informa\\u00e7\\u00e3o direto com a institui\\u00e7\\u00e3o de ensino por meio do DiplomaNaWeb.\\"","imagem_url":"\\/media\\/files\\/depoimentos\\/1376933290.png","autor":"Gerente de Neg\\u00f3cios do grupo DSR Intelig\\u00eancia Log\\u00edstica","descricao_autor":"Gerente de Neg\\u00f3cios do grupo DSR Intelig\\u00eancia Log\\u00edstica"}'),
	(3, NULL, 35, '2013-08-19 11:48:02', 'A', 'SITES VENDEM DIPLOMAS FALSOS DE UNIVERSIDADES', '{"descricao":"Esquema fraudulento garante certificado de n\\u00edvel superior em 10 dias; MEC diz n\\u00e3o ter responsabilidade no caso.","descricao_autor":null,"imagem_url":"\\/media\\/files\\/noticias\\/1376923682.png","link_materia":"http:\\/\\/www.estadao.com.br\\/noticias\\/vidae,sites-vendem-diplomas-falsos-de-universidades,992592,0.htm"}'),
	(4, NULL, 35, '2013-08-19 14:17:37', 'A', 'DIPLOMA A R$ 410,00', '{"descricao":"Pela internet se vende diplomas universit\\u00e1rios a R$ 410,00. Procuradora da Rep\\u00fablica afirma que venda \\u00e9 ato criminoso...","link_materia":"http:\\/\\/oglobo.globo.com\\/educacao\\/pela-internet-diplomas-universitarios-r-410-6356076","imagem_url":"\\/media\\/files\\/noticias\\/1376932656.png"}'),
	(5, NULL, 35, '2013-08-19 14:25:35', 'A', 'MENTIRAS NO CURRÍCULO', '{"descricao":"Mais da metade dos curr\\u00edculos cont\\u00e9m alguma esp\\u00e9cie de mentira. Todas elas s\\u00e3o facilmente flagradas pelos recrutadores....","link_materia":"http:\\/\\/veja.abril.com.br\\/180608\\/p_148.shtml","imagem_url":"\\/media\\/files\\/noticias\\/1376933135.png"}'),
	(6, NULL, 35, '2013-08-19 14:26:38', 'A', 'DEMISSÃO POR DIPLOMA FALSO', '{"descricao":"UEL demite 26 servidores por usarem diploma falso para obter promo\\u00e7\\u00e3o. Eles ter\\u00e3o que ressarcir a institui\\u00e7\\u00e3o dos valores recebidos...","link_materia":"http:\\/\\/g1.globo.com\\/parana\\/noticia\\/2012\\/08\\/uel-demite-26-servidores-por-usar-diploma-falso-para-obter-promocao.html","imagem_url":"\\/media\\/files\\/noticias\\/1376933198.png"}'),
	(7, NULL, 35, '2013-08-19 14:27:10', 'A', 'FALSO MÉDICO', '{"descricao":"A faculdade Unigranrio, na Baixada Fluminense, decidiu expulsar o estudante de medicina acusado de ter liberado a menina Joanna...","link_materia":"http:\\/\\/g1.globo.com\\/rio-de-janeiro\\/noticia\\/2010\\/09\\/faculdade-do-rj-confirma-expulsao-de-falso-medico-que-atendeu-joanna.html","imagem_url":"\\/media\\/files\\/noticias\\/1376933230.png"}'),
	(8, NULL, 34, '2013-08-19 14:28:51', 'A', 'ROSANE MACHADO', '{"descricao":"\\"Todas as faculdades v\\u00eam sofrendo com a falsifica\\u00e7\\u00e3o de diplomas e outros documentos. O site DiplomaNaWeb veio resolver este problema para a Facear e hoje todos os diplomas que n\\u00f3s emitimos, a partir de julho de 2012, est\\u00e3o publicados no site e fica mais dif\\u00edcil que eles sejam falsificados, pois qualquer um pode verificar no site se o documento f\\u00edsico \\u00e9 ou n\\u00e3o verdadeiro.\\"","descricao_autor":"Diretora Acad\\u00eamica da Facear - Faculdade Educacional Arauc\\u00e1ria","imagem_url":"\\/media\\/files\\/depoimentos\\/1376933331.png"}'),
	(9, NULL, 34, '2013-08-19 14:29:16', 'A', 'JOSÉ FLAVIO GALVÃO', '{"descricao":"\\"Com o meu diploma no site DiplomaNaWeb ficou bem mais f\\u00e1cil acess\\u00e1-lo, tanto para mim como para poss\\u00edveis interessados. Al\\u00e9m disso tamb\\u00e9m tenho mais credibilidade, pois \\u00e9 a pr\\u00f3pria faculdade que atesta que este documento \\u00e9 verdadeiro.\\"","descricao_autor":"Bacharel em Administra\\u00e7\\u00e3o Facear","imagem_url":"\\/media\\/files\\/depoimentos\\/1376933356.png"}'),
	(10, NULL, 34, '2013-08-19 14:29:37', 'A', 'RODRIGO N. RIQUELME MACEDO', '{"descricao":"\\"Como ex-aluno, Advogado e Mestrando, tenho a dizer que a atividade profissional ap\\u00f3s a vida acad\\u00eamica depende muito da continuidade da express\\u00e3o de seriedade e comprometimento que a universidade proporciona em um mercado t\\u00e3o competitivo de trabalho. A FACEAR ao disponibilizar os diplomas de seus formandos no site DiplomaNaWeb, ratifica o compromisso com seus alunos at\\u00e9 mesmo ap\\u00f3s a formatura.\\"","descricao_autor":"Advogado formado Facear","imagem_url":"\\/media\\/files\\/depoimentos\\/1376933377.png"}'),
	(11, NULL, 34, '2013-08-19 14:30:01', 'A', 'ARIANE CRISTINA G. STOCKER', '{"descricao":"\\"Adorei saber que meu diploma estava dispon\\u00edvel no site DiplomaNaWeb. Acho que todas as faculdades deveriam adotar essa pr\\u00e1tica, com certeza a falsifica\\u00e7\\u00e3o diminuiria, j\\u00e1 que \\u00e9 a pr\\u00f3pria institui\\u00e7\\u00e3o de ensino que afirma que o documento \\u00e9 verdadeiro. Sem contar na facilidade e agilidade de poder ver e enviar seu diploma de qualquer lugar que esteja.\\"","descricao_autor":"Licenciada em Letras Facear","imagem_url":"\\/media\\/files\\/depoimentos\\/1376933401.png"}');
/*!40000 ALTER TABLE `pag_pagina_bloco` ENABLE KEYS */;


-- Copiando estrutura para tabela crepz631_hazo_diploma.pag_template
CREATE TABLE IF NOT EXISTS `pag_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `descricao` longtext,
  `cad_data` datetime NOT NULL,
  `cad_user_id` int(10) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `file_content` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_pag_template_hazo_user1_idx` (`cad_user_id`),
  CONSTRAINT `fk_pag_template_hazo_user1` FOREIGN KEY (`cad_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela crepz631_hazo_diploma.pag_template: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `pag_template` DISABLE KEYS */;
REPLACE INTO `pag_template` (`id`, `titulo`, `descricao`, `cad_data`, `cad_user_id`, `file_name`, `file_content`) VALUES
	(1, 'Visualização 01', '<p style="text-align: justify;"><em><span style="font-size:11px"><span style="font-size:14px"><strong>Mussum</strong></span> ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra l&aacute; , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. M&eacute; faiz elementum girarzis, nisi eros vermeio, in elementis m&eacute; pra quem &eacute; amistosis quis leo. Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</span></em></p>\r\n\r\n<p style="text-align: justify;"><span style="font-size:11px"><strong>Suco</strong> de cevadiss, &eacute; um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no m&eacute;, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet m&eacute; vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.</span></p>\r\n\r\n<p style="text-align: justify;"><span style="font-size:11px">Casamentiss faiz malandris se pirulit&aacute;, Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem ipsum dolor sit amet, consectetuer Ispecialista im m&eacute; intende tudis nuam golada, vinho, uiski, carir&iacute;, rum da jamaikis, s&oacute; num pode ser mijis. Adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</span></p>\r\n', '2013-08-15 10:18:55', 3, 'visualizacao_01.twig', _binary 0x434F4E544555444F);
/*!40000 ALTER TABLE `pag_template` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
