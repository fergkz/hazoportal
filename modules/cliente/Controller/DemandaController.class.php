<?php

use Pessoa\ClienteUsuario as ClienteUsuario;
use Pessoa\Cliente as Cliente;

use Projeto\Demanda as Demanda;
use Projeto\TipoDemanda as TipoDemanda;
use Projeto\Anexo as Anexo;
use Projeto\DemandaGerente as DemandaGerente;

use System\User as User;
use Lib\WideImage\Image     as Image;

class DemandaController extends \System\MyController
{
    
    public function __construct()
    {
        parent::__construct();
        
        if( !$this->session('clientes_logado') ){
            $res = ClienteUsuario::getList(array(
                'dao.user_id = ?' => User::online()->getID()
            ));
            
            if( $res['cont_total'] > 0  ){
                foreach( $res['rows'] as $ClienteUsuario ){
                    $tmp[] = $ClienteUsuario->getClienteObj();
                }
                $this->session('clientes_logado', $tmp);
            }else{
                $this->session('clientes_logado', null);
            }
        }
    }
    
    /**
     * @return view
     */
    public function listaAction()
    {
        $render = array();
        
        $this->breadcrumb[] = array( 'title' => 'Minhas Demandas' );
        
        
        $res = ClienteUsuario::getList(array(
            'dao.user_id = ?' => User::online()->getID()
        ));
        
        if( $res['cont_total'] <= 0 ){
            return 404;
        }
        
        foreach( $res['rows'] as $ClienteUsuario ){
            $clientsIn[] = "?";
            $clientsID[] = $ClienteUsuario->getClienteID();
        }
        
        $render['demandas'] = Demanda::getList(array(
            "dao.cliente_id in (".implode(', ', $clientsIn).")" => $clientsID
        ), null, null, null, array(
            "dao.cad_data asc"
        ));
        
        $this->view()->setTemplate('demanda/lista.twig')->display($render);
    }
    
    /**
     * @return view
     */
    public function novoAction()
    {
        $this->breadcrumb[] = array( 'title' => 'Minhas Demandas', 'url' => url_module."/lista" );
        $this->breadcrumb[] = array( 'title' => 'Criação de demanda' );
        
        if( !$this->session('clientes_logado') ){
            return 404;
        }
        
        $render['clientes'] = $this->session('clientes_logado');
        
        $res = TipoDemanda::getList(array(
            "dao.permite_cliente_selecionar = 'S'"
        ));
        $render['tipos'] = $res['rows'];
        
        $this->view()->setTemplate('demanda/cadastro.twig')->display($render);
    }
    
    /**
     * @return Json
     */
    public function criarAction()
    {
        $render['status'] = false;
        
        $res = Cliente::getList(array(
            'pes.slug = ?' => $this->post('cliente')
        ), null, 0, 1);
        
        if( $res['cont_total'] <= 0 ){
            $this->error('Cliente inválido')->json();
        }else{
            $Cliente = $res['rows'][0];
        }
        
        $Demanda = new Demanda();
        $Demanda->setClienteID( $Cliente->getID() );
        $Demanda->setTipoDemandaID( $this->post('tipo_demanda') );
        $Demanda->setTitulo( $this->post('titulo') );
        $Demanda->setDescricao( $this->post('descricao') );
        $Demanda->setStatus('A');
        
        if( $Demanda->save() ){
            
            $DemandaGerente = new DemandaGerente();
            $DemandaGerente->setDemandaID( $Demanda->getID() );
            $DemandaGerente->setUserID( $Cliente->getGerenteUserID() );
            
            if( $DemandaGerente->save() ){
                
                if( $this->files('arquivo') ){

                    $Anexo = new Anexo();

                    $files = $this->files('arquivo');

                    foreach( $files['name'] as $i => $file ){

                        $Anexo = new Anexo();
                        $Anexo->setFileContent(file_get_contents($files['tmp_name'][$i]) );
                        $Anexo->setFileName( $files['name'][$i] );
                        $Anexo->setFileType( $files['type'][$i] );
                        $Anexo->setFileSize( $files['size'][$i] );
                        $Anexo->setDemandaID( $Demanda->getID() );

                        $Anexo->save();

                    }
                }
        
            }
        
        }
        
        if( !$this->error() ){
            $render['status'] = true;
            $render['redirect'] = url_module."/lista";
        }
        
        $this->json($render, true);
    }
    
    /**
     * @return View
     */
    public function visualizacaoAction( $slug = null )
    {
        $res = Demanda::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        
        if( $res['cont_total'] <= 0 ){
            return 404;
        }else{
            $Demanda = $res['rows'][0];
        }
        
        $this->breadcrumb[] = array( 'title' => 'Minhas Demandas', 'url' => url_module."/lista" );
        $this->breadcrumb[] = array( 'title' => $Demanda->getTitulo() );
        
        $render['Demanda'] = $Demanda;
        $render['anexos'] = $Demanda->listaAnexos();
        
        $render['atividades'] = $Demanda->listaAtividades();
        
        $this->view()->setTemplate('demanda/visualizacao.twig')->display($render);
    }
    
    /**
     * @return View:Image
     */
    public function anexoVisualizacaoAction( $slug = null )
    {
        $Anexo = Anexo::getBySlug($slug);
        
        if( !$Anexo ){
            return 404;
        }
        
        $img = Image::load(  $Anexo->getFileContent() );
        
        if( $this->get('sz') ){
            $img = $img->resize($this->get('sz'));
        }
        
        $img->output('png');
    }
    
    /**
     * @return View:Download
     */
    public function anexoDownloadAction( $slug = null )
    {
        $Anexo = Anexo::getBySlug($slug);
        
        if( !$Anexo ){
            return 404;
        }
        
        header("Content-disposition: attachment; filename={$slug}.".$Anexo->getFileExtension());
        header('Content-type: '.$Anexo->getFileType());
        die($Anexo->getFileContent());
    }
    
}