<?php

class IndexController extends \System\MyController
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function indexAction()
    {
        $this->redirect(url_mode."/demanda/lista");
    }
    
}