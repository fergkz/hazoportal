<?php

use MVC\Controller as Controller;

use Model\Pagina\Pagina as Pagina;
use Model\Pagina\PaginaBloco as Bloco;
use Lib\WideImage\Image as Image;

class BlocoController extends Controller{
    
    public function indexAction( $paginaSlug = null ){
        $render = array();
        $this->view()->display($render);
    }
    
    public function listaAction( $paginaSlug = null ){
        $render['pagina_slug'] = $paginaSlug;
        switch( $paginaSlug ){
            case 'depoimentos':
                $render['pagina_descr'] = 'Depoimento';
                break;
            case 'noticias':
                $render['pagina_descr'] = 'Notícia';
                break;
        }
        
        $Pagina = Pagina::getBySlug($paginaSlug);
        
        if( !$Pagina || !is_object($Pagina) ){
            return 404;
        }
        
        $render['blocos'] = Bloco::listaByPagina($Pagina);
        
        $this->view()->display($render);
    }
    
    public function cadastroAction( $paginaSlug = null, $blocoID = null ){
        $render['pagina_slug'] = $paginaSlug;
        
        $Pagina = Pagina::getBySlug($paginaSlug);
        
        if( !$Pagina || !is_object($Pagina) ){
            return 404;
        }
        
        $Bloco = $blocoID ? Bloco::getByID($blocoID) : new Bloco();
        
        if( $_POST ){
            
            if( $_FILES['arquivo'] ){
                try{
                    $Image = Image::loadFromUpload('arquivo');
                }catch( Exception $e ){
                    unset($e);
                    _setError("A imagem informada é inválida");
                }   
            }
                
            $Bloco->setTitulo($_POST['titulo']);
            foreach( $_POST as $ind => $val ){
                if( !in_array($ind, array('titulo')) ){
                    $Bloco->setConteudo($ind, $val);
                }
            }
            $Bloco->setStatus('A');
            $Bloco->setPaginaObj($Pagina);
            
            if( !_getErrors() ){
            
                if( !empty($Image) && is_object($Image) ){
                    
                    $folder = path.ds.'media'.ds.'files'.ds.$Pagina->getSlug();

                    if( !file_exists($folder) ){
                        mkdir($folder);
                    }

                    $filename = time().'.png';
                    
                    $Image->resize(null, 280)->saveToFile($folder.ds.$filename);
                
                    $Bloco->setConteudo('imagem_url', '/media/files/'.$Pagina->getSlug().'/'.$filename);
                }

                if( $Bloco->save() && !_getErrors() ){
                    _setSuccess("Alterações salvas com sucesso");
                    $this->redirect(url."/editor/bloco/lista/{$paginaSlug}");
                }elseif( !empty($Image) && is_object($Image) ){
                    unlink($filename);
                }
                
            }

        }
        $render['Bloco'] = $Bloco;
        
        $this->view()->display($render);
    }
}