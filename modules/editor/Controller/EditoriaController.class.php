<?php

use MVC\Controller as Controller;

use Model\Pagina\Editoria as Editoria;
use Model\Pessoa\Autor as Autor;
use Model\Pessoa\Pessoa as Pessoa;
use Model\Pagina\EditoriaAutor as EditoriaAutor;

class EditoriaController extends Controller{
    
    public function indexAction(){
        $render['editorias'] = Editoria::listAll();
        $this->view()->display($render);
    }
    
    public function cadastroAction( $editoriaID = null ){
        $render['editorias'] = Editoria::listAll();
        $render['editoria_id'] = $editoriaID;
        
        $Editoria = !$editoriaID ? new Editoria() : Editoria::getByID($editoriaID);
        
        if( !$Editoria || !is_object($Editoria) ){
            return 404;
        }
        
        if( $_POST ){

            $Editoria->setNome($_POST['nome']);
            $Editoria->setSlug($_POST['slug']);
            $Editoria->setStatus($_POST['status']);
            $Editoria->setParentEditoriaID( @$_POST['parent'] ?: null );
            
            if( $Editoria->save() ){
                _setSuccess("Alterações salvas com sucesso na editoria '".$Editoria->getNome()."'");
                $this->redirect(url."/editor/editoria/visualizacao/".$Editoria->getID());
            }

        }
        
        $render['Editoria'] = $Editoria;
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $editoriaID = null ){
        $render['editoria_id'] = $editoriaID;
        
        $Editoria = Editoria::getByID($editoriaID);
        
        if( !$Editoria || !is_object($Editoria) ){
            return 404;
        }
        
        $render['Editoria'] = $Editoria;
        $render['autores'] = $Editoria->getAutores();
        $this->view()->display($render);
    }
    
    public function excluirAction( $editoriaID = null ){
        $render['editoria_id'] = $editoriaID;
        
        $Editoria = Editoria::getByID($editoriaID);
        
        if( !$Editoria || !is_object($Editoria) ){
            return 404;
        }
        
        if( @$_POST['confirma'] ){
            if( $Editoria->save('D') ){
                _setSuccess("Editoria excluída com sucesso");
                $this->redirect(url."/editor/editoria");
            }
        }
        
        $render['Editoria'] = $Editoria;
        $this->view()->display($render);
    }
    
    public function searchAutorAction(){
        echo Autor::search('full-editoria');
    }
    
    public function listaAutoresAction( $editoriaID = null ){
        
        $Editoria = Editoria::getByID($editoriaID);
        if( !$Editoria ){
            return 404;
        }
        $render['autores'] = $Editoria->getAutores();

        $this->view()->display($render);
        
    }
    
    public function cadastroAutorAction( $editoriaID = null, $editoriaAutorID = null ){
        
        $render['editoria_id'] = $editoriaID;
        $render['editoria_autor_id'] = $editoriaAutorID;
        
        $Editoria = Editoria::getByID($editoriaID);
        if( !$Editoria ){
            return 404;
        }
        
        if( $editoriaAutorID ){
            $EditoriaAutor = EditoriaAutor::getByID($editoriaAutorID);
            if( !$EditoriaAutor || $EditoriaAutor->getEditoriaID() !== $editoriaID ){
                return 404;
            }
        }else{
            $EditoriaAutor = new EditoriaAutor();
            $EditoriaAutor->setEditoriaID($editoriaID);
        }
        
        if( !empty($_POST) ){
            
            $EditoriaAutor->setAutorID($_POST['autor']);
            $EditoriaAutor->save();
            
            if( !_getErrors() ){
                $response['status'] = true;
                $response['message'] = "Autor salvo com sucesso!";
            }else{
                $response['status'] = false;
                $response['message'] = implode("<br/>",_getErrors());
                _clearErrors();
            }
            die(json_encode($response));
        }
        
        $render['Editoria'] = $Editoria;
        $render['EditoriaAutor'] = $EditoriaAutor;
        
        $this->view()->display($render);

    }
    
    public function excluirAutorAction( $editoriaID = null, $editoriaAutorID = null ){
        
        $Editoria = Editoria::getByID($editoriaID);
        if( !$Editoria || !$editoriaAutorID ){
            return 404;
        }
        
        $EditoriaAutor = EditoriaAutor::getByID($editoriaAutorID);
        if( !$EditoriaAutor || $EditoriaAutor->getEditoriaID() !== $editoriaID ){
            return 404;
        }
        
        $EditoriaAutor->save('D');
            
        if( !_getErrors() ){
            $response['status'] = true;
            $response['message'] = "Autor desanexado desta editoria com sucesso!";
        }else{
            $response['status'] = false;
            $response['message'] = implode("<br/>",_getErrors());
            _clearErrors();
        }
        
        die(json_encode($response));

    }
    
    
//    public function autorCadastroAction( $editoriaID = null, $cpf = null ){
//        
//        $render['editoria_id'] = $editoriaID;
//        
//        if( !$cpf ){
//            
//            if( !empty($_POST) ){
//                $render['pessoas'] = Pessoa::consulta($_POST['consulta']);
//            }
//            
//            $this->view()->setTemplate('editoria/autor-cadastro-cpf.html')->display($render);
//            
//            exit;
//            
//        }else{
//            die( "cpf informado: {$cpf}" );
//        }
//
//        $render['editoria_id'] = $editoriaID;
        
//        $Editoria = Editoria::getByID($editoriaID);
//        
//        if( !$Editoria || !is_object($Editoria) ){
//            return 404;
//        }
//        
//        $render['autor_id'] = $autorID;
//        
//        $Autor = $autorID ? Autor::getByID($autorID) : new Autor();
//        
//        if( $autorID && (!$Autor || !$Autor->getID()) ){
//            return 404;
//        }
        
//        $this->view()->display($render);
//    }
    
}