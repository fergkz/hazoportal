<?php

use MVC\Controller as Controller;

use Model\Pessoa\Autor      as Autor;
use Model\Pagina\Pagina     as Pagina;
use Model\Pagina\Editoria   as Editoria;
use Core\Model\User         as User;

class ArtigoController extends Controller{
    
    public function indexAction(){
        $render = array();
        
        $Autor = Autor::getOnline();
        
        if( @$_GET ){
            $get = $_GET;
            $dados = Pagina::pesquisa( $get, array( 'autor_permissao' => $Autor ) );
            $render['paginas'] = @$dados['paginas'];
            $render['link_atual'] = @$_GET['pg'];
            unset($_GET['pg']);
            foreach( $_GET as $ind => $row ){
                $nGet[] = $ind."=".$row;
            }
            $render['link_next'] = "?".implode("&", $nGet)."&pg=";
            $render['link_cont'] = $dados['cont'];
            $render['max_links'] = $dados['max_links'];
        }
        
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $paginaID = null ){
        
        $render['pagina_id']   = $paginaID;
        
        $Autor = Autor::getOnline();
        
        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }
        $Pagina = Pagina::getByID($paginaID);
        if( !$Pagina ){
            return 404;
        }
        
        if( !self::permiteAlteracao($Pagina) ){
            return 404;
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
    public function selecionarEditoriaAction(){
        $render = array();
        $Autor = Autor::getOnline();
        $render['editorias'] = Editoria::listaByAutor($Autor, 'A');
        if( $render['editorias'] ){
            foreach( $render['editorias'] as $Editoria ){
                $render['parent_editorias'][$Editoria->getID()] = Editoria::getArrayParents($Editoria);
            }
        }
        $this->view()->display($render);
    }
    
    public function cadastroAction( $editoriaID = null, $paginaID = null ){
        
        $Editoria = Editoria::getByID($editoriaID);
        
        if( !$Editoria ){
            return 404;
        }
        
        $render['Editoria'] = $Editoria;

        $Autor = Autor::getOnline();

        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }

        if( $paginaID ){
            $Pagina = Pagina::getByID($paginaID);
            if( !$Pagina ){
                return 404;
            }
        }else{
            $Pagina = new Pagina();
            $Pagina->setEditoriaID($editoriaID);
        }
        
        if( !self::permiteAlteracao(null, $Editoria) ){
            return 404;
        }
        
        if( $_POST ){
            
            if( !empty($_POST['conteudo']) ){
                $data = explode("<hr />",$_POST['conteudo']);
                if( count($data) > 0 ){
                    $data2['resumo']            = array_shift($data);
                    $data2['conteudo']          = $data2['resumo'].implode("<hr>", $data);
                    $data2['conteudo_completo'] = $_POST['conteudo'];
//                    debug($data);
                    $Pagina->setConteudoArray( $data2 );
                }
            }else{
                $Pagina->setConteudoArray( array() );
            }
            
            $Pagina->setStatus( $_POST['status'] );
            $Pagina->setTitulo( $_POST['titulo'] );
            $Pagina->save();
            
            if( !_getErrors() ){
                _setSuccess("Página salva com sucesso");
                $this->redirect(url."/editor/artigo/visualizacao/".$Pagina->getID());
            }
            
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
    private static function permiteAlteracao( $Pagina = null, $Editoria = null ){
        
        if( $Pagina ){
            $User = User::online();
            if( $Pagina->getCadastroUserID() === $User->getID() ){
                return true;
            }
        }
            
        if( $Editoria ){
            $autoresEditoria = $Editoria->getAutores();
        }elseif( $Pagina ){
            $autoresEditoria = $Pagina->getEditoriaObj()->getAutores();
        }else{
            return false;
        }
        $Autor = Autor::getOnline();

        $grant = false;
        if( $autoresEditoria ){
            foreach( $autoresEditoria as $AutorEditoria ){
                if( $AutorEditoria->getAutorID() == $Autor->getID() ){
                    $grant = true;
                    break;
                }
            }
        }

        if( !$grant ){
            return false;
        }
        
        return true;
    }
    
}