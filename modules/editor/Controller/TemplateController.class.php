<?php

use MVC\Controller as Controller;

use Model\Pagina\Template as Template;

class TemplateController extends Controller{
    
    public function indexAction(){
        $render['templates'] = Template::listAll();
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $templateID = null ){
        
        $render['template_path'] = Template::getPath();
        
        $Template = Template::getByID($templateID);
        if( !$Template ){
            return 404;
        }
        
        $render['Template'] = $Template;
        
        $this->view()->display($render);
    }
    
    public function cadastroAction( $templateID = null ){
        
        $render['template_id'] = $templateID;
        $render['template_path'] = Template::getPath();
        
        if( $templateID ){
            $Template = Template::getByID($templateID);
            if( !$Template ){
                return 404;
            }
        }else{
            $Template = new Template();
        }
        
        if( !empty($_POST) ){
            
            $Template->setTitulo($_POST['titulo']);
            $Template->setDescricao($_POST['descricao']);
            $Template->setFileName($_POST['path']);
            
            $Template->save();
            
            if( !_getErrors() ){
                $this->redirect(url."/editor/template/visualizacao/".$Template->getID());
            }
            
        }
        
        $render['Template'] = $Template;
        
        $this->view()->display($render);
    }
    
    public function preVisualizacaoAction(){
        $render = array();
        $this->view()->display($render);
    }
    
}