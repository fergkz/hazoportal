<?php

use MVC\Controller as Controller;

use Model\Pessoa\Autor as Autor;
use Model\Pagina\Pagina as Pagina;
use Model\Pagina\Editoria as Editoria;
use Model\Pagina\Template as Template;

class PaginaController extends Controller{
    
    public function __construct(){
        
    }
    
    public function indexAction(){
        $render = array();
        
        if( @$_GET ){
            $get = $_GET;
            $dados = Pagina::pesquisa( $get );
            $render['paginas'] = @$dados['paginas'];
            $render['link_atual'] = @$_GET['pg'];
            unset($_GET['pg']);
            foreach( $_GET as $ind => $row ){
                $nGet[] = $ind."=".$row;
            }
            $render['link_next'] = "?".implode("&", $nGet)."&pg=";
            $render['link_cont'] = $dados['cont'];
            $render['max_links'] = $dados['max_links'];
        }
        
        $this->view()->display($render);
    }
    
    public function listaPaginasEditoriaAction(){
        $render = array();
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $paginaID = null ){
        
        $render['pagina_id']   = $paginaID;
        
        $Autor = Autor::getOnline();
        
        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }
        $Pagina = Pagina::getByID($paginaID);
        if( !$Pagina ){
            return 404;
        }
        
        if( !self::permiteAlteracao($Pagina) ){
            return 404;
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
    public function cadastroAction( $editoriaID = null, $parentID = null, $paginaID = null ){
        $render['editorias'] = Editoria::listAll();
        $render['templates'] = Template::listAll();
        
        if( $editoriaID === "~" ){
            $editoriaID = null;
        }
        if( $parentID === "~" ){
            $parentID = null;
        }
        if( $paginaID === "~" ){
            $paginaID = null;
        }
        
        $render['editoria_id'] = $editoriaID;
        $render['parent_id']   = $parentID;
        $render['pagina_id']   = $paginaID;
        
        $Autor = Autor::getOnline();
        
        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }
        
        if( $paginaID ){
            $Pagina = Pagina::getByID($paginaID);
            if( !$Pagina ){
                return 404;
            }
        }else{
            $Pagina = new Pagina();
            $Pagina->setEditoriaID($editoriaID);
            $Pagina->setParentPaginaID($parentID);
        }
        
        if( !self::permiteAlteracao($Pagina) ){
            return 404;
        }
        
        if( $_POST ){
            
            $Pagina->setPublicacaoDataInicio(   $_POST['dt_ini']);
            $Pagina->setPublicacaoDataFinal(    $_POST['dt_fim']);
            $Pagina->setSlug(                   null);
            $Pagina->setStatus(                 $_POST['status']);
            $Pagina->setTemplateID(             null);
            $Pagina->setTitulo(                 $_POST['titulo']);
            $Pagina->setEditoriaID(             $_POST['editoria'] ?: null);
            $Pagina->setTemplateID(             $_POST['template'] ?: null);
            
            $Pagina->save();
            
            if( !_getErrors() ){
                _setSuccess("Página salva com sucesso");
                $this->redirect(url."/editor/pagina/visualizacao/".$Pagina->getID());
            }
            
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
    public function editarTemplateAction( $paginaID = null ){
        
        $render['pagina_id']   = $paginaID;
        
        $Autor = Autor::getOnline();
        
        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }
        $Pagina = Pagina::getByID($paginaID);
        if( !$Pagina || !$Pagina->getTemplateID() ){
            return 404;
        }
        
        if( !self::permiteAlteracao($Pagina) ){
            return 404;
        }

        if( !empty($_POST) ){
            
            if( !empty($_POST['conteudo']) ){
                $data = explode("<hr />",$_POST['conteudo']);
                if( count($data) > 0 ){
                    $_POST['resumo'] = array_shift($data);
                    $_POST['conteudo'] = $_POST['resumo'].implode("<hr>", $data);
                }
            }
            
            $Pagina->setConteudoArray( $_POST );
            
            if( $Pagina->save() ){
                $response['status'] = 'success';
                $response['message'] = 'Conteúdo da página atualizado com sucesso';
            }else{
                $response['status'] = 'error';
                $response['message'] = implode("<br/>", _getErrors());
                _clearErrors();
            }
            die(json_encode($response));
        }
        
        $render['Pagina'] = $Pagina;
        $render['template_link'] = $Pagina;
        
        $filename = $Pagina->getTemplateObj()->getPath().$Pagina->getTemplateObj()->getFileName();
        
        $render['template_path'] = str_replace(path,"",$filename);
        
        $render = array_merge($render, $Pagina->getConteudoArray());
        
        $this->view()->display($render);
    }
    
    public function excluirAction( $paginaID = null ){
        
        $Autor = Autor::getOnline();
        
        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }
        
        $Pagina = Pagina::getByID($paginaID);
        
        if( !$Pagina ){
            return 404;
        }
        
        if( !self::permiteAlteracao($Pagina) ){
            return 404;
        }
        
        if( $_POST && $_POST['excluir'] ){
            
            if( $Pagina->save('D') ){
                _setSuccess("Página excluída com sucesso");
                $this->redirect(url."/editor/pagina");
            }
            
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
    private static function permiteAlteracao( $Pagina ){
        if( $Pagina->getEditoriaID() ){
            
            $autoresEditoria = $Pagina->getEditoriaObj()->getAutores();
            $Autor = Autor::getOnline();
            
            $grant = false;
            if( $autoresEditoria ){
                foreach( $autoresEditoria as $AutorEditoria ){
                    if( $AutorEditoria->getAutorID() == $Autor->getID() ){
                        $grant = true;
                        break;
                    }
                }
            }
            
            if( !$grant ){
                return false;
            }
        }
        return true;
    }
    
}