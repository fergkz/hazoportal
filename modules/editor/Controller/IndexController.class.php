<?php

use MVC\Controller as Controller;
use Core\Model\User as User;

class IndexController extends Controller{
    
    public function indexAction(){
        
        $this->redirect(url.User::online()->getGroupObj()->getDefaultUrl());
        
    }
    
}