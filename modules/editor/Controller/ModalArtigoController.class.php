<?php

use MVC\Controller as Controller;

use Model\Pessoa\Autor      as Autor;
use Model\Pagina\Pagina     as Pagina;
use Model\Pagina\Editoria   as Editoria;
use Lib\Format              as Format;

class ModalArtigoController extends Controller{
    
    public function selecionarEditoriaAction(){
        $render = array();
        $Autor = Autor::getOnline();
        $render['editorias'] = Editoria::listaByAutor($Autor, 'A');
        if( $render['editorias'] ){
            foreach( $render['editorias'] as $Editoria ){
                $render['parent_editorias'][$Editoria->getID()] = Editoria::getArrayParents($Editoria);
            }
        }
        $this->view()->display($render);
    }
    
    public function cadastroAction( $editoriaID = null, $paginaID = null ){

        $Editoria = Editoria::getByID($editoriaID);
        
        if( !$Editoria ){
            return 404;
        }
        
        $render['Editoria'] = $Editoria;

        $Autor = Autor::getOnline();

        if( !$Autor || $Autor->getStatus() !== 'A' ){
            return 404;
        }

        if( $paginaID ){
            $Pagina = Pagina::getByID($paginaID);
            if( !$Pagina ){
                return 404;
            }
        }else{
            $Pagina = new Pagina();
            $Pagina->setEditoriaID($editoriaID);
        }
        
        if( !$Pagina->permiteLogadoAlteracao() ){
            return 404;
        }
//        debug($_SERVER['REQUEST_METHOD']);
        if( $_POST ){
            _clearErrors();
            if( !empty($_POST['conteudo']) ){
                $data = explode("<hr />",$_POST['conteudo']);
                if( count($data) > 0 ){
                    $data2['resumo']            = array_shift($data);
                    $data2['conteudo']          = $data2['resumo'].implode("<hr>", $data);
                    $data2['conteudo_completo'] = $_POST['conteudo'];
                    $Pagina->setConteudoArray( $data2 );
                }
            }else{
                $Pagina->setConteudoArray( array() );
            }
            
            $Pagina->setStatus( $_POST['status'] );
            $Pagina->setTitulo( $_POST['titulo'] );
            $Pagina->setSlug( trim(Format::strigToUrl( $Pagina->getTitulo() )) );
            $Pagina->save();
            
            if( !_getErrors() ){
                $editorias = $Pagina->getArrayEditorias();
                $link = url.'/artigo';
                foreach( $editorias  as $Editoria2 ){
                    $link .= '/'.$Editoria2->getSlug();
                }
                $link .= '/'.$Pagina->getSlug();
                
                
                $response = array(
                    'status' => true,
                    'msg' => "Página salva com sucesso",
                    'href' => $link
                );
            }else{
                $response = array(
                    'status' => false,
                    'msg' => implode("; ", _getErrors())
                );
            }
            
            die( json_encode($response) );
        }

        $render['Pagina'] = $Pagina;
        
        $this->view()->display($render);
    }
    
}