<?php

use MVC\Controller as Controller;

class IndexController extends Controller{
    
    public function indexAction(){
        $this->view()->display();
    }
    
}