<?php

namespace Model\Novo;

class MyModel
{

    public function __construct ()
    {
//        parent::__construct();
    }

    public function clearParse()
    {
        unset($this->_parse);
        unset($this->_pk);
        unset($this->_action);
    }
    
    public function load ($attributes = null)
    {
        $db = new \Core\System\DataBaseConnection();
        foreach (static::$daoPrimary as $attribute => $column) {
            if( empty($this->$attribute) ){
                return false;
            }
            $where[] = $column." = ?";
            $bind[] = $this->$attribute;
        }

        $sql = "SELECT * FROM ".static::$daoTable." WHERE ".implode(" and ", $where);

        $res = $db->query($sql, $bind);

        if (!$res) {
            return false;
        }

        $row = $res[0];

        foreach (static::$daoCols as $attribute => $column) {
            if (!$attributes || in_array($attribute, $attributes)) {
                $this->$attribute = $row[$column];
            }
        }

        $this->clearParse();
        return $this;
    }

    public function delete ()
    {
        return $this->save('D');
    }

    private function loadQueryParams($wherePrimary, $bindPrimary)
    {
        $primaryExists = $wherePrimary && count($wherePrimary) > 0 ? true : false;
        if ($this->daoAction !== 'D') {
            if( $primaryExists || $this->daoAction === 'U' ){
                // update
                $this->daoAction = "U";
                foreach (static::$daoCols as $attribute => $column) {
                    $bind[] = $this->$attribute;
                    $this->$attribute = $this->$attribute;
                    $sqlCols[] = " {$column} = ? ";
                }
                $sql = "update ".static::$daoTable." set ".implode(", ", $sqlCols)." where ".implode(' and ', $wherePrimary);
                $bind = array_merge($bind, $bindPrimary);
            }else{
                // insert
                $this->daoAction = "I";
                foreach( static::$daoCols as $attribute => $column ){
                    $this->$attribute = @$this->$attribute;
                    $bind[] = @$this->$attribute;
                    $sqlCols[] = $column;
                    $sqlVals[] = "?";
                }
                $sql = "insert into ".static::$daoTable." (".implode(", ", $sqlCols).") values (".implode(" ,", $sqlVals).")";
            }
        }else {
            // delete
            if (empty($wherePrimary)) {
                return false;
            }
            $sql = "delete from ".static::$daoTable." where ".implode(" and ", $wherePrimary);
            $bind = $bindPrimary;
        }
        return array(
            'sql' => $sql,
            'bind'=> $bind
        );
    }
    
    public function save ($operacao = null)
    {
        $this->old = clone $this;
        $this->old->load();
        
        $db = new \Core\System\DataBaseConnection();
        $primaryExists = true;
        $primaryAttributes = null;
        foreach (static::$daoPrimary as $attribute => $column) {
            if( empty($this->$attribute) ){
                $primaryExists = false;
            }else{
                $wherePrimary[] = $column." = ?";
                $bindPrimary[] = $this->$attribute;
            }
            $primaryAttributes[] = $attribute;
        }

        try{
            $this->daoAction = $operacao;
            $this->loadQueryParams(@$wherePrimary, @$bindPrimary);
            
            /** Begin::Triggers **/
            if( empty($this->old) ){
                $this->old = clone $this;
                $this->old->load();
                unset($this->old->old);
            }
            if( method_exists($this, 'triggerBeforeSave') ){
                $this->triggerBeforeSave();
            }
            /** End::Triggers **/
            
            $tmp  = $this->loadQueryParams(@$wherePrimary, @$bindPrimary);
            $sql  = $tmp['sql'];
            $bind = $tmp['bind'];

            $res = $db->execute($sql, $bind);

            if ($res) {
                if ($this->daoAction === 'I') {
                    $primary = is_array($primaryAttributes) ? $primaryAttributes[0] : $primaryAttributes;
                    if( $primary ){
                        $this->$primary = $db->lastId();
                    }
                }
                /** Begin::Triggers **/
                if( method_exists($this, 'triggerAfterSave') ){
                    $this->triggerAfterSave();
                }
                /** End::Triggers **/
                return $this;
            }else {
                return false;
            }
        }catch( \Exception $e ){
            $db->rollback();
            _setError($e->getMessage());
            return false;
        }
    }

    public static function getList ($whereColumns = array (), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array ())
    {
        $db = new \Core\System\DataBaseConnection();
        $where = $bind = array ();

        if( $whereColumns ){
            foreach( $whereColumns as $column => $value ){

                if( !in_array((String)$column, static::$daoCols) ){

                    if( !is_array($value) ){
                        $bind[] = $value;
                    }else {
                        $arrayVals = array();
                        foreach( $value as $val ){
                            $bind[] = $val;
                        }
                    }

                    $where[] = $column;

                }else{

                    if (!is_array($value)) {
                        $bind[] = $value;
                        $where[] = "{$column} like ?";
                    }else {
                        $arrayVals = array ();
                        foreach ($value as $val) {
                            $arrayVals[] = "?";
                            $bind[] = $val;
                        }
                        $vals = implode(",", $arrayVals);
                        $where[] = "{$column} in ({$vals})";
                    }

                }

            }
        }

        if ($loadAttributes && is_array($loadAttributes) && count($loadAttributes) > 0) {
            foreach ($loadAttributes as $attribute) {
                $selectCols[] = static::$daoCols[$attribute];
            }
            $selectCols = implode(", ", $selectCols);
        }else {
            $selectCols = "*";
        }

        $sql = "SELECT {$selectCols} FROM ".static::$daoTable;
        
        if ($where && count($where) > 0) {
            $sql .= " where ".implode(" and ", $where);
        }

        $resCont = $db->query("select count(*) as cont_total from ({$sql}) t", $bind);
        $result['cont_total'] = $resCont[0]['cont_total'];

        if ($order && count($order) > 0) {

            foreach ($order as $row) {
                $tmp = explode(" ", $row);
                $attribute = $tmp[0];
                $direction = empty($tmp[1]) ? "" : " ".$tmp[1];
                $sqlOrder[] = static::$daoCols[$attribute].$direction;
            }

            $sql .= " order by ".implode(", ", $sqlOrder);
        }

        if ($rowLimit) {
            $sql .= " limit {$rowStart}, {$rowLimit}";
        }

        $res = $db->query($sql, $bind);

        if ($res) {
            $dados = null;
            foreach ($res as $row) {
                $classname = get_called_class();
                $Obj = new $classname;

                foreach ($row as $column => $value) {
                    if( !$column || !in_array($column, static::$daoCols) ){
                        continue;
                    }
                    $key = array_keys(static::$daoCols, $column)[0];
                    $Obj->$key = $value;
                }

                $Obj->clearParse();
                $dados[] = $Obj;
            }

            $result['rows'] = $dados;

            return $result;
        }else {
            return false;
        }
    }

}