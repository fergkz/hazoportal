<?php

namespace Model\Novo;

use Lib\Validate as Validate;
use Lib\Format   as Format;

class Pessoa extends MyModel
{
    protected static $daoTable = "pes_pessoa";
    protected static $daoPrimary = array (
        'ID' => 'id'
    );
    protected static $daoCols = array (
        'ID'                => 'id',
        'razao'             => 'razao',
        'fantasia'          => 'fantasia',
        'slug'              => 'slug',
        'cpf'               => 'cpf',
        'cnpj'              => 'cnpj',
        'dataNascimento'    => 'data_nascimento'
    );

    function triggerBeforeSave(){
        
        if( $this->daoAction !== 'D' ){
            
            $this->cpf = (String)Format::formNumber($this->cpf);

            if( !Validate::isCPF($this->cpf) ){
                _raise("CPF Inválido");
            }

            $cpfExists = self::getList(array(
                'cpf' => $this->cpf
            ), array(
                'ID'
            ), 0, 1, null);
            
            if( $cpfExists['cont_total'] > 0 && $cpfExists['rows'][0]->ID !== $this->ID ){
                _raise("O CPF {$this->cpf} já está cadastrado para outra pessoa");
            }
        }
        
    }
}