<?php

namespace Model\Singles;

class MagicCard extends \MVC\Model{

    public $ID;
    public $name;
    public $namePt;
    public $indice;
    public $cost;
    public $pow;
    public $tgh;
    public $imgUrl;
    public $imgName;
    public $imgExt;
    public $imgContent;
    public $type;
    public $typePt;
    public $descr;
    public $descrPt;
    public $serieImgUrl;
    public $serieImgName;
    public $serieImgExt;
    public $serieImgContent;
    public $serieTitle;
    public $serieTitlePt;
    public $raridade;
    public $raridadePt;
    public $number;
    
    public static function getByIndice( $ind ){
        $sql = "select id from mtg_carta where ind = :ind";
        $bind['ind'] = $ind;
        $res = _query($sql, $bind);
        return self::getInstance(@$res[0]['id']);
    }
    
    public static function getMaxIndice(){
        $sql = "select max(ind) as ind from mtg_carta";
        $res = _query($sql);
        return @$res[0]['ind'] ?: 0;
    }
    
    public static function getCardsSemImagem( $minIndice = 0 ){
        $sql = "select ind from mtg_carta where ind >= :ind and img_content is null or img_content = '\"\"'";
        $bind['ind'] = $minIndice;
        $res = _query($sql, $bind);
        return @$res[0]['ind'] ?: 0;
    }
    
    public static function search( $text = "" ){
        $sql = "select id from mtg_carta 
                 where ";
        
        $exp = explode(" ", trim($text));
        $i = 0;
        foreach( $exp as $row ){
            $i++;
            $cond1[] = " upper(name) like upper(:text{$i}) ";
            $cond2[] = " upper(name_pt) like upper(:text{$i}) ";
            $cond3[] = " upper(descr) like upper(:text{$i}) ";
            $cond4[] = " upper(descr_pt) like upper(:text{$i}) ";
            $bind["text{$i}"] = "%{$row}%";
        }
        
        $cond[] = implode(" and ", $cond1);
        $cond[] = implode(" and ", $cond2);
        $cond[] = implode(" and ", $cond3);
        $cond[] = implode(" and ", $cond4);
        
        $sql .= implode(" or ", $cond);
        
        $res = _query($sql, $bind);
        
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) ? $dados : false;
    }
    
}