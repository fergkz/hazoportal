<?php

namespace Model\Pagina;

use Core\Model\User as User;

class Template extends \MVC\Model{
    
    protected $ID;
    protected $titulo;
    protected $descricao;
    protected $cadastroUserID;
    protected $cadastroUserObj;
    protected $cadastroData;
    protected $fileName;
    protected $fileContent;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setCadastroUserID( $cadastroUserID ){
        $this->cadastroUserID = $cadastroUserID;
        return $this;
    }

    public function getCadastroUserID(){
        return $this->cadastroUserID;
    }

    public function setCadastroUserObj( $cadastroUserObj ){
        $this->cadastroUserObj = $cadastroUserObj;
        if( $this->cadastroUserObj ){
            $this->cadastroUserID = $this->cadastroUserObj->getID();
        }
        return $this;
    }

    public function getCadastroUserObj(){
        if( !$this->cadastroUserObj ){
            $this->cadastroUserObj = User::getByID($this->cadastroUserID);
        }
        return $this->cadastroUserObj;
    }

    public function setCadastroData( $cadastroData ){
        $this->cadastroData = $cadastroData;
        return $this;
    }

    public function getCadastroData(){
        return $this->cadastroData;
    }

    public function setFileName( $fileName ){
        $this->fileName = $fileName;
        return $this;
    }

    public function getFileName(){
        return $this->fileName;
    }

    public function setFileContent( $fileContent ){
        $this->fileContent = $fileContent;
        return $this;
    }

    public function getFileContent(){
        return $this->fileContent;
    }
    
    public static function getPath(){
        return path.ds.trim(str_replace(array('\\','/'), ds, pagina_template_path), "\\..\/").ds;
    }
    
    protected function triggerBeforeInsertUpdate(){
        $filename = self::getPath().$this->new->fileName;
        if( !file_exists($filename) || !is_file($filename) ){
            _raise("O arquivo do template não foi encontrado");
        }
        $this->new->fileContent = file_get_contents($filename);
        $this->new->cadastroData = date("Y-m-d H:i:s");
        $this->new->cadastroUserID = User::online()->getID();
    }
    
}