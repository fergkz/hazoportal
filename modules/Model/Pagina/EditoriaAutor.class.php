<?php

namespace Model\Pagina;

use Model\Pessoa\Autor as Autor;
use Model\Pagina\Editoria as Editoria;

class EditoriaAutor extends \MVC\Model{
    
    protected $ID;
    protected $editoriaID;
    protected $editoriaObj;
    protected $autorID;
    protected $autorObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setEditoriaID( $editoriaID ){
        $this->editoriaID = $editoriaID;
        return $this;
    }

    public function getEditoriaID(){
        return $this->editoriaID;
    }

    public function setEditoriaObj( $editoriaObj ){
        $this->editoriaObj = $editoriaObj;
        if( $this->editoriaObj ){
            $this->editoriaID = $this->editoriaObj->getID();
        }
        return $this;
    }

    public function getEditoriaObj(){
        if( !$this->editoriaObj ){
            $this->editoriaObj = Editoria::getByID($this->editoriaID);
        }
        return $this->editoriaObj;
    }

    public function setAutorID( $autorID ){
        $this->autorID = $autorID;
        return $this;
    }

    public function getAutorID(){
        return $this->autorID;
    }

    public function setAutorObj( $autorObj ){
        $this->autorObj = $autorObj;
        if( $this->autorObj ){
            $this->autorID = $this->autorObj->getID();
        }
        return $this;
    }

    public function getAutorObj(){
        if( !$this->autorObj ){
            $this->autorObj = Autor::getByID($this->autorID);
        }
        return $this->autorObj;
    }
    
}