<?php

namespace Model\Pagina;

use Model\Pagina\Pagina as Pagina;

class Editoria extends \MVC\Model{
    
    protected $ID;
    protected $nome;
    protected $slug;
    protected $status;
    protected $parentEditoriaID;
    protected $parentEditoriaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setNome( $nome ){
        $this->nome = $nome;
        return $this;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setParentEditoriaID( $parentEditoriaID ){
        $this->parentEditoriaID = $parentEditoriaID;
        return $this;
    }

    public function getParentEditoriaID(){
        return $this->parentEditoriaID;
    }

    public function setParentEditoriaObj( $parentEditoriaObj ){
        $this->parentEditoriaObj = $parentEditoriaObj;
        if( $this->parentEditoriaObj ){
            $this->parentEditoriaID = $this->parentEditoriaObj->getID();
        }
        return $this;
    }

    public function getParentEditoriaObj(){
        if( !$this->parentEditoriaObj ){
            $this->parentEditoriaObj = self::getByID($this->parentEditoriaID);
        }
        return $this->parentEditoriaObj;
    }
    
    public function getSubEditorias(){
        return self::listaByParent($this);
    }
    
    public function getAutores(){
        $bind['this_id'] = $this->ID;
        $sql = "select id from pag_autor_editoria where editoria_id = :this_id";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = EditoriaAutor::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : null;
    }
    
    public static function getBySlug( $slug ){
        $bind['slug'] = urldecode($slug);
        $sql = "select id from pag_editoria where slug = :slug";
        $res = _query($sql, $bind);
        return self::getByID(@$res[0]['id']);
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( $this->new->ID && $this->new->ID == $this->new->parentEditoriaID ){
            _raise("Uma editoria não pode ser filha dela mesma");
        }
    }
    
    protected function triggerBeforeDelete(){
        $autores = $this->getAutores();
        if( $autores ){
            foreach( $autores as $Autor ){
                $Autor->save('D');
            }
        }
    }
    
    public function listaPaginas( $somenteAtivas = true, $qtde = false, $params = array() ){
        if( $qtde ){
            $paginas = Pagina::listaByEditoria($this);
            return $paginas ? count($paginas) : 0;
        }
        return Pagina::listaByEditoria($this, $somenteAtivas, $params);
    }
    
    public static function listaByParent( $parent = null, $listaSubs = null ){
        $bind = array();
        
        if( $parent ){
            $parentID = is_object($parent) ? $parent->getID() : $parent;
            $bind['this_id'] = $parentID;
            $sql = "select id from pag_editoria where parent_editoria_id = :this_id";
        }else{
            $sql = "select id from pag_editoria where parent_editoria_id is null";
        }
        
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
            if( $listaSubs ){
                $subs = self::listaByParent($row['id'], true);
                if( $subs ){
                    $dados = array_merge($dados, $subs);
                }
            }
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
    public static function listaByAutor( $autor = null, $status = 'A' ){
        
        $bind['autor_id'] = is_object($autor) ? $autor->getID() : $autor;
        
        $sql = "select e.id 
                  from pag_editoria e
                  join pag_autor_editoria ae on ae.editoria_id = e.id
                 where ae.autor_id = :autor_id";
        
        if( $status ){
            $bind['status'] = $status;
            $sql .= " and e.status = :status";
        }
        
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : null;
        
    }
    
    public static function getArrayParents( $editoria ){
        
        $editoriaID = is_object($editoria) ? $editoria->getID() : $editoria;
        
        $bind['editoria_id'] = $editoriaID;
        $sql = "select parent_editoria_id from pag_editoria where id = :editoria_id";
        
        $res = _query($sql, $bind);
        $EditoriaPai = self::getByID( @$res[0]['parent_editoria_id'] );
        
        
        $responseParents = array();
        if( $EditoriaPai ){
            $parents = self::getArrayParents($EditoriaPai);
            if( $parents && is_array($parents) && count($parents) > 0 ){
                $responseParents = array_merge($parents, $responseParents);
            }
            
            $responseParents[] = $EditoriaPai;
        }
        
        return $responseParents;
        
    }
    
    public function getParentsLink()
    {
        $array = self::getArrayParents($this);
        $tmp = null;
        foreach( $array as $Obj ){
            $tmp[] = $Obj->getSlug();
        }
        return ($tmp ? implode("/",$tmp).'/' : '').$this->getSlug();
    }
    
}