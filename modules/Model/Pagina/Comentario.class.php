<?php

namespace Model\Pagina;

use Model\Pagina\Pagina as Pagina;
use Core\Model\User as User;

class Comentario extends \MVC\Model{
    
    protected $ID;
    protected $paginaID;
    protected $paginaObj;
    protected $userID;
    protected $userObj;
    protected $data;
    protected $status;
    protected $texto;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setPaginaID( $paginaID ){
        $this->paginaID = $paginaID;
        return $this;
    }

    public function getPaginaID(){
        return $this->paginaID;
    }

    public function setPaginaObj( $paginaObj ){
        $this->paginaObj = $paginaObj;
        if( $this->paginaObj ){
            $this->paginaID = $this->paginaObj->getID();
        }
        return $this;
    }

    public function getPaginaObj(){
        if( !$this->paginaObj ){
            $this->paginaObj = Pagina::getByID($this->paginaID);
        }
        return $this->paginaObj;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function setData( $data ){
        $this->data = $data;
        return $this;
    }

    public function getData(){
        return $this->data;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }
    
    public function setTexto( $texto ){
        $this->texto = $texto;
        return $this;
    }

    public function getTexto(){
        return $this->texto;
    }
    
    protected function triggerBeforeInsert(){
        $this->new->data = date("Y-m-d H:i:s");
        $this->new->userID = User::online()->getID();
    }
    
    protected function triggerBeforeInsertUpdate(){
        
        $this->new->texto = trim($this->new->texto);
        
        $textoLenght = strlen($this->new->texto);
        
        
        if( $textoLenght > 140 ){
            _raise('O comentário deve conter no máximo 140 caracteres');
        }elseif( $textoLenght < 20 ){
            _raise('O comentário deve conter no mínimo 20 caracteres');
        }
        
    }
    
    public static function listaComentariosAtivosByPagina( $pagina, $ultimoComentario = null ){
        
        $bind['pagina_id'] = is_object($pagina) ? $pagina->getID() : $pagina;

        $sql = " select pc.id 
                   from pag_comentario pc
                  where pc.pagina_id = :pagina_id 
                    and pc.status = 'A' ";
        
        if( $ultimoComentario ){
            $bind['last_comment'] = $ultimoComentario;
            $sql .= "and pc.id > :last_commment";
        }
        
        $sql .= "order by pc.data desc";
        
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
}