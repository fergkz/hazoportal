<?php

namespace Model\Pagina;

use Model\Pagina\Pagina as Pagina;

class PaginaBloco extends \MVC\Model{
    
    protected $ID;
    protected $sequence;
    protected $paginaID;
    protected $paginaObj;
    protected $cadastroData;
    protected $status;
    protected $titulo;
    protected $conteudoJson;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setSequence( $sequence ){
        $this->sequence = $sequence;
        return $this;
    }

    public function getSequence(){
        return $this->sequence;
    }

    public function setPaginaID( $paginaID ){
        $this->paginaID = $paginaID;
        return $this;
    }

    public function getPaginaID(){
        return $this->paginaID;
    }

    public function setPaginaObj( $paginaObj ){
        $this->paginaObj = $paginaObj;
        if( $this->paginaObj ){
            $this->paginaID = $this->paginaObj->getID();
        }
        return $this;
    }

    public function getPaginaObj(){
        if( !$this->paginaObj ){
            $this->paginaObj = Pagina::getByID($this->paginaID);
        }
        return $this->paginaObj;
    }

    public function setCadastroData( $cadastroData ){
        $this->cadastroData = $cadastroData;
        return $this;
    }

    public function getCadastroData(){
        return $this->cadastroData;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }
    
    public function clearConteudo(){
        $this->conteudoJson = json_encode(array());
    }
    
    public function getConteudoArray(){
        return $this->conteudoJson ? json_decode($this->conteudoJson, true) : array();
    }

    public function setConteudoArray( $conteudoArray ){
        $this->conteudoJson = is_array($conteudoArray) ? json_encode($conteudoArray) : array();
    }

    public function setConteudo( $chave, $conteudo = null ){
        $conteudoArray = $this->getConteudoArray();
        $conteudoArray[$chave] = $conteudo;
        $this->setConteudoArray($conteudoArray);
    }

    public function getConteudo( $chave ){
        if( $this->conteudoJson ){
            $conteudoArray = json_decode($this->conteudoJson, true);
            return @$conteudoArray[$chave];
        }
        return null;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !$this->new->cadastroData ){
            $this->new->cadastroData = date('Y-m-d H:i:s');
        }
        if( !$this->new->status ){
            $this->new->status = 'A';
        }
    }

    public static function listaByPagina( $pagina ){
        if( is_numeric($pagina) ){
            $bind['pagina_id'] = Pagina::getByID($pagina);
        }elseif( is_string($pagina) ){
            $Pagina = Pagina::getBySlug($pagina);
            $bind['pagina_id'] = $Pagina->getID();
        }elseif( is_object($pagina) ){
            $bind['pagina_id'] = $pagina->getID();
        }
        $sql = "select id from pag_pagina_bloco where pagina_id = :pagina_id order by sequence, data_cadastro";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : null;
    }

}