<?php

namespace Model\Pagina;

use Model\Pagina\Editoria   as Editoria;
use Model\Pagina\Template   as Template;
use Core\Model\User         as User;
use Lib\Format              as Format;
use Model\Pessoa\Autor      as Autor;

class Pagina extends \MVC\Model{
    
    protected $ID;
    protected $titulo;
    protected $slug;
    protected $publicacaoDataInicio;
    protected $publicacaoDataFinal;
    protected $status;
    protected $cadastroData;
    protected $cadastroUserID;
    protected $cadastroUserObj;
    protected $modificacaoData;
    protected $modificacaoUserID;
    protected $modificacaoUserObj;
    protected $parentPaginaID;
    protected $parentPaginaObj;
    protected $editoriaID;
    protected $editoriaObj;
    protected $templateID;
    protected $templateObj;
    protected $conteudoJson;
    protected $contador;
    
    private $conteudos;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }
    
    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setPublicacaoDataInicio( $publicacaoDataInicio ){
        $this->publicacaoDataInicio = Format::dataToDb($publicacaoDataInicio);
        return $this;
    }

    public function getPublicacaoDataInicio(){
        return Format::dataToBr($this->publicacaoDataInicio);
    }

    public function setPublicacaoDataFinal( $publicacaoDataFinal ){
        $this->publicacaoDataFinal = $publicacaoDataFinal ? Format::dataToDb($publicacaoDataFinal) : null;
        return $this;
    }

    public function getPublicacaoDataFinal(){
        return $this->publicacaoDataFinal ? Format::dataToBr($this->publicacaoDataFinal) : null;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setCadastroData( $cadastroData ){
        $this->cadastroData = $cadastroData;
        return $this;
    }

    public function getCadastroData( $format = null ){
        
        if( $format ){
            
            $dataMes = array(
                '01' => 'Janeiro',
                '02' => 'Fevereiro',
                '03' => 'Março',
                '04' => 'Abril',
                '05' => 'Maio',
                '06' => 'Junho',
                '07' => 'Julho',
                '08' => 'Agosto',
                '09' => 'Setembro',
                '10' => 'Outubro',
                '11' => 'Novembro',
                '12' => 'Dezembro',
            );
            
            $data1 = explode(" ", $this->cadastroData);
            $data2 = explode("-", $data1[0]);
            
            switch( $format ){
                case 'dia':
                    return $data2['2'];
                    break;
                case 'ano':
                    return $data2['0'];
                    break;
                case 'mes_descr':
                    return $dataMes[$data2['1']];
                    break;
            }
            
        }
        
        return $this->cadastroData;
    }

    public function setCadastroUserID( $cadastroUserID ){
        $this->cadastroUserID = $cadastroUserID;
        return $this;
    }

    public function getCadastroUserID(){
        return $this->cadastroUserID;
    }

    public function setCadastroUserObj( $cadastroUserObj ){
        $this->cadastroUserObj = $cadastroUserObj;
        if( $this->cadastroUserObj ){
            $this->cadastroUserID = $this->cadastroUserObj->getID();
        }
        return $this;
    }

    public function getCadastroUserObj(){
        if( !$this->cadastroUserObj ){
            $this->cadastroUserObj = User::getByID($this->cadastroUserID);
        }
        return $this->cadastroUserObj;
    }

    public function setModificacaoData( $modificacaoData ){
        $this->modificacaoData = $modificacaoData;
        return $this;
    }

    public function getModificacaoData(){
        return $this->modificacaoData;
    }

    public function setModificacaoUserID( $modificacaoUserID ){
        $this->modificacaoUserID = $modificacaoUserID;
        return $this;
    }

    public function getModificacaoUserID(){
        return $this->modificacaoUserID;
    }
    
    public function setModificacaoUserObj( $modificacaoUserObj ){
        $this->modificacaoUserObj = $modificacaoUserObj;
        if( $this->modificacaoUserObj ){
            $this->modificacaoUserID = $this->modificacaoUserObj->getID();
        }
        return $this;
    }

    public function getModificacaoUserObj(){
        if( !$this->modificacaoUserObj ){
            $this->modificacaoUserObj = User::getByID($this->modificacaoUserID);
        }
        return $this->modificacaoUserObj;
    }

    public function setParentPaginaID( $parentPaginaID ){
        $this->parentPaginaID = $parentPaginaID;
        return $this;
    }

    public function getParentPaginaID(){
        return $this->parentPaginaID;
    }

    public function setParentPaginaObj( $parentPaginaObj ){
        $this->parentPaginaObj = $parentPaginaObj;
        if( $this->parentPaginaObj ){
            $this->parentPaginaID = $this->parentPaginaObj->getID();
        }
        return $this;
    }

    public function getParentPaginaObj(){
        if( !$this->parentPaginaObj ){
            $this->parentPaginaObj = Pagina::getByID($this->parentPaginaID);
        }
        return $this->parentPaginaObj;
    }
    
    public function setEditoriaID( $editoriaID ){
        $this->editoriaID = $editoriaID;
        return $this;
    }

    public function getEditoriaID(){
        return $this->editoriaID;
    }

    public function setEditoriaObj( $editoriaObj ){
        $this->editoriaObj = $editoriaObj;
        if( $this->editoriaObj ){
            $this->editoriaID = $this->editoriaObj->getID();
        }
        return $this;
    }

    public function getEditoriaObj(){
        if( !$this->editoriaObj ){
            $this->editoriaObj = Editoria::getByID($this->editoriaID);
        }
        return $this->editoriaObj;
    }
    
    public function setTemplateID( $templateID ){
        $this->templateID = $templateID;
        return $this;
    }

    public function getTemplateID(){
        return $this->templateID;
    }

    public function setTemplateObj( $templateObj ){
        $this->templateObj = $templateObj;
        if( $this->templateObj ){
            $this->templateID = $this->templateObj->getID();
        }
        return $this;
    }

    public function getTemplateObj(){
        if( !$this->templateObj ){
            $this->templateObj = Template::getByID($this->templateID);
        }
        return $this->templateObj;
    }
    
    public function clearConteudo(){
        $this->conteudoJson = json_encode(array());
    }
    
    public function getConteudoArray(){
        return $this->conteudoJson ? json_decode($this->conteudoJson, true) : array();
    }
    
    public function setConteudoArray( $conteudoArray ){
        $this->conteudoJson = is_array($conteudoArray) ? json_encode($conteudoArray) : array();
    }
    
    public function setConteudo( $chave, $conteudo = null ){
        $conteudoArray = $this->getConteudoArray();
        $conteudoArray[$chave] = $conteudo;
        $this->setConteudoArray($conteudoArray);
    }
    
    public function getConteudo( $chave ){
        if( $this->conteudoJson ){
            $conteudoArray = json_decode($this->conteudoJson, true);
            return @$conteudoArray[$chave];
        }
        return null;
    }
    
    public function getContador(){
        return $this->contador;
    }
    
    public function adicionaContador(){
        $this->contador = (int)$this->contador + 1;
        return $this->save();
    }
    
    public static function getTopContador( $limit = '5' ){
        $sql = "select id from pag_pagina order by contador desc limit {$limit}";
        $res = _query($sql);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : null;
    }
    
    protected function triggerBeforeInsert(){
        $this->new->cadastroData = date('Y-m-d H:i:s');
        $this->new->cadastroUserID = User::online()->getID();
    }
    
    protected function triggerBeforeInsertUpdate(){
        $this->new->titulo = trim($this->new->titulo);
        if( !$this->new->publicacaoDataInicio ){
            $this->new->publicacaoDataInicio = $this->new->cadastroData;
        }
        if( $this->new->slug ){
            $this->new->slug = trim($this->new->slug);
            $slug = trim(Format::strigToUrl($this->new->slug));
            if( $slug !== $this->new->slug ){
                _raise("A slug informada é inválida");
            }
        }else{
            $this->new->slug = trim(Format::strigToUrl($this->new->titulo));
        }
        if( !$this->new->conteudoJson ){
            $this->new->conteudoJson = json_encode(array());
        }
    }
    
    protected function triggerBeforeDelete(){
        $blocos = PaginaBloco::listaByPagina($this);
        if( $blocos ){
            foreach( $blocos as $Bloco ){
                $Bloco->save('D');
            }
        }
    }
    
    public static function getBySlug( $slug ){
        $bind['slug'] = $slug;
        $sql = "select id from pag_pagina where slug = :slug";
        $res = _query($sql, $bind);
        return self::getByID(@$res[0]['id']);
    }
    
    public function getArrayEditorias(){
        $parents = Editoria::getArrayParents($this->editoriaID) ?: array();
        $parents[] = $this->getEditoriaObj();
        return $parents;
    }
    
    public static function pesquisa( $get, $config = array() ){
        
        $bind  = array();
        $where = array();
        $order = array();
        
        if( !empty($get['texto']) ){
            $textos = explode(" ", trim($get['texto']));
            if( $textos and count($textos) > 0 ){
                $i = 0;
                $p = array();
                foreach( $textos as $row ){
                    $i++;
                    
                     $p[] = "(   p.titulo like upper(:cons_{$i})
                               or
                                 p.slug like upper(:cons_{$i}) )";
                    $bind["cons_{$i}"] = "%{$row}%";
                    
                }
                $where[] = implode(' and ', $p);
            }
        }
        
        if( !empty($config['autor_permissao']) ){
            $bind['autor_permissao'] = is_object($config['autor_permissao']) ? $config['autor_permissao']->getID() : $config['autor_permissao'];
            $where[] = "p.editoria_id in ( 
                            select ae.id
                              from pag_editoria ae
                              join pag_autor_editoria aae on aae.editoria_id = ae.id
                             where aae.autor_id = :autor_permissao
                        )";
        }
        
        switch( @$get['order'] ){
            default:
            case 'DATA_DESC':
                $order[] = "p.cad_data desc";
                break;
            case 'TITULO':
                $order[] = "p.titulo";
                break;
        }
        
        $sql = " select p.id
                   from pag_pagina p ";

        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        if( count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }
        
        $cont = _query("select count(*) as cont from ( $sql ) t", $bind);
        $dados['cont'] = $cont[0]['cont'];
        
        $pg = (int)(!empty($get['pg']) ? $get['pg'] : 1);
        
        $dados['max_links'] = ceil($dados['cont']/10);
        
        if( !empty($get['pg']) ){
            $ini = ($get['pg'] ?: 1) * 10 - 10;
            $sql .= " limit {$ini}, 11";
        }else{
            $sql .= " limit 0, 11";
        }
        
        $res = _query($sql, $bind);
        
        if( $res and count($res) > 10 ){
            $dados['nex'] = true;
            array_pop($res);
        }
        
        foreach( @$res as $row ){
            $dados['paginas'][] = self::getByID($row['id']);
        }
        
        return $dados;
        
    }
    
    public static function listaByEditoria( $editoria = null, $somenteAtivas = true, $params = array() ){
        
        $where = array();
        $bind = array();
        $limit = "";
        
        $editoriaID = is_object($editoria) ? $editoria->getID() : $editoria;
        $bind['editoria_id'] = $editoriaID;
        
        
        if( $editoria ){
            $sql = "select id from pag_pagina where editoria_id = :editoria_id ";
        }else{
            $sql = "select id from pag_pagina where editoria_id is not null ";
        }
            
        if( $somenteAtivas ){
            $sql .= " and status = 'A'";
        }
        
        $resCont = _query(" select count(*) as cont from ({$sql}) t", $bind);
        $qtdePaginas = $resCont[0]['cont'];
        
        if( !empty($params['pagina']) ){
            
            $params['pagina'] = $params['pagina'];
            
            $qtde = empty($params['qtde_pag']) ? 3 : $params['qtde_pag'];
            
            if( $params['pagina'] == 1 ){
                $limit = " limit 0, ".$qtde;
            }else{
                $limit = " limit ".(($params['pagina']-1) * ($qtde) ).", ".$qtde;
            }
            
            if( $qtdePaginas <= $qtde ){
                $data['qtde_paginas'] = 1;
            }else{
                $data['qtde_paginas'] = ceil( $qtdePaginas / $qtde );
            }
            
        }
        
        $sql .= " order by cad_data desc {$limit}";
        
        $res = _query($sql, $bind);

        $dados = array();
        
        foreach( $res as $row ){
            $Pagina = self::getByID($row['id']);
            $dados[$Pagina->getCadastroData()] = self::getByID($row['id']);
        }
        
        if( $editoria ){
            $Editoria = Editoria::getByID($editoriaID);
            $Subs = $Editoria->getSubEditorias();
        }else{
            $Subs = Editoria::listaByParent();
        }
            
        if( $Subs ){
            foreach( $Subs as $Editoria ){
                $paginas = self::listaByEditoria($Editoria);
                if( $paginas ){
                    $dados = array_merge($paginas, $dados);
                }
            }
        }
        
        $data['paginas'] = $dados;
        
        return $data;
//        return count($dados) > 0 ? $dados : null;
    }
    
    public static function listaTodasByEditoria( $editoria = null, $somenteAtivas = true, $paramsPaginacao = array() ){
        $bind = array();
        $dados = array(
            'quantidade' => 0,
            'paginas'    => null
        );
        $limit = "";
        
        
        if( $editoria ){
            $whereEditoria[] = is_object($editoria) ? $editoria->getID() : $editoria;
            
            $lista = Editoria::listaByParent($editoria, true);
            if( $lista ){
                foreach( $lista as $Editoria ){
                    $whereEditoria[] = $Editoria->getID();
                }
            }
            
            $where[] = "p.editoria_id in (".implode(',',$whereEditoria).")";
        }
        
        if( $somenteAtivas ){
            $where[] = "p.status = 'A' and e.status = 'A'";
        }
        
        $sql = "select p.id from pag_pagina p join pag_editoria e on e.id = p.editoria_id";
        
        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }

        ### Contador
        $sqlCont = "select count(*) as cont from ({$sql}) t";
        $resCont = _query($sqlCont, $bind);
        $dados['qtde_total'] = $resCont[0]['cont'];
        
        if( !empty($paramsPaginacao['somente_qtde']) ){
            return $dados['qtde_total'];
        }
        
        ### endContador
        
        if( !empty($paramsPaginacao['pagina']) ){
            
            $qtde = empty($paramsPaginacao['qtde_pag']) ? 3 : $paramsPaginacao['qtde_pag'];
            
            if( $paramsPaginacao['pagina'] == 1 ){
                $limit = " limit 0, ".$qtde;
            }else{
                $limit = " limit ".(($paramsPaginacao['pagina']-1) * ($qtde) ).", ".$qtde;
            }
            
            if( $dados['qtde_total'] <= $qtde ){
                $dados['qtde_paginas'] = 1;
            }else{
                $dados['qtde_paginas'] = ceil( $dados['qtde_total'] / $qtde );
            }
            
        }
        
        $sql .= " order by cad_data desc {$limit}";
        
        $res = _query($sql, $bind);
        foreach( $res as $row ){
            $Pagina = self::getByID($row['id']);
            $dados['paginas'][$Pagina->getCadastroData()] = $Pagina;
        }
        
        return $dados;
        
    }
    
    public function permiteLogadoAlteracao(){
            
        $Autor = Autor::getOnline();
        $User = User::online();
        
        if( $this->cadastroUserID === $User->getID() ){
            return true;
        }
        
        if( !$Autor ){
            return false;
        }
        
        $autoresEditoria = $this->getEditoriaObj()->getAutores();

        $grant = false;
        if( $autoresEditoria ){
            foreach( $autoresEditoria as $AutorEditoria ){
                if( $AutorEditoria->getAutorID() == $Autor->getID() ){
                    $grant = true;
                    break;
                }
            }
        }

        if( !$grant ){
            
            return false;
        }
        
        return true;
    }
    
}