<?php

namespace Model\Projetos;

use Model\Projetos\Demanda as Demanda;
use Core\Model\User as User;

class DemandaGerente extends \MVC\Model{
    
    protected $ID;
    protected $demandaID;
    protected $demandaObj;
    protected $userID;
    protected $userObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setDemandaID( $demandaID ){
        $this->demandaID = $demandaID;
        return $this;
    }

    public function getDemandaID(){
        return $this->demandaID;
    }

    public function setDemandaObj( $demandaObj ){
        $this->demandaObj = $demandaObj;
        if( $this->demandaObj ){
            $this->demandaID = $this->demandaObj->getID();
        }
        return $this;
    }

    public function getDemandaObj(){
        if( !$this->demandaObj ){
            $this->demandaObj = Demanda::getByID($this->demandaID);
        }
        return $this->demandaObj;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
    public static function listaByDemanda( $demanda ){
        $Demanda = is_object($demanda) ? $demanda : Demanda::getByID($demanda);
        $sql = "select id from proj_demanda_gerente where demanda_id = :demanda_id";
        $bind['demanda_id'] = $Demanda->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) ? $dados : false;
    }
    
    public static function clearByDemanda( $demanda ){
        $gerentes = self::listaByDemanda($demanda);
        if( $gerentes ){
            foreach( $gerentes as $Gerente ){
                $Gerente->save("D");
            }
        }
    }
    
    public static function isGerenteByDemanda( $demanda, $gerenteUser = null ){
        if( $gerenteUser ){
            $User = is_object($gerenteUser) ? $gerenteUser : User::getByID($gerenteUser);
        }else{
            $User = User::online();
        }
        if( $User->getGroupObj()->getMode() == "S" ){
            return true;
        }
        $gerentes = self::listaByDemanda($demanda);
        if( $gerentes ){
            foreach( $gerentes as $Gerente ){
                if( $Gerente->getUserID() == $User->getID() ){
                    return true;
                }
            }
        }
    }
    
    public static function isGerenteLogadoByDemanda( $demanda ){
        return self::isGerenteByDemanda($demanda);
//        if( User::online()->getGroupObj()->getMode() == "S" ){
//            return true;
//        }
//        $gerentes = self::listaByDemanda($demanda);
//        if( $gerentes ){
//            foreach( $gerentes as $Gerente ){
//                if( $Gerente->getUserID() == User::online()->getID() ){
//                    return true;
//                }
//            }
//        }
//        return false;
    }
    
}