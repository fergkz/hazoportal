<?php

namespace Model\Projetos;

use Model\Projetos\Atividade as Atividade;
use Core\Model\User as User;
use Model\Projetos\AtividadeEtapa as Etapa;
use Lib\Format as Format;

class Operacao extends \MVC\Model{
    
    protected $ID;
    protected $atividadeID;
    protected $atividadeObj;
    protected $userID;
    protected $userObj;
    protected $dataInicio;
    protected $dataFinal;
    protected $descricao;
    protected $atividadeEtapaID;
    protected $atividadeEtapaObj;
    
    protected $anexos;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setAtividadeID( $atividadeID ){
        $this->atividadeID = $atividadeID;
        return $this;
    }

    public function getAtividadeID(){
        return $this->atividadeID;
    }

    public function setAtividadeObj( $atividadeObj ){
        $this->atividadeObj = $atividadeObj;
        if( $this->atividadeObj ){
            $this->atividadeID = $this->atividadeObj->getID();
        }
        return $this;
    }

    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = Atividade::getByID($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function setDataInicio( $dataInicio ){
        $this->dataInicio = $dataInicio;
        return $this;
    }

    public function getDataInicio(){
        return $this->dataInicio;
    }

    public function setDataFinal( $dataFinal ){
        $this->dataFinal = $dataFinal;
        return $this;
    }

    public function getDataFinal(){
        return $this->dataFinal;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    
    public function setAtividadeEtapaID( $atividadeEtapaID ){
        $this->atividadeEtapaID = $atividadeEtapaID;
        return $this;
    }

    public function getAtividadeEtapaID(){
        return $this->atividadeEtapaID;
    }

    public function setAtividadeEtapaObj( $atividadeEtapaObj ){
        $this->atividadeEtapaObj = $atividadeEtapaObj;
        if( $this->atividadeEtapaObj ){
            $this->atividadeEtapaID = $this->atividadeEtapaObj->getID();
        }
        return $this;
    }

    public function getAtividadeEtapaObj(){
        if( !$this->atividadeEtapaObj ){
            $this->atividadeEtapaObj = Etapa::getByID($this->atividadeEtapaID);
        }
        return $this->atividadeEtapaObj;
    }
    
    public function getAnexos(){
        if( !$this->anexos ){
            $this->anexos = Anexo::listByOperacao($this, false);
        }
        return $this->anexos;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !$this->new->dataInicio ){
            $this->new->dataInicio = date("Y-m-d H:i:s");
        }
        if( !$this->new->userID ){
            $this->new->userID = User::online()->getID();
        }
        if( !$this->new->atividadeEtapaID && $this->new->atividadeID ){
            $this->new->atividadeEtapaID = Atividade::getInstance($this->new->atividadeID)->getEtapaID();
        }
    }
    
    protected function triggerBeforeDelete(){
        Anexo::deleteByOperacao($this);
    }
    
    public function abre(){
        return $this->save();
    }
    
    public function fecha(){
        $this->dataFinal = date("Y-m-d H:i:s");
        return $this->save();
    }
    
    public static function getEmAbertoByAtividadeLogado( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : Atividade::getByID($atividade);
        $User = User::online();
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id
                   and user_id = :user_id
                   and data_final is null";
        $bind['atividade_id'] = $Atividade->getID();
        $bind['user_id']      = $User->getID();
        $res = _query($sql, $bind);
        if( @$res[0]['id'] ){
            return self::getByID($res[0]['id']);
        }else{
            return false;
        }
    }
    
    public static function getEmAbertoByAtividade( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : Atividade::getByID($atividade);
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id
                   and data_final is null";
        $bind['atividade_id'] = $Atividade->getID();
        $res = _query($sql, $bind);
        if( @$res[0]['id'] ){
            return self::getByID($res[0]['id']);
        }else{
            return false;
        }
    }
    
    public static function listaByAtividade( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : Atividade::getByID($atividade);
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id 
                 order by data_inicio asc";
        $bind['atividade_id'] = $Atividade->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByAtividade( $atividade ){
         $operacoes = self::listaByAtividade($atividade);
         if( $operacoes ){
             foreach( $operacoes as $Operacao ){
                 if( !$Operacao->save('D') ){
                     return false;
                 }
             }
         }
         return true;
    }
    
    public function getTempoOperacao( $formatoRetorno = "H:i:s" ){
        $operacoes = Operacao::listaByAtividade($this);
        $tempo = Format::subDatas(_coalesce($this->getDataFinal(), date("Y-m-d H:i:s")), $this->getDataInicio(), "s");
        switch( $formatoRetorno ){
            case "H:i:s":
                return Format::formataSegundosHoras($tempo);
                break;
            case "s":
                return $tempo;
                break;
            case "m":
                return $tempo/60;
                break;
        }
    }
}