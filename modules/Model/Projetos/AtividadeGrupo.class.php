<?php

namespace Model\Projetos;

use Model\Projetos\Atividade as Atividade;
use Core\Model\Group as Group;
use Core\Model\User as User;

class AtividadeGrupo extends \MVC\Model{
    
    protected $ID;
    protected $atividadeID;
    protected $atividadeObj;
    protected $groupID;
    protected $groupObj;
    protected $userID;
    protected $userObj;
    protected $execucao; # [H]omologação   [D]esenvolvimento
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setAtividadeID( $atividadeID ){
        $this->atividadeID = $atividadeID;
        return $this;
    }

    public function getAtividadeID(){
        return $this->atividadeID;
    }

    public function setAtividadeObj( $atividadeObj ){
        $this->atividadeObj = $atividadeObj;
        if( $this->atividadeObj ){
            $this->atividadeID = $this->atividadeObj->getID();
        }
        return $this;
    }

    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = Atividade::getByID($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function setGroupID( $groupID ){
        $this->groupID = $groupID;
        return $this;
    }

    public function getGroupID(){
        return $this->groupID;
    }

    public function setGroupObj( $groupObj ){
        $this->groupObj = $groupObj;
        if( $this->groupObj ){
            $this->groupID = $this->groupObj->getID();
        }
        return $this;
    }

    public function getGroupObj(){
        if( !$this->groupObj ){
            $this->groupObj = Group::getByID($this->groupID);
        }
        return $this->groupObj;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function setExecucao( $execucao ){
        $this->execucao = $execucao;
        return $this;
    }

    public function getExecucao(){
        return $this->execucao;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !$this->groupID and !$this->userID ){
            _raise("O grupo ou usuário deve ser informado para o grupo de execução");
        }
    }
    
    public static function listaByAtividade( $atividade = null ){
        $Atividade = is_object($atividade) ? $atividade : Atividade::getByID($atividade);
        
        if( !$Atividade or !$Atividade->getID() ){
            return false;
        }
        
        $sql = "select id from proj_atividade_grupo where atividade_id = :atividade_id";
        $bind['atividade_id'] = $Atividade->getID();
        $res = _query($sql, $bind);
        
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getInstance($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByAtividade( $atividade = null ){
        $atividades = self::listaByAtividade($atividade);
        if( !$atividades ){
            return false;
        }
        foreach( $atividades as $Atividade ){
            if( !$Atividade->save("D") ){
                return false;
            }
        }
        return true;
    }
    
}