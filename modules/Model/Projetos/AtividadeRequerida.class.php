<?php

namespace Model\Projetos;

use Model\Projetos\Atividade as Atividade;

class AtividadeRequerida extends \MVC\Model{
    
    protected $ID;
    protected $atividadeRequeridaID;
    protected $atividadeRequeridaObj;
    protected $atividadeDependenteID;
    protected $atividadeDependenteObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setAtividadeRequeridaID( $atividadeRequeridaID ){
        $this->atividadeRequeridaID = $atividadeRequeridaID;
        return $this;
    }

    public function getAtividadeRequeridaID(){
        return $this->atividadeRequeridaID;
    }

    public function setAtividadeRequeridaObj( $atividadeRequeridaObj ){
        $this->atividadeRequeridaObj = $atividadeRequeridaObj;
        if( $this->atividadeRequeridaObj ){
            $this->atividadeRequeridaID = $this->atividadeRequeridaObj->getID();
        }
        return $this;
    }

    public function getAtividadeRequeridaObj(){
        if( !$this->atividadeRequeridaObj ){
            $this->atividadeRequeridaObj = Atividade::getByID($this->atividadeRequeridaID);
        }
        return $this->atividadeRequeridaObj;
    }

    public function setAtividadeDependenteID( $atividadeDependenteID ){
        $this->atividadeDependenteID = $atividadeDependenteID;
        return $this;
    }

    public function getAtividadeDependenteID(){
        return $this->atividadeDependenteID;
    }

    public function setAtividadeDependenteObj( $atividadeDependenteObj ){
        $this->atividadeDependenteObj = $atividadeDependenteObj;
        if( $this->atividadeDependenteObj ){
            $this->atividadeDependenteID = $this->atividadeDependenteObj->getID();
        }
        return $this;
    }

    public function getAtividadeDependenteObj(){
        if( !$this->atividadeDependenteObj ){
            $this->atividadeDependenteObj = Atividade::getByID($this->atividadeDependenteID);
        }
        return $this->atividadeDependenteObj;
    }
    
    public static function listaByAtividadeDependente( $atividadeDependente = null ){
        $Dependente = is_object($atividadeDependente) ? $atividadeDependente : Atividade::getByID($atividadeDependente);
        if( !$Dependente or !$Dependente->getID() ){
            return false;
        }
        $sql = "select id from proj_atividade_requerida where atividade_dependente_id = :atividade_dependente";
        $bind['atividade_dependente'] = $Dependente->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = AtividadeRequerida::getInstance($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function listaByAtividadeRequerida( $atividadeRequerida = null ){
        $Requerida = is_object($atividadeRequerida) ? $atividadeRequerida : Atividade::getByID($atividadeRequerida);
        if( !$Requerida or !$Requerida->getID() ){
            return false;
        }
        $sql = "select id from proj_atividade_requerida where atividade_requerida_id = :atividade_requerida";
        $bind['atividade_requerida'] = $Requerida->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = AtividadeRequerida::getInstance($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByAtividadeDependente( $atividadeDependente = null ){
        $atividadesDependentes = self::listaByAtividadeDependente($atividadeDependente);
        if( !$atividadesDependentes ){
            return false;
        }
        foreach( $atividadesDependentes as $AtividadeRequerida ){
            if( !$AtividadeRequerida->save("D") ){
                return false;
            }
        }
        return true;
    }
    
    public static function deleteByAtividadeRequerida( $atividadeRequerida = null ){
        $atividadesRequeridas = self::listaByAtividadeRequerida($atividadeRequerida);
        if( !$atividadesRequeridas ){
            return false;
        }
        foreach( $atividadesRequeridas as $AtividadeRequerida ){
            if( !$AtividadeRequerida->save("D") ){
                return false;
            }
        }
        return true;
    }
    
}