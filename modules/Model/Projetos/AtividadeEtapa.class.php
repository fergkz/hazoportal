<?php

namespace Model\Projetos;

class AtividadeEtapa extends \MVC\Model{
    
    protected $ID;
    protected $descricao;
    protected $proximaEtapaID;
    protected $proximaEtapaObj;
    protected $permiteSelecionar;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setProximaEtapaID( $proximaEtapaID ){
        $this->proximaEtapaID = $proximaEtapaID;
        return $this;
    }

    public function getProximaEtapaID(){
        return $this->proximaEtapaID;
    }

    public function setProximaEtapaObj( $proximaEtapaObj ){
        $this->proximaEtapaObj = $proximaEtapaObj;
        if( $this->proximaEtapaObj ){
            $this->proximaEtapaID = $this->proximaEtapaObj->getID();
        }
        return $this;
    }

    public function getProximaEtapaObj(){
        if( !$this->proximaEtapaObj ){
            $this->proximaEtapaObj = self::getByID($this->proximaEtapaID);
        }
        return $this->proximaEtapaObj;
    }

    public function setPermiteSelecionar( $permiteSelecionar ){
        $this->permiteSelecionar = $permiteSelecionar;
        return $this;
    }

    public function getPermiteSelecionar(){
        return $this->permiteSelecionar;
    }
    
    public static function searchPermiteSelecionar( $modo = "simple" ){
        
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select e.id, e.descricao as label
                               from proj_atividade_etapa e
                              where e.permite_selecionar = 1
                                and upper(e.descricao) like upper(::) 
                              order by e.descricao";
                break;
            case "select-all":
                $sqlQuery = "select e.id, e.descricao as label
                               from proj_atividade_etapa e
                              where e.permite_selecionar = 1";
                break;
        }
        
        return self::searchBySql($sqlQuery);
    }
    
    public static function listaProximoPermiteSelecionar(){
        $sql = "select e.id
                  from proj_atividade_etapa e
                 where e.permite_selecionar = 1";
        $res = _query($sql);
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return $dados;
    }
    
}