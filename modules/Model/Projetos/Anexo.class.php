<?php

namespace Model\Projetos;

use Model\Projetos\Atividade as Atividade;
use Model\Projetos\Demanda   as Demanda;
use Model\Projetos\Operacao  as Operacao;
use Lib\Format               as Format;
use Core\Model\User          as User;

class Anexo extends \MVC\Model{

    protected $ID;
    protected $slug;
    protected $atividadeID;
    protected $atividadeObj;
    protected $demandaID;
    protected $demandaObj;
    protected $operacaoID;
    protected $operacaObj;
    protected $principal;
    protected $titulo;
    protected $descricao;
    protected $fileContent;
    protected $fileName;
    protected $fileType;
    protected $fileSize;
    protected $fileExtension;

    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setAtividadeID( $atividadeID ){
        $this->atividadeID = $atividadeID;
        return $this;
    }

    public function getAtividadeID(){
        return $this->atividadeID;
    }

    public function setAtividadeObj( $atividadeObj ){
        $this->atividadeObj = $atividadeObj;
        if( $this->atividadeObj ){
            $this->atividadeID = $this->atividadeObj->getID();
        }
        return $this;
    }

    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = Atividade::getByID($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function setDemandaID( $demandaID ){
        $this->demandaID = $demandaID;
        return $this;
    }

    public function getDemandaID(){
        return $this->demandaID;
    }

    public function setDemandaObj( $demandaObj ){
        $this->demandaObj = $demandaObj;
        if( $this->demandaObj ){
            $this->demandaID = $this->demandaObj->getID();
        }
        return $this;
    }

    public function getDemandaObj(){
        if( !$this->demandaObj ){
            $this->demandaObj = Demanda::getByID($this->demandaID);
        }
        return $this->demandaObj;
    }

    public function setOperacaoID( $operacaoID ){
        $this->operacaoID = $operacaoID;
        return $this;
    }

    public function getOperacaoID(){
        return $this->operacaoID;
    }

    public function setOperacaoObj( $operacaObj ){
        $this->operacaoObj = $operacaObj;
        if( $this->operacaoObj ){
            $this->operacaoID = $this->operacaoObj->getID();
        }
        return $this;
    }

    public function getOperacaoObj(){
        if( !$this->operacaoObj ){
            $this->operacaoObj = Operacao::getByID($this->operacaoID);
        }
        return $this->operacaoObj;
    }

    public function setPrincipal( $principal ){
        $this->principal = $principal;
        return $this;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setFileContent( $fileContent ){
        $this->fileContent = $fileContent;
        return $this;
    }

    public function getFileContent(){
        return $this->fileContent;
    }

    public function setFileName( $fileName ){
        $this->fileName = $fileName;
        return $this;
    }

    public function getFileName(){
        return $this->fileName;
    }

    public function setFileType( $fileType ){
        $this->fileType = $fileType;
        return $this;
    }

    public function getFileType(){
        return $this->fileType;
    }

    public function setFileSize( $fileSize ){
        $this->fileSize = $fileSize;
        return $this;
    }

    public function getFileSize(){
        return $this->fileSize;
    }

    public function setFileExtension( $fileExtension ){
        $this->fileExtension = $fileExtension;
        return $this;
    }

    public function getFileExtension(){
        return $this->fileExtension;
    }

    protected function triggerBeforeInsertUpdate(){
        $tmp = explode(".", trim($this->new->fileName));
        $this->new->fileExtension = strtolower(end($tmp));
        $this->new->fileName = Format::strigToUrl(str_replace(".", "-", $this->new->fileName) );
    }
    
    protected function triggerAfterInsertUpdate(){
        if( !empty($this->demandaID) ){
            $slug = "demanda-";
        }elseif( !empty($this->atividadeID) ){
            $slug = "atividade-";
        }elseif( !empty($this->operacaoID) ){
            $slug = "operacao-";
        }else{
            _raise("A demanda, atividade ou operação deve ser informada");
        }
        
        $slug .= $this->titulo ?: $this->new->fileName;
        $slug .= "-".$this->ID;
        
        $slug = Format::strigToUrl($slug);
        
        if( $this->slug !== $slug ){
            $this->slug = $slug;
            $this->save();
        }
    }
    
    protected function permiteUsuarioVisualizar( $User ){
        $bind['user_id'] = is_object($User) ? $User->getID() : $User;
        $sql = "select count(*) as cont
                  from proj_demanda d
                  join proj_atividade a on a.demanda_id = d.id
                 where :user_id in (select ger.user_id from proj_demanda_gerente ger where ger.demanda_id = d.id)
                    or :user_id in (select grp.user_id from proj_atividade_grupo grp where grp.atividade_id = a.id)
                    or :user_id in (select usr.id from proj_atividade_grupo grp2 
                                                  join hazo_user usr on usr.group_id = grp2.group_id)";
        $res = _query($sql, $bind);
        return $res[0]['cont'] > 0 ? true : false;
    }
    
    public function permiteLogadoVisualizar(){
        return $this->permiteUsuarioVisualizar(User::online());
    }
    
    protected static function listBy( $parent, $id, $loadContent = null ){
        switch( $parent ){
            case 'demanda':
                $bind['demanda_id'] = $id;
                $whereCond[] = "x.demanda_id = :demanda_id";
                break;
            case 'atividade':
                $bind['atividade_id'] = $id;
                $whereCond[] = "x.atividade_id = :atividade_id";
                break;
            case 'operacao':
                $bind['operacao_id'] = $id;
                $whereCond[] = "x.atividade_operacao_id = :operacao_id";
                break;
        }
        $where = "where ".implode(" and ", $whereCond);
        $sql = "select x.id from proj_anexo x {$where}";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            if( $loadContent ){
                $dados[] = Anexo::getByID($row['id']);
            }else{
                $dados[] = Anexo::getByID($row['id'])->setFileContent(null);
            }
        }
        return count($dados) > 0 ? $dados : null;
    }
    
    public static function listByDemanda( $Demanda, $loadContent = true ){
        $Demanda = is_object($Demanda) ? $Demanda->getID() : $Demanda;
        return self::listBy("demanda", $Demanda, $loadContent);
    }
    
    public static function listByAtividade( $Atividade, $loadContent = true ){
        $Atividade = is_object($Atividade) ? $Atividade->getID() : $Atividade;
        return self::listBy("atividade", $Atividade, $loadContent);
    }
    
    public static function listByOperacao( $Operacao, $loadContent = true ){
        $Operacao = is_object($Operacao) ? $Operacao->getID() : $Operacao;
        return self::listBy("operacao", $Operacao, $loadContent);
    }
    
    public static function getBySlug( $slug ){
        $sql = "select id from proj_anexo where slug = :slug";
        $bind['slug'] = $slug;
        $res = _query($sql, $bind);
        return !empty($res[0]['id']) ? self::getByID($res[0]['id']) : null;
    }
    
    protected static function deleteBy( $parent, $id ){
        $anexos = self::listBy($parent, $id);
        if( $anexos ){
            foreach( $anexos as $Anexo ){
                if( !$Anexo->save("D") ){
                    return false;
                }
            }
        }
        return true;
    }
    
    public static function deleteByDemanda( $Demanda ){
        $Demanda = is_object($Demanda) ? $Demanda->getID() : $Demanda;
        return self::deleteBy("demanda", $Demanda);
    }
    
    public static function deleteByAtividade( $Atividade ){
        $Atividade = is_object($Atividade) ? $Atividade->getID() : $Atividade;
        return self::deleteBy("atividade", $Atividade);
    }
    
    public static function deleteByOperacao( $Operacao ){
        $Operacao = is_object($Operacao) ? $Operacao->getID() : $Operacao;
        return self::deleteBy("operacao", $Operacao);
    }
    
}