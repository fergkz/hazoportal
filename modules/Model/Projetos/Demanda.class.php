<?php

namespace Model\Projetos;

use Model\Pessoa\Cliente as Cliente;
use Core\Model\User as User;
use Lib\Format as Format;
use Model\Projetos\Atividade as Atividade;
use Model\Projetos\DemandaGerente as Gerente;

class Demanda extends \MVC\Model{
    
    protected $ID;
    protected $slug;
    protected $clienteID;
    protected $clienteObj;
    protected $titulo;
    protected $descricao;
    protected $status; # [A]berta, [C]oncluído, ca[N]celado, [S]tandby
    protected $cadastroData;
    protected $cadastroUserID;
    protected $cadastroUserObj;
    protected $prioridade;
    protected $tipoDemandaID;
    protected $tipoDemandaObj;
    
    protected $anexos;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setClienteID( $clienteID ){
        $this->clienteID = $clienteID;
        return $this;
    }

    public function getClienteID(){
        return $this->clienteID;
    }

    public function setClienteObj( $clienteObj ){
        $this->clienteObj = $clienteObj;
        if( $clienteObj ){
            $this->clienteID = $clienteObj->getID();
        }
        return $this;
    }

    public function getClienteObj(){
        if( !$this->clienteObj ){
            $this->clienteObj = Cliente::getInstance($this->clienteID);
        }
        return $this->clienteObj;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }
    
    public function getStatusDescricao(){
        $status = array(
            'A' => 'Aberta', 
            'C' => 'Concluída',
            'N' => 'Cancelada',
            'S' => 'Standby'
        );
        return $status[$this->status];
    }

    public function setCadastroData( $cadastroData ){
        $this->cadastroData = $cadastroData;
        return $this;
    }

    public function getCadastroData(){
        return $this->cadastroData;
    }

    public function setCadastroUserID( $cadastroUserID ){
        $this->cadastroUserID = $cadastroUserID;
        return $this;
    }

    public function getCadastroUserID(){
        return $this->cadastroUserID;
    }

    public function setCadastroUserObj( $cadastroUserObj ){
        $this->cadastroUserObj = $cadastroUserObj;
        if( $cadastroUserObj ){
            $this->cadastroUserID = $cadastroUserObj->getID();
        }
        return $this;
    }

    public function getCadastroUserObj(){
        if( !$this->cadastroUserObj ){
            $this->cadastroUserObj = User::getInstance($this->cadastroUserID);
        }
        return $this->cadastroUserObj;
    }

    public function setPrioridade( $prioridade ){
        $this->prioridade = $prioridade;
        return $this;
    }

    public function getPrioridade(){
        return $this->prioridade;
    }

    public function setTipoDemandaID( $tipoDemandaID ){
        $this->tipoDemandaID = $tipoDemandaID;
        return $this;
    }

    public function getTipoDemandaID(){
        return $this->tipoDemandaID;
    }

    public function setTipoDemandaObj( $tipoDemandaObj ){
        $this->tipoDemandaObj = $tipoDemandaObj;
        if( $tipoDemandaObj ){
            $this->tipoDemandaID = $tipoDemandaObj->getID();
        }
        return $this;
    }

    public function getTipoDemandaObj(){
        if( !$this->tipoDemandaObj ){
            $this->tipoDemandaObj = TipoDemanda::getInstance($this->tipoDemandaID);
        }
        return $this->tipoDemandaObj;
    }
    
    public function getAtividade( $atividadeID = null ){
        return Atividade::getByDemandaAtividade( $this->ID, $atividadeID );
    }
    
    public function getAnexos(){
        if( !$this->anexos ){
            $this->anexos = Anexo::listByDemanda($this, false);
        }
        return $this->anexos;
    }
    
    protected function triggerBeforeInsert(){
        $this->new->cadastroData = date("Y-m-d H:i:s");
        $this->new->cadastroUserID = User::online()->getID();
        if( !(int)$this->new->prioridade ){
            $sql = "select COALESCE(max(prioridade),0)+1 as cont from proj_demanda where status = 'A'";
            $res = _query($sql);
            $this->new->prioridade = $res[0]['cont'];
        }
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( $this->new->clienteID ){
            $Cliente = Cliente::getByID($this->new->clienteID);
            $this->new->slug = Format::strigToUrl($Cliente->getPessoaObj()->getRazao().'-'.$this->new->titulo);
        }else{
            _raise("O cliente deve ser informado");
        }
    }
    
    protected function triggerBeforeDelete(){
        $atividades = Atividade::listaByDemanda($this);
        if( $atividades ){
            foreach( $atividades as $Atividade ){
                $Atividade->save("D");
            }
        }
        $gerentes = DemandaGerente::listaByDemanda($this);
        if( $gerentes ){
            foreach( $gerentes as $Gerente ){
                $Gerente->save("D");
            }
        }
        Anexo::deleteByDemanda($this);
    }
    
    public function getAtividades(){
        return Atividade::listaByDemanda( $this->ID );
    }
    
    public static function getInstanceBySlug( $demandaSlug ){
        $sql = "select id from proj_demanda where slug = :slug";
        $bind['slug'] = $demandaSlug;
        $res = _query($sql, $bind);
        return self::getInstance(@$res[0]['id']);
    }
    
    public static function pesquisa( $clienteID = null, $gerenteUser = null, $texto = null, $status = null ){
        $sql = "select d.id from proj_demanda d 
                  left join pes_cliente c on c.id = d.cliente_id
                  left join pes_pessoa p on p.id = c.pessoa_id";
        $bind = array();
        $where = array();
        if( $clienteID ){
            $where[] = "d.cliente_id = :cliente_id";
            $bind['cliente_id'] = $clienteID;
        }
        if( $gerenteUser ){
            $userID = is_object($gerenteUser) ? $gerenteUser->getID() : $gerenteUser;
            $where[] = "d.id in (select demanda_id from proj_demanda_gerente where user_id = :gerente_user_id)";
            $bind['gerente_user_id'] = $userID;
        }
        if( $texto ){
            $nTexto = explode(" ", trim($texto));
            if( $nTexto ){
                $ind = 0;
                foreach( $nTexto as $row ){
                    if( !$row ){
                        continue;
                    }
                    $ind++;
                    $where[] = " ( upper(d.slug)      like upper(:texto_{$ind}) 
                                or upper(d.titulo)    like upper(:texto_{$ind}) 
                                or upper(p.razao)     like upper(:texto_{$ind}) 
                                or upper(p.fantasia)  like upper(:texto_{$ind}) 
                                or upper(p.cpf)       like upper(:texto_{$ind}) 
                                or upper(p.cnpj)      like upper(:texto_{$ind}) 
                                or upper(d.descricao) like upper(:texto_{$ind})  ) ";
                    $bind["texto_{$ind}"] = "%".$row."%";
                }
            }
        }
        if( $status && is_array($status) ){
            $w  = "d.status in (";
            $ind = 0;
            foreach( $status as $row ){
                if( $row ){
                    $ind++;
                    $bind["status{$ind}"] = $row;
                    if( $ind !== 1 ){
                        $w .= ", :status{$ind}";
                    }else{
                        $w .= ":status{$ind}";
                    }
                }
            }
            $w .= ")";
            if( $ind > 0 ){
                $where[] = $w;
            }
        }elseif( $status ){
            $where[] = "d.status in (:status)";
            $bind["status"] = $status;
        }
        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        $sql .= " order by FIELD(d.status, 'A', 'C', 'N', 'S'), d.prioridade ";
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : false;
    }
    
    public function changeStatus(){
        
        $atividades = $this->getAtividades();
        
        if( $atividades ){
            $cont = array('total'=>0);
            foreach( $atividades as $Atividade ){
                if( empty($cont[$Atividade->getEtapaID()]) ){
                    $cont[$Atividade->getEtapaID()] = 0;
                }
                $cont['total']++;
                $cont[$Atividade->getEtapaID()]++;
            }
            
            if( !empty($cont['U']) and $cont['U'] + (empty($cont['C']) ? 0 : $cont['C']) === $cont['total'] ){
                if( $this->status !== "C" ){   
                    $this->status = "C"; #[C]oncluída
                    $this->save();
                }
            }
            elseif( !empty($cont['A']) or !empty($cont['P']) or !empty($cont['G']) or !empty($cont['H']) or !empty($cont['V']) or !empty($cont['D']) or !empty($cont['I']) ){
                if( $this->status !== "A" ){   
                    $this->status = "A"; #[A]berta
                    $this->save();
                }
            }
            elseif( !empty($cont['C']) and $cont['C'] === $cont['total'] ){
                if( $this->status !== "N" ){   
                    $this->status = "N"; #ca[N]celada
                    $this->save();
                }
            }
        }elseif( $this->status !== "S" ){
            $this->status = "S"; #[S]tandby
            $this->save();
        }
    }
    
    public static function listaByStatus( $status = "A", $gerenteUser = null ){
        $sql = "select id from proj_demanda where status = :status order by FIELD(status, 'A', 'I', 'C'), prioridade, cad_data desc";
        $bind['status'] = $status;
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $Demanda = self::getByID($row['id']);
            if( $gerenteUser ){
                if( !Gerente::isGerenteByDemanda( $Demanda, $gerenteUser ) ){
                    continue;
                }
            }
            $dados[] = $Demanda;
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public function getPercAtividadesConcluidas(){
        $atividades = $this->getAtividades();
        if( $atividades ){
            $duracaoAtividades = 0;
            $duracaoConcluidas = 0;
            foreach( $atividades as $Atividade ){
                if( !in_array($Atividade->getEtapaID(), array("C")) ){
                    $duracaoAtividades += $Atividade->getDuracaoEstimada();
                }
                if( in_array($Atividade->getEtapaID(), array("U")) ){
                    $duracaoConcluidas += $Atividade->getDuracaoEstimada();
                }
            }
            
            if( $duracaoAtividades > 0 and $duracaoConcluidas > 0 ){
                return number_format($duracaoConcluidas / $duracaoAtividades * 100, 2);
            }
        }
        return 0;
    }
    
    public static function search( $modo = "simple" ){
        $bind = array();
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select d.id, d.titulo as label
                               from proj_demanda d
                              where upper(d.titulo) like upper(::)
                              order by d.titulo";
                break;
            case "simple_ger":
                $sqlQuery = "select d.id, d.titulo as label
                               from proj_demanda d
                               left join proj_demanda_gerente g on g.demanda_id = d.id
                              where upper(d.titulo) like upper(::)
                                and g.user_id = ".User::online()->getID()."
                              order by d.titulo";
                break;
        }
        return self::searchBySql($sqlQuery, $bind);
    }
    
}