<?php

namespace Model\Projetos;

class TipoDemanda extends \MVC\Model{
    
    protected $ID;
    protected $titulo;
    protected $descricao;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    
    protected function triggerBeforeInsertUpdate(){
        $this->new->ID = strtoupper($this->new->ID);
    }
    
    public static function search( $modo = "simple" ){
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select t.id, t.titulo as label
                               from proj_tipo_demanda t
                              where upper(t.titulo) like upper(::)
                                 or upper(t.descricao) like upper(::)
                              order by t.titulo";
                break;
        }
        return self::searchBySql($sqlQuery);
    }
    
}