<?php

namespace Model\Projetos;

use Core\Model\User as User;

class AtividadePadroes extends \MVC\Model{
    
    protected $ID;
    protected $userID;
    protected $userObj;
    protected $content;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }
    
    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
    public function setContent( $content ){
        $this->content = serialize($content);
        return $this;
    }

    public function getContent(){
        return $this->content ? unserialize($this->content) : array();
    }
    
    public function triggerBeforeInsertUpdate(){
        if( !$this->new->userID ){
            $this->new->userID = User::online()->getID();
        }
    }
    
    public static function getByLogado(){
        $sql = "select id from proj_atividade_padroes where user_id = :user_id";
        $bind['user_id'] = User::online()->getID();
        $res = _query($sql, $bind);
        return self::getInstance( @$res[0]['id'] );
    }
    
}