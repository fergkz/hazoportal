<?php

namespace Model\Pessoa;

use Core\Model\User as User;
use Model\Pessoa\Pessoa as Pessoa;
use Model\Pessoa\PessoaEmail as Email;

use Lib\Format as Format;

class Aluno extends \MVC\Model{
    
    protected $ID;
    protected $userID;
    protected $userObj;
    protected $pessoaID;
    protected $pessoaObj;
    
    const groupID = "5";
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }
    
    public static function getByCPF( $cpf ){
        $sql = "select a.id from dnw_aluno a join pes_pessoa p on p.id = a.pessoa_id where p.cpf = :cpf";
        $bind['cpf'] = $cpf;
        $res = _query($sql, $bind);
        return self::getByID( @$res[0]['id'] ?: null );
    }
    
    public function triggerBeforeInsertUpdate(){
        if( !$this->new->userID and $this->getPessoaObj() ){
            $Email = Email::getByPessoa($this->getPessoaObj(), true);
            if( $Email ){
                $User = new User();
                $User->setEmail( $Email->getEmail() );
                $User->setGroupID( self::groupID );
                $User->setLogin( Format::formNumber( $this->getPessoaObj()->getCpf() ) );
                $User->setName( $this->getPessoaObj()->getRazao() );
                $User->setPassword(null);
                $User->setStatus('A');
                if( $User->save() ){
                    $this->new->userID = $User->getID();
                }else{
                    _raise();
                }
            }else{
                _raise("O e-mail deve ser informado");
            }
        }
    }
    
    public static function pesquisa( $get ){
        
        $bind  = array();
        $where = array();
        $order = array();
        
        if( !empty($get['texto']) ){
            $textos = explode(" ", trim($get['texto']));
            if( $textos and count($textos) > 0 ){
                $i = 0;
                $p = array();
                foreach( $textos as $row ){
                    $i++;
                    
                     $p[] = "(   p.razao like upper(:cons_{$i})
                               or
                                 p.fantasia like upper(:cons_{$i})
                               or
                                 p.cpf like upper(:cons_{$i})
                               or
                                 p.cnpj like upper(:cons_{$i}) )";
                    $bind["cons_{$i}"] = "%{$row}%";
                    
                }
                $where[] = implode(' and ', $p);
            }
        }
        
        switch( @$get['order'] ){
            default:
            case 'ALUNO':
                $order[] = "p.razao";
                break;
        }
        
        $sql = " select a.id
                   from dnw_aluno a
                   join pes_pessoa p on p.id = a.pessoa_id";

        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        if( count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }
        
        $cont = _query("select count(*) as cont from ( $sql ) t", $bind);
        $dados['cont'] = $cont[0]['cont'];
        
        $pg = (int)(!empty($get['pg']) ? $get['pg'] : 1);
        
        $dados['max_links'] = ceil($dados['cont']/10);
        
        if( !empty($get['pg']) ){
            $ini = ($get['pg'] ?: 1) * 10 - 10;
            $sql .= " limit {$ini}, 11";
        }else{
            $sql .= " limit 0, 11";
        }
        
        $res = _query($sql, $bind);
        
        if( $res and count($res) > 10 ){
            $dados['nex'] = true;
            array_pop($res);
        }
        
        foreach( @$res as $row ){
            $dados['alunos'][] = self::getByID($row['id']);
        }
        
        return $dados;
        
    }
    
}