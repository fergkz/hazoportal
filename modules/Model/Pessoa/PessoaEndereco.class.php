<?php

namespace Model\Pessoa;

use Model\Pessoa\Pessoa as Pessoa;
use Model\Geografico\Cidade as Cidade;

use Lib\Format as Format;

class PessoaEndereco extends \MVC\Model{
    
    protected $ID;
    protected $logradouro;
    protected $numero;
    protected $cep;
    protected $bairro;
    protected $principal;
    protected $pessoaID;
    protected $pessoaObj;
    protected $cidadeID;
    protected $cidadeObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setLogradouro( $logradouro ){
        $this->logradouro = $logradouro;
        return $this;
    }

    public function getLogradouro(){
        return $this->logradouro;
    }

    public function setNumero( $numero ){
        $this->numero = $numero;
        return $this;
    }

    public function getNumero(){
        return $this->numero;
    }

    public function setCep( $cep ){
        $this->cep = Format::formNumber($cep);
        return $this;
    }

    public function getCep(){
        return $this->cep;
    }

    public function setBairro( $bairro ){
        $this->bairro = $bairro;
        return $this;
    }

    public function getBairro(){
        return $this->bairro;
    }

    public function setPrincipal( $principal ){
        $this->principal = $principal;
        return $this;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }

    public function setCidadeID( $cidadeID ){
        $this->cidadeID = $cidadeID;
        return $this;
    }

    public function getCidadeID(){
        return $this->cidadeID;
    }

    public function setCidadeObj( $cidadeObj ){
        $this->cidadeObj = $cidadeObj;
        if( $this->cidadeObj ){
            $this->cidadeID = $this->cidadeObj->getID();
        }
        return $this;
    }

    public function getCidadeObj(){
        if( !$this->cidadeObj ){
            $this->cidadeObj = Cidade::getByID($this->cidadeID);
        }
        return $this->cidadeObj;
    }
    
    protected function triggerBeforeInsertUpdate(){
        $this->new->logradouro = trim( Format::formNomeProprio($this->new->logradouro) );
        $this->new->numero = trim( Format::formNumber($this->new->numero) );
    }
    
    public static function getPrincipalByPessoa( $pessoa ){
        return self::getByPessoa($pessoa, true);
    }
    
    public static function getByPessoa( $pessoa, $first = null ){
        $pessoaID = is_object($pessoa) ? $pessoa->getID() : $pessoa;
        $sql = "select id from pes_pessoa_endereco where pessoa_id = :pessoa_id";
        $bind['pessoa_id'] = $pessoaID;
        $res = _query($sql, $bind);
        $dados = array();
        
        if( $res ){
            foreach( $res as $row ){
                if( $first ){
                    return self::getByID($row['id']);
                }
                $dados[] = self::getByID($row['id']);
            }
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByPessoa( $pessoa ){
        $dados = self::getByPessoa($pessoa);
        if( $dados ){
            foreach( $dados as $Obj ){
                if( !$Obj->save('D') ){
                    return false;
                }
            }
        }
        return true;
    }
    
}