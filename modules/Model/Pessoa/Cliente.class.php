<?php

namespace Model\Pessoa;

class Cliente extends \MVC\Model{
    
    protected $ID;
    protected $pessoaID;
    protected $pessoaObj;
    protected $status;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $pessoaObj ){
            $this->pessoaID = $pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }
    
    public static function search( $modo = "simple" ){
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select c.id, 
                               case when p.fantasia is null or p.fantasia = p.razao then
                                   p.razao
                               else
                                   concat(p.fantasia,' (',p.razao,')')
                               end as label
                               from pes_cliente c
                               join pes_pessoa p on p.id = c.pessoa_id
                              where c.status = 'A' 
                                and ( upper(p.razao) like upper(::)
                                   or upper(p.fantasia) like upper(::)
                                   or upper(p.cpf) like upper(::)
                                   or upper(p.cnpj) like upper(::) )
                              order by p.fantasia, p.razao";
                break;
        }
        return self::searchBySql($sqlQuery);
    }
    
}