<?php

namespace Model\Pessoa;
use Core\Model\User as User;

class Usuario extends User{
    
    public static function search( $modo = "simple" ){
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select u.id, u.name as label
                               from hazo_user u
                              where u.status = 'A' 
                                and upper(u.name) like upper(::)
                              order by u.name";
                break;
            case "simple-with-group":
                $sqlQuery = "select u.id, concat(u.name,' (',g.name,')') as label
                               from hazo_user u
                               join hazo_group g on g.id = u.group_id
                              where u.status = 'A' 
                                and upper(concat(u.name,' (',g.name,')')) like upper(::)
                              order by u.name";
                break;
            case "nao-logado":
                $sqlQuery = "select u.id, u.name as label
                               from hazo_user u
                              where u.status = 'A' 
                                and upper(u.name) like upper(::)
                                and u.id <> ".User::online()->getID()."
                              order by u.name";
                break;
        }
        return self::searchBySql($sqlQuery);
    }
    
    public static function pesquisa( $get, $condicao = null ){
        
        $bind  = array();
        $where = array();
        $order = array();
        
        if( !empty($get['texto']) ){
            $textos = explode(" ", trim($get['texto']));
            if( $textos and count($textos) > 0 ){
                $i = 0;
                $p = array();
                foreach( $textos as $row ){
                    $i++;
                    
                     $p[] = "(   u.name like upper(:cons_{$i})
                               or
                                 u.login like upper(:cons_{$i})
                               or
                                 u.email like upper(:cons_{$i}) )";
                    $bind["cons_{$i}"] = "%{$row}%";
                    
                }
                $where[] = implode(' and ', $p);
            }
        }
        
        switch( @$get['order'] ){
            default:
            case 'NAME':
                $order[] = "u.name";
                break;
        }
        
//        if( $condicao === 'cad-usuario' ){
//            $except = implode(',', $GLOBALS['config']['groups']);
//            $where[] = "u.group_id not in ({$except})";
//            $where[] = "g.mode not in ('S')";
            $logID = User::online()->getID();
            $where[] = "u.id not in ({$logID})";
//        }
        
        $sql = " select u.id from hazo_user u join hazo_group g on g.id = u.group_id";

        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        if( count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }
//        debug($sql);
        $cont = _query("select count(*) as cont from ( $sql ) t", $bind);
        $dados['cont'] = $cont[0]['cont'];
        
        $pg = (int)(!empty($get['pg']) ? $get['pg'] : 1);
        
        $dados['max_links'] = ceil($dados['cont']/10);
        
        if( !empty($get['pg']) ){
            $ini = ($get['pg'] ?: 1) * 10 - 10;
            $sql .= " limit {$ini}, 11";
        }else{
            $sql .= " limit 0, 11";
        }
        
        $res = _query($sql, $bind);
        
        if( $res and count($res) > 10 ){
            $dados['nex'] = true;
            array_pop($res);
        }
        
        foreach( @$res as $row ){
            $dados['usuarios'][] = User::getByID($row['id']);
        }
        
        return $dados;
        
    }
    
}