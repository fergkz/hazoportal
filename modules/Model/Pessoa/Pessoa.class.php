<?php

namespace Model\Pessoa;

use Lib\Validate as Validate;
use Lib\Format as Format;

use Model\Pessoa\PessoaEmail    as Email;
use Model\Pessoa\PessoaTelefone as Telefone;

class Pessoa extends \MVC\Model{

    protected $ID;
    protected $razao;
    protected $fantasia;
    protected $slug;
    protected $cpf;
    protected $cnpj;
    protected $dataNascimento;
    
    protected $emails;
    protected $telefones;

    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setRazao( $razao ){
        $this->razao = $razao;
        return $this;
    }

    public function getRazao(){
        return $this->razao;
    }

    public function setFantasia( $fantasia ){
        $this->fantasia = $fantasia;
        return $this;
    }

    public function getFantasia(){
        return $this->fantasia;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setCpf( $cpf ){
        $this->cpf = $cpf;
        return $this;
    }

    public function getCpf(){
        return $this->cpf;
    }

    public function setCnpj( $cnpj ){
        $this->cnpj = $cnpj;
        return $this;
    }

    public function getCnpj(){
        return $this->cnpj;
    }

    public function setDataNascimento( $dataNascimento ){
        $this->dataNascimento = $dataNascimento ? Format::dataToDb($dataNascimento) : null;
        return $this;
    }

    public function getDataNascimento(){
        return Format::dataToBr($this->dataNascimento);
    }

    protected function triggerBeforeInsertUpdate(){
        if( $this->new->cpf && !Validate::isCPF($this->new->cpf) ){
            _raise("CPF Inválido");
        }
        if( $this->new->cnpj && !Validate::isCNPJ($this->new->cnpj) ){
            _raise("CNPJ Inválido");
        }
        if( !$this->new->razao ){
            _raise("A razão social deve ser informada");
        }
        $this->new->cpf = Format::formNumber($this->new->cpf);
        $this->new->cnpj = Format::formNumber($this->new->cnpj);
        $this->new->razao = Format::formNomeProprio($this->new->razao);
        if( !$this->new->fantasia ){
            $this->new->fantasia = $this->new->razao;
        }
        $this->new->fantasia = Format::formNomeProprio($this->new->fantasia);
    }

    protected function triggerAfterInsertUpdate(){
        $newSlug = trim(Format::strigToUrl($this->razao."-".$this->fantasia."-".$this->ID));
        if( $newSlug != $this->slug ){
            $this->slug = $newSlug;
            $this->save();
        }
    }

    public static function consulta( $consulta = null ){
        if( !$consulta ){
            return false;
        }
        $sql = "select id from pes_pessoa p where ";

        $p = array();
        $cont = 0;
        foreach( explode(" ", $consulta) as $row ){
            $cont++;
            $p[] = "(
                        p.razao like upper(:cons_{$cont})
                      or
                        p.fantasia like upper(:cons_{$cont})
                      or
                        p.cpf like upper(:cons_{$cont})
                      or
                        p.cnpj like upper(:cons_{$cont})
                    )";
            $bind['cons_'.$cont] = "%{$row}%";
        }
        $sql .= implode(" and ", $p);
        $res = _query($sql, $bind);

        $dados = array( );
        foreach( $res as $row ){
            $dados[] = Pessoa::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }

    public static function getBySlug( $slug ){
        $sql = "select id from pes_pessoa where slug = :slug";
        $bind['slug'] = $slug;
        $res = _query($sql, $bind);
        return !empty($res[0]['id']) ? self::getByID($res[0]['id']) : false;
    }
    
    public function setEmails( $emails ){
        $this->emails = $emails;
        return $this;
    }
    
    public function getEmails(){
        if( !$this->emails ){
            $this->emails = Email::getByPessoa($this->ID);
        }
        return $this->emails;
    }
    
    public function setTelefones( $telefones ){
        $this->telefones = $telefones;
        return $this;
    }
    
    public function getTelefones(){
        if( !$this->telefones ){
            $this->telefones = Telefone::getByPessoa($this->ID);
        }
        return $this->telefones;
    }
    
    
    public static function pesquisa( $get ){
        
        $bind  = array();
        $where = array();
        $order = array();
        
        if( !empty($get['texto']) ){
            $textos = explode(" ", trim($get['texto']));
            if( $textos and count($textos) > 0 ){
                $i = 0;
                $p = array();
                foreach( $textos as $row ){
                    $i++;
                    
                     $p[] = "(   p.razao like upper(:cons_{$i})
                               or
                                 p.fantasia like upper(:cons_{$i})
                               or
                                 p.cpf like upper(:cons_{$i})
                               or
                                 p.cnpj like upper(:cons_{$i}) )";
                    $bind["cons_{$i}"] = "%{$row}%";
                    
                }
                $where[] = implode(' and ', $p);
            }
        }
        
        switch( @$get['order'] ){
            default:
            case 'NOME':
                $order[] = "p.razao";
                break;
        }
        
        $sql = " select p.id
                   from pes_pessoa p ";

        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        if( count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }
        
        $cont = _query("select count(*) as cont from ( $sql ) t", $bind);
        $dados['cont'] = $cont[0]['cont'];
        
        $pg = (int)(!empty($get['pg']) ? $get['pg'] : 1);
        
        $dados['max_links'] = ceil($dados['cont']/10);
        
        if( !empty($get['pg']) ){
            $ini = ($get['pg'] ?: 1) * 10 - 10;
            $sql .= " limit {$ini}, 11";
        }else{
            $sql .= " limit 0, 11";
        }
        
        $res = _query($sql, $bind);
        
        if( $res and count($res) > 10 ){
            $dados['nex'] = true;
            array_pop($res);
        }
        
        foreach( @$res as $row ){
            $dados['pessoas'][] = self::getByID($row['id']);
        }
        
        return $dados;
        
    }

    public static function search( $modo = "simple" ){
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select p.id, p.razao as label
                               from pes_pessoa p
                              where upper(p.razao) like upper(::)
                              order by p.razao";
                break;
        }
        return self::searchBySql($sqlQuery);
    }
    
}