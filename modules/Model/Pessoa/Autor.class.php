<?php

namespace Model\Pessoa;

use Core\Model\User as User;
use Model\Pessoa\Pessoa as Pessoa;

class Autor extends \MVC\Model{
    
    protected $ID;
    protected $userID;
    protected $userObj;
    protected $pessoaID;
    protected $pessoaObj;
    protected $status;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }
    
    public static function getByUser( $user ){
        $bind['user_id'] = is_object($user) ? $user->getID() : $user;
        $sql = "select a.id from pag_autor a where a.user_id = :user_id";
        $res = _query($sql, $bind);
        return self::getByID( @$res[0]['id'] ?: null );
    }
    
    public static function getOnline(){
        return self::getByUser( User::online() );
    }
    
    public static function pesquisa( $get ){
        
        $bind  = array();
        $where = array();
        $order = array();
        
        if( !empty($get['texto']) ){
            $textos = explode(" ", trim($get['texto']));
            if( $textos and count($textos) > 0 ){
                $i = 0;
                $p = array();
                foreach( $textos as $row ){
                    $i++;
                    
                     $p[] = "(   p.razao like upper(:cons_{$i})
                               or
                                 p.fantasia like upper(:cons_{$i})
                               or
                                 p.cpf like upper(:cons_{$i})
                               or
                                 p.cnpj like upper(:cons_{$i}) )";
                    $bind["cons_{$i}"] = "%{$row}%";
                    
                }
                $where[] = implode(' and ', $p);
            }
        }
        
        switch( @$get['order'] ){
            default:
            case 'AUTOR':
                $order[] = "p.razao";
                break;
        }
        
        $sql = " select a.id
                   from pag_autor a
                   join pes_pessoa p on p.id = a.pessoa_id";

        if( count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        if( count($order) > 0 ){
            $sql .= " order by ".implode(", ", $order);
        }
        
        $cont = _query("select count(*) as cont from ( $sql ) t", $bind);
        $dados['cont'] = $cont[0]['cont'];
        
        $pg = (int)(!empty($get['pg']) ? $get['pg'] : 1);
        
        $dados['max_links'] = ceil($dados['cont']/10);
        
        if( !empty($get['pg']) ){
            $ini = ($get['pg'] ?: 1) * 10 - 10;
            $sql .= " limit {$ini}, 11";
        }else{
            $sql .= " limit 0, 11";
        }
        
        $res = _query($sql, $bind);
        
        if( $res and count($res) > 10 ){
            $dados['nex'] = true;
            array_pop($res);
        }
        
        foreach( @$res as $row ){
            $dados['autores'][] = self::getByID($row['id']);
        }
        
        return $dados;
        
    }
    
    public static function search( $mode ){
        switch( $mode ){
            default:
            case 'full-editoria':
                $sql = "select a.id, concat(p.razao, ' - ', u.login, ' (',g.name, ')') as label
                          from pag_autor a
                          join pes_pessoa p on p.id = a.pessoa_id
                          join hazo_user u on u.id = a.user_id
                          join hazo_group g on g.id = u.group_id
                         where upper(concat(p.razao)) like upper(::) 
                           and a.status = 'A'
                           and u.status = 'A'";
                break;
        }
        return self::searchBySql($sql);
    }
    
}