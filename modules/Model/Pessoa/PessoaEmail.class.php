<?php

namespace Model\Pessoa;

use Model\Pessoa\Pessoa as Pessoa;

use Lib\Validate as Validate;

class PessoaEmail extends \MVC\Model{
    
    protected $ID;
    protected $email;
    protected $principal;
    protected $pessoaID;
    protected $pessoaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setEmail( $email ){
        $this->email = $email;
        return $this;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setPrincipal( $principal ){
        $this->principal = $principal;
        return $this;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !Validate::isEmail($this->new->email) ){
            _raise("E-mail inválido");
        }
        $this->new->email = trim($this->new->email);
    }
    
    public static function getPrincipalByPessoa( $pessoa ){
        return self::getByPessoa($pessoa, true);
    }
    
    public static function getByPessoa( $pessoa, $first = null ){
        $pessoaID = is_object($pessoa) ? $pessoa->getID() : $pessoa;
        $sql = "select id from pes_pessoa_email where pessoa_id = :pessoa_id";
        $bind['pessoa_id'] = $pessoaID;
        $res = _query($sql, $bind);
        
        $dados = array();
        
        if( $res ){
            foreach( $res as $row ){
                if( $first ){
                    return self::getByID($row['id']);
                }
                $dados[] = self::getByID($row['id']);
            }
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByPessoa( $pessoa ){
        $dados = self::getByPessoa($pessoa);
        if( $dados ){
            foreach( $dados as $Obj ){
                if( !$Obj->save('D') ){
                    return false;
                }
            }
        }
        return true;
    }
    
}