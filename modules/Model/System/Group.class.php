<?php

namespace Model\System;

class Group extends \Core\Model\Group{
    
    public static function search( $modo = "simple" ){
        switch( $modo ){
            default:
            case "simple":
                $sqlQuery = "select g.id, g.name as label
                               from hazo_group g
                              where g.mode not in ('S')
                                and upper(g.name) like upper(::)";
                break;
        }
        return self::searchBySql($sqlQuery);
    }
    
}