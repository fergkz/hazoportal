<?php

namespace Model\Estoque;

use Model\Estoque\Almoxarifado as Almoxarifado;
use Core\Model\User as User;

class AlmoxarifadoUsuario extends \MVC\Model{

    protected $ID;
    protected $almoxarifadoID;
    protected $almoxarifadoObj;
    protected $userID;
    protected $userObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setAlmoxarifadoID( $almoxarifadoID ){
        $this->almoxarifadoID = $almoxarifadoID;
        return $this;
    }

    public function getAlmoxarifadoID(){
        return $this->almoxarifadoID;
    }

    public function setAlmoxarifadoObj( $almoxarifadoObj ){
        $this->almoxarifadoObj = $almoxarifadoObj;
        if( $this->almoxarifadoObj ){
            $this->almoxarifadoID = $this->almoxarifadoObj->getID();
        }
        return $this;
    }

    public function getAlmoxarifadoObj(){
        if( !$this->almoxarifadoObj ){
            $this->almoxarifadoObj = Almoxarifado::getByID($this->almoxarifadoID);
        }
        return $this->almoxarifadoObj;
    }

    public function setUserID( $userID ){
        $this->userID = $userID;
        return $this;
    }

    public function getUserID(){
        return $this->userID;
    }

    public function setUserObj( $userObj ){
        $this->userObj = $userObj;
        if( $this->userObj ){
            $this->userID = $this->userObj->getID();
        }
        return $this;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
}