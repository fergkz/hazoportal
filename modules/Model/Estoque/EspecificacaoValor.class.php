<?php

namespace Model\Estoque;

use Model\Estoque\Especificacao as Especificacao;
use Model\Estoque\ProdutoItem as ProdutoItem;

class EspecificacaoValor extends \MVC\Model{

    protected $ID;
    protected $especificacaoID;
    protected $especificacaoObj;
    protected $produtoItemID;
    protected $produtoItemObj;
    protected $valor;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setEspecificacaoID( $especificacaoID ){
        $this->especificacaoID = $especificacaoID;
        return $this;
    }

    public function getEspecificacaoID(){
        return $this->especificacaoID;
    }

    public function setEspecificacaoObj( $especificacaoObj ){
        $this->especificacaoObj = $especificacaoObj;
        if( $this->especificacaoObj ){
            $this->especificacaoID = $this->especificacaoObj->getID();
        }
        return $this;
    }

    public function getEspecificacaoObj(){
        if( !$this->especificacaoObj ){
            $this->especificacaoObj = Especificacao::getByID($this->especificacaoID);
        }
        return $this->especificacaoObj;
    }

    public function setProdutoItemID( $produtoItemID ){
        $this->produtoItemID = $produtoItemID;
        return $this;
    }

    public function getProdutoItemID(){
        return $this->produtoItemID;
    }

    public function setProdutoItemObj( $produtoItemObj ){
        $this->produtoItemObj = $produtoItemObj;
        if( $this->produtoItemObj ){
            $this->produtoItemID = $this->produtoItemObj->getID();
        }
        return $this;
    }

    public function getProdutoItemObj(){
        if( !$this->produtoItemObj ){
            $this->produtoItemObj = ProdutoItem::getByID($this->produtoItemID);
        }
        return $this->produtoItemObj;
    }
    
    public function setValor( $valor ){
        $this->valor = $valor;
        return $this;
    }

    public function getValor(){
        return $this->valor;
    }
    
}