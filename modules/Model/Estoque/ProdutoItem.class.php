<?php

namespace Model\Estoque;

use Model\Estoque\Produto as Produto;
use Model\Estoque\Valor as Valor;

class ProdutoItem extends \MVC\Model{

    protected $ID;
    protected $produtoID;
    protected $produtoObj;
    protected $almoxarifadoID;
    protected $almoxarifadoObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setProdutoID( $produtoID ){
        $this->produtoID = $produtoID;
        return $this;
    }

    public function getProdutoID(){
        return $this->produtoID;
    }

    public function setProdutoObj( $produtoObj ){
        $this->produtoObj = $produtoObj;
        if( $this->produtoObj ){
            $this->produtoID = $this->produtoObj->getID();
        }
        return $this;
    }

    public function getProdutoObj(){
        if( !$this->produtoObj ){
            $this->produtoObj = Produto::getByID($this->produtoID);
        }
        return $this->produtoObj;
    }

    public function setAlmoxarifadoID( $almoxarifadoID ){
        $this->almoxarifadoID = $almoxarifadoID;
        return $this;
    }

    public function getAlmoxarifadoID(){
        return $this->almoxarifadoID;
    }

    public function setAlmoxarifadoObj( $almoxarifadoObj ){
        $this->almoxarifadoObj = $almoxarifadoObj;
        if( $this->almoxarifadoObj ){
            $this->almoxarifadoID = $this->almoxarifadoObj->getID();
        }
        return $this;
    }

    public function getAlmoxarifadoObj(){
        if( !$this->almoxarifadoObj ){
//            $this->almoxarifadoObj = :XXXXXXX::getByID($this->almoxarifadoID);
        }
        return $this->almoxarifadoObj;
    }
    
}