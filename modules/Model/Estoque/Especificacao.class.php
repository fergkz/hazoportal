<?php

namespace Model\Estoque;

use Model\Estoque\Produto as Produto;

class Especificacao extends \MVC\Model{

    protected $ID;
    protected $produtoID;
    protected $produtoObj;
    protected $titulo;
    protected $descricao;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setProdutoID( $produtoID ){
        $this->produtoID = $produtoID;
        return $this;
    }

    public function getProdutoID(){
        return $this->produtoID;
    }

    public function setProdutoObj( $produtoObj ){
        $this->produtoObj = $produtoObj;
        if( $this->produtoObj ){
            $this->produtoID = $this->produtoObj->getID();
        }
        return $this;
    }

    public function getProdutoObj(){
        if( !$this->produtoObj ){
            $this->produtoObj = Produto::getByID($this->produtoID);
        }
        return $this->produtoObj;
    }
    
    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    
}