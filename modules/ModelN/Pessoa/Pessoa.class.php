<?php

namespace Pessoa;

use Lib\Validate as Validate;
use Lib\Format as Format;

use Pessoa\PessoaEmail    as Email;
use Pessoa\PessoaTelefone as Telefone;

class Pessoa extends \System\MyModel{
    
    public static $daoTable   = "pes_pessoa";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'             => "id",
        'razao'          => "razao",
        'fantasia'       => "fantasia",
        'slug'           => "slug",
        'cpf'            => "cpf",
        'cnpj'           => "cnpj",
        'dataNascimento' => "data_nascimento"
    );

    protected $ID;
    protected $razao;
    protected $fantasia;
    protected $slug;
    protected $cpf;
    protected $cnpj;
    protected $dataNascimento;
    
    protected $emails;
    protected $telefones;

    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setRazao( $razao ){
        $this->razao = $razao;
        return $this;
    }

    public function getRazao(){
        return $this->razao;
    }

    public function setFantasia( $fantasia ){
        $this->fantasia = $fantasia;
        return $this;
    }

    public function getFantasia(){
        return $this->fantasia;
    }

    public function setSlug( $slug ){
        $this->slug = $slug;
        return $this;
    }

    public function getSlug(){
        return $this->slug;
    }

    public function setCpf( $cpf ){
        $this->cpf = $cpf;
        return $this;
    }

    public function getCpf(){
        return $this->cpf;
    }

    public function setCnpj( $cnpj ){
        $this->cnpj = $cnpj;
        return $this;
    }

    public function getCnpj(){
        return $this->cnpj;
    }

    public function setDataNascimento( $dataNascimento )
    {
        $this->dataNascimento = $dataNascimento ? Format::dataToDb($dataNascimento) : null;
        return $this;
    }

    public function getDataNascimento(){
        return Format::dataToBr($this->dataNascimento);
    }

    protected function triggerBeforeInsertUpdate()
    {
        if( $this->cpf && !Validate::isCPF($this->cpf) ){
            $this->raise("CPF Inválido");
        }
        if( $this->cnpj && !Validate::isCNPJ($this->cnpj) ){
            $this->raise("CNPJ Inválido");
        }
        if( !$this->razao ){
            $this->raise("A razão social deve ser informada");
        }
        $this->cpf   = Format::formNumber($this->cpf);
        $this->cnpj  = Format::formNumber($this->cnpj);
        $this->razao = Format::formNomeProprio($this->razao);
        if( !$this->fantasia ){
            $this->fantasia = $this->razao;
        }
        $this->fantasia = Format::formNomeProprio($this->fantasia);
    }

    protected function triggerAfterSave()
    {
        $newSlug = trim(Format::strigToUrl(substr($this->cpf,0,2).$this->ID.'-'.$this->razao));
        if( $newSlug != $this->slug ){
            $this->slug = $newSlug;
            $this->save();
        }
    }

    public static function getBySlug( $slug )
    {
        $res = self::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        
        return !$res['cont_total'] >= 0 ? $res['rows'][0] : null;
    }
    
    public function setEmails( $emails )
    {
        $this->emails = $emails;
        return $this;
    }
    
    public function listaEmail()
    {
        if( !$this->emails ){
            $this->emails = Email::getList( array( 'pessoa_id = ?' => $this->ID ) );
        }
        return $this->emails;
    }
    
    public function getEmailPrincipal()
    {
        $emails = Email::getList(array( 'pessoa_id = ?' => $this->ID, 'principal = ?' => 'S' ));
        return $emails['cont_total'] > 0 ? $emails['rows'][0] : null;
    }
    
    public function setTelefones( $telefones )
    {
        $this->telefones = $telefones;
        return $this;
    }
    
    public function listaTelefone()
    {
        if( !$this->telefones ){
            $this->telefones = Telefone::getList( array('pessoa_id = ?' => $this->ID) );
        }
        return $this->telefones;
    }
    
}