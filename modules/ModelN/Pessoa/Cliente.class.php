<?php

namespace Pessoa;

use System\User as User;

class Cliente extends \System\MyModel{
    
    public static $daoTable = "pes_cliente";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'            => 'id',
        'pessoaID'      => 'pessoa_id',
        'status'        => 'status',
        'gerenteUserID' => 'gerente_user_id'
    );

    protected $ID;
    protected $pessoaID;
    protected $pessoaObj;
    protected $status;
    protected $gerenteUserID;
    protected $gerenteUserObj;
    
    public function getPessoaObj()
    {
        if( !$this->pessoaObj ){
            $this->pessoaObj = new Pessoa($this->pessoaID);
        }
        return $this->pessoaObj;
    }
    
    public function getGerenteUserObj()
    {
        if( !$this->gerenteUserObj ){
            $this->gerenteUserObj = new User($this->gerenteUserID);
        }
        return $this->gerenteUserObj;
    }
    
    public static function getByPessoa( $pessoa = null )
    {
        $id = is_object($pessoa) ? $pessoa->getID() : $pessoa;
        
        $res = self::getList(array(
            'pessoa_id = ?' => $id
        ), null, 0, 1);
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'][0];
        }else{
            return false;
        }
    }
    
    public function listaUsuarios()
    {
        $res = ClienteUsuario::getList(array(
            'cliente_id = ?' => $this->ID
        ), null, null, null, array('pes.razao'));
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'];
        }else{
            return false;
        }
    }
    
    public static function getList( $whereColumns = array( ), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array() )
    {
        $join = "
            join pes_pessoa pes on pes.id = dao.pessoa_id
            left join pes_pessoa_email em1 on em1.pessoa_id = pes.id and em1.principal = 'S'
        ";
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join);
    }
    
}