<?php

namespace Pessoa;

use Pessoa\Pessoa as Pessoa;

use Lib\Validate as Validate;

class PessoaEmail extends \System\MyModel{
    
    public static $daoTable   = "pes_pessoa_email";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'        => "id",
        'email'     => "email",
        'principal' => "principal",
        'pessoaID'  => "pessoa_id"
    );
    
    protected $ID;
    protected $email;
    protected $principal;
    protected $pessoaID;
    protected $pessoaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setEmail( $email ){
        $this->email = $email;
        return $this;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setPrincipal( $principal ){
        $this->principal = $principal;
        return $this;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !Validate::isEmail($this->new->email) ){
            _raise("E-mail inválido");
        }
        $this->new->email = trim($this->new->email);
    }
    
    public static function getPrincipalByPessoa( $pessoa ){
        return self::getByPessoa($pessoa, true);
    }
    
    public static function deleteByPessoa( $pessoa ){
        $dados = self::getByPessoa($pessoa);
        if( $dados ){
            foreach( $dados as $Obj ){
                if( !$Obj->save('D') ){
                    return false;
                }
            }
        }
        return true;
    }
    
}