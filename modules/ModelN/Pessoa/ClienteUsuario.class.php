<?php

namespace Pessoa;

use System\User as User;

class ClienteUsuario extends \System\MyModel{
    
    public static $daoTable = "pes_cliente_usuario";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'        => 'id',
        'clienteID' => 'cliente_id',
        'userID'    => 'user_id'
    );

    protected $ID;
    protected $clienteID;
    protected $clienteObj;
    protected $userID;
    protected $userObj;
    
    public function getClienteObj()
    {
        if( !$this->clienteObj ){
            $this->clienteObj = new Cliente($this->clienteID);
        }
        return $this->clienteObj;
    }
    
    public function getUserObj()
    {
        if( !$this->userObj ){
            $this->userObj = new User($this->userID);
        }
        return $this->userObj;
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction == 'I' ){
            $res = self::getList(array(
                'cliente_id = ?' => $this->clienteID,
                'user_id = ?' => $this->userID
            ), array('ID'), 0, 1);
            
            if( $res['cont_total'] > 0 ){
                $this->raise("Este usuário já está cadastrado para este cliente");
            }
        }
    }
    
    public static function getList( $whereColumns = array( ), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array() )
    {
        $join = "
            join pes_cliente cli on cli.id = dao.cliente_id
            join pes_pessoa pes on pes.id = cli.pessoa_id
        ";
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join);
    }
    
}