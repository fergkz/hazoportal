<?php

namespace Pessoa;

use Pessoa\Pessoa as Pessoa;

use Lib\Format as Format;

class PessoaTelefone extends \System\MyModel{
    
    public static $daoTable   = "pes_pessoa_telefone";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'        => "id",
        'telefone'  => "telefone",
        'principal' => "principal",
        'pessoaID'  => "pessoa_id"
    );
    
    protected $ID;
    protected $telefone;
    protected $principal;
    protected $pessoaID;
    protected $pessoaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setTelefone( $telefone ){
        $this->telefone = $telefone;
        return $this;
    }

    public function getTelefone(){
        return $this->telefone;
    }

    public function setPrincipal( $principal ){
        $this->principal = $principal;
        return $this;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setPessoaID( $pessoaID ){
        $this->pessoaID = $pessoaID;
        return $this;
    }

    public function getPessoaID(){
        return $this->pessoaID;
    }

    public function setPessoaObj( $pessoaObj ){
        $this->pessoaObj = $pessoaObj;
        if( $this->pessoaObj ){
            $this->pessoaID = $this->pessoaObj->getID();
        }
        return $this;
    }

    public function getPessoaObj(){
        if( !$this->pessoaObj ){
            $this->pessoaObj = Pessoa::getByID($this->pessoaID);
        }
        return $this->pessoaObj;
    }
    
    protected function triggerBeforeInsertUpdate(){
        $this->new->telefone = Format::formNumber($this->new->telefone);
        if( strlen($this->new->telefone) < 10 ){
            _raise("O telefone deve conter no mínimo 10 números (incluindo o DDD)");
        }
    }
    
    public static function getPrincipalByPessoa( $pessoa ){
        return self::getByPessoa($pessoa, true);
    }
    
    public static function getByPessoa( $pessoa, $first = null ){
        $pessoaID = is_object($pessoa) ? $pessoa->getID() : $pessoa;
        $sql = "select id from pes_pessoa_telefone where pessoa_id = :pessoa_id";
        $bind['pessoa_id'] = $pessoaID;
        $res = _query($sql, $bind);
        
        $dados = array();
        
        if( $res ){
            foreach( $res as $row ){
                if( $first ){
                    return self::getByID($row['id']);
                }
                $dados[] = new PessoaTelefone($row['id']);
            }
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByPessoa( $pessoa ){
        $dados = self::getByPessoa($pessoa);
        if( $dados ){
            foreach( $dados as $Obj ){
                if( !$Obj->save('D') ){
                    return false;
                }
            }
        }
        return true;
    }
    
}