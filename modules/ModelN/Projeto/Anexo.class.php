<?php

namespace Projeto;

use Projeto\Atividade as Atividade;
use Projeto\Demanda   as Demanda;
use Projeto\Operacao  as Operacao;

use Lib\Format               as Format;

class Anexo extends \System\MyModel
{   
    public static $daoTable   = "proj_anexo";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'            => 'id',
        'slug'          => 'slug',
        'atividadeID'   => 'atividade_id',
        'demandaID'     => 'demanda_id',
        'operacaoID'    => 'atividade_operacao_id',
        'principal'     => 'principal',
        'titulo'        => 'titulo',
        'descricao'     => 'descricao',
        'fileContent'   => 'file_content',
        'fileName'      => 'file_name',
        'fileType'      => 'file_type',
        'fileSize'      => 'file_size',
        'fileExtension' => 'file_extension'
    );
    
    protected $ID;
    protected $slug;
    protected $atividadeID;
    protected $atividadeObj;
    protected $demandaID;
    protected $demandaObj;
    protected $operacaoID;
    protected $operacaObj;
    protected $principal;
    protected $titulo;
    protected $descricao;
    protected $fileContent;
    protected $fileName;
    protected $fileType;
    protected $fileSize;
    protected $fileExtension;

    public function getAtividadeObj()
    {
        if( !$this->atividadeObj ){
            $this->atividadeObj = new Atividade($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function getDemandaObj()
    {
        if( !$this->demandaObj ){
            $this->demandaObj = new Demanda($this->demandaID);
        }
        return $this->demandaObj;
    }

    public function getOperacaoObj()
    {
        if( !$this->operacaoObj ){
            $this->operacaoObj = new Operacao($this->operacaoID);
        }
        return $this->operacaoObj;
    }

    protected function triggerBeforeSave()
    {       
        if( in_array($this->daoAction, array('I')) ){
            $tmp = explode(".", trim($this->fileName));
            $this->fileExtension = strtolower(end($tmp));
            $this->fileName = Format::strigToUrl(str_replace(".", "-", $this->fileName) );
        }
        
    }
    
    protected function triggerAfterSave()
    {   
        if( in_array($this->daoAction, array('I','U')) ){
            if( !empty($this->demandaID) ){
                $slug = "demanda-";
            }elseif( !empty($this->atividadeID) ){
                $slug = "atividade-";
            }elseif( !empty($this->operacaoID) ){
                $slug = "operacao-";
            }else{
                $this->raise("A demanda, atividade ou operação deve ser informada");
            }

            $slug .= $this->titulo ? $this->titulo : $this->fileName;
            $slug .= "-".$this->ID;

            $slug = Format::strigToUrl($slug);

            if( $this->slug !== $slug ){
                $this->slug = $slug;
                $this->save();
            }
        }
    }
    
    public static function getBySlug( $slug )
    {
        $res = self::getList(array(
            'dao.slug = ?' => $slug
        ), null, 0, 1);
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'][0];
        }else{
            return false;
        }
    }
    
    public static function listByDemanda( $demanda )
    {
        $id = is_object($demanda) ? $demanda->getID() : $demanda;
        
        $res = self::getList(array(
            'dao.demanda_id = ?' => $id
        ));
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'];
        }else{
            return false;
        }
    }
    
    protected static function listBy( $parent, $id, $loadContent = null )
    {
        switch( $parent ){
            case 'demanda':
                $bind['demanda_id'] = $id;
                $whereCond[] = "x.demanda_id = :demanda_id";
                break;
            case 'atividade':
                $bind['atividade_id'] = $id;
                $whereCond[] = "x.atividade_id = :atividade_id";
                break;
            case 'operacao':
                $bind['operacao_id'] = $id;
                $whereCond[] = "x.atividade_operacao_id = :operacao_id";
                break;
        }
        $where = "where ".implode(" and ", $whereCond);
        $sql = "select x.id from proj_anexo x {$where}";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            if( $loadContent ){
                $dados[] = Anexo::getByID($row['id']);
            }else{
                $dados[] = Anexo::getByID($row['id'])->setFileContent(null);
            }
        }
        return count($dados) > 0 ? $dados : null;
    }
    
    public static function listByAtividade( $Atividade, $loadContent = true )
    {
        $Atividade = is_object($Atividade) ? $Atividade->getID() : $Atividade;
        return self::listBy("atividade", $Atividade, $loadContent);
    }
    
}