<?php

namespace Projeto;

use Pessoa\Cliente as Cliente;
use Projeto\Atividade as Atividade;
use Projeto\DemandaGerente as Gerente;

use Core\Model\User as User;
use Lib\Format as Format;

class Demanda extends \System\MyModel{
    
    public static $daoTable   = "proj_demanda";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'              => 'id',
        'slug'            => 'slug',
        'clienteID'       => 'cliente_id',
        'titulo'          => 'titulo',
        'descricao'       => 'descricao',
        'status'          => 'status',
        'cadastroData'    => 'cad_data',
        'cadastroUserID'  => 'cad_user_id',
        'prioridade'      => 'prioridade',
        'tipoDemandaID'   => 'tipo_demanda_id'
    );
    
    protected $ID;
    protected $slug;
    protected $clienteID;
    protected $clienteObj;
    protected $titulo;
    protected $descricao;
    protected $status; # [A]berta, [C]oncluído, ca[N]celado, [S]tandby
    protected $cadastroData;
    protected $cadastroUserID;
    protected $cadastroUserObj;
    protected $prioridade;
    protected $tipoDemandaID;
    protected $tipoDemandaObj;
    
    protected $anexos;
    
    public function getClienteObj()
    {
        if( !$this->clienteObj ){
            $this->clienteObj = new Cliente($this->clienteID);
        }
        return $this->clienteObj;
    }

    public function getStatusDescricao()
    {
        $status = array(
            'A' => 'Aberta', 
            'C' => 'Concluída',
            'N' => 'Cancelada',
            'S' => 'Standby'
        );
        return $status[$this->status];
    }

    public function getCadastroUserObj()
    {
        if( !$this->cadastroUserObj ){
            $this->cadastroUserObj = User::getInstance($this->cadastroUserID);
        }
        return $this->cadastroUserObj;
    }

    public function getTipoDemandaObj()
    {
        if( !$this->tipoDemandaObj ){
            $this->tipoDemandaObj = new TipoDemanda($this->tipoDemandaID);
        }
        return $this->tipoDemandaObj;
    }
    
    public function getAtividade( $atividadeID = null )
    {
        return Atividade::getByDemandaAtividade( $this->ID, $atividadeID );
    }
    
    public function listaAnexos()
    {
        if( !$this->anexos ){
            $this->anexos = Anexo::listByDemanda($this, false);
        }
        return $this->anexos;
    }
    
    protected function triggerBeforeSave()
    {   
        if( $this->daoAction == 'I' ){
            
            $this->cadastroData = date("Y-m-d H:i:s");
            $this->cadastroUserID = User::online()->getID();
            if( !(int)$this->prioridade ){
                $sql = "select coalesce(max(prioridade),0)+1 as cont from proj_demanda where status = 'A'";
                $res = _query($sql);
                $this->prioridade = $res[0]['cont'];
            }
            
        }
        
        if( $this->clienteID ){
            $Cliente = new Cliente($this->clienteID);
            $this->slug = Format::strigToUrl($Cliente->getPessoaObj()->getRazao().'-'.$this->titulo);
        }else{
            $this->raise("O cliente deve ser informado");
        }
        
        if( $this->daoAction == 'D' ){
        
            $atividades = Atividade::listaByDemanda($this);
            if( $atividades ){
                foreach( $atividades as $Atividade ){
                    $Atividade->save("D");
                }
            }
            $gerentes = DemandaGerente::listaByDemanda($this);
            if( $gerentes ){
                foreach( $gerentes as $Gerente ){
                    $Gerente->save("D");
                }
            }
            Anexo::deleteByDemanda($this);
            
        }
    }
    
    public function listaAtividades(){
        return Atividade::listaByDemanda( $this->ID );
    }
    
    public static function getInstanceBySlug( $demandaSlug ){
        $sql = "select id from proj_demanda where slug = :slug";
        $bind['slug'] = $demandaSlug;
        $res = _query($sql, $bind);
        return new self(@$res[0]['id']);
    }
    
    public function changeStatus(){
        
        $atividades = $this->getAtividades();
        
        if( $atividades ){
            $cont = array('total'=>0);
            foreach( $atividades as $Atividade ){
                if( empty($cont[$Atividade->getEtapaID()]) ){
                    $cont[$Atividade->getEtapaID()] = 0;
                }
                $cont['total']++;
                $cont[$Atividade->getEtapaID()]++;
            }
            
            if( !empty($cont['U']) and $cont['U'] + (empty($cont['C']) ? 0 : $cont['C']) === $cont['total'] ){
                if( $this->status !== "C" ){   
                    $this->status = "C"; #[C]oncluída
                    $this->save();
                }
            }
            elseif( !empty($cont['A']) or !empty($cont['P']) or !empty($cont['G']) or !empty($cont['H']) or !empty($cont['V']) or !empty($cont['D']) or !empty($cont['I']) ){
                if( $this->status !== "A" ){   
                    $this->status = "A"; #[A]berta
                    $this->save();
                }
            }
            elseif( !empty($cont['C']) and $cont['C'] === $cont['total'] ){
                if( $this->status !== "N" ){   
                    $this->status = "N"; #ca[N]celada
                    $this->save();
                }
            }
        }elseif( $this->status !== "S" ){
            $this->status = "S"; #[S]tandby
            $this->save();
        }
    }
    
}