<?php

namespace Projeto;

use Projeto\Atividade as Atividade;

class AtividadeRequerida extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade_requerida";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'                    => "id",
        'atividadeRequeridaID'  => "atividade_requerida_id",
        'atividadeDependenteID' => "atividade_dependente_id"
    );
    
    protected $ID;
    protected $atividadeRequeridaID;
    protected $atividadeRequeridaObj;
    protected $atividadeDependenteID;
    protected $atividadeDependenteObj;
    
    public function getAtividadeRequeridaObj(){
        if( !$this->atividadeRequeridaObj ){
            $this->atividadeRequeridaObj = new Atividade($this->atividadeRequeridaID);
        }
        return $this->atividadeRequeridaObj;
    }

    public function getAtividadeDependenteObj(){
        if( !$this->atividadeDependenteObj ){
            $this->atividadeDependenteObj = new Atividade($this->atividadeDependenteID);
        }
        return $this->atividadeDependenteObj;
    }
    
    public static function listaByAtividadeDependente( $atividadeDependente = null ){
        $Dependente = is_object($atividadeDependente) ? $atividadeDependente : new Atividade($atividadeDependente);
        if( !$Dependente or !$Dependente->getID() ){
            return false;
        }
        $sql = "select id from proj_atividade_requerida where atividade_dependente_id = :atividade_dependente";
        $bind['atividade_dependente'] = $Dependente->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = new AtividadeRequerida($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function listaByAtividadeRequerida( $atividadeRequerida = null ){
        $Requerida = is_object($atividadeRequerida) ? $atividadeRequerida : new Atividade($atividadeRequerida);
        if( !$Requerida or !$Requerida->getID() ){
            return false;
        }
        $sql = "select id from proj_atividade_requerida where atividade_requerida_id = :atividade_requerida";
        $bind['atividade_requerida'] = $Requerida->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = new AtividadeRequerida($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByAtividadeDependente( $atividadeDependente = null ){
        $atividadesDependentes = self::listaByAtividadeDependente($atividadeDependente);
        if( !$atividadesDependentes ){
            return false;
        }
        foreach( $atividadesDependentes as $AtividadeRequerida ){
            if( !$AtividadeRequerida->save("D") ){
                return false;
            }
        }
        return true;
    }
    
    public static function deleteByAtividadeRequerida( $atividadeRequerida = null ){
        $atividadesRequeridas = self::listaByAtividadeRequerida($atividadeRequerida);
        if( !$atividadesRequeridas ){
            return false;
        }
        foreach( $atividadesRequeridas as $AtividadeRequerida ){
            if( !$AtividadeRequerida->save("D") ){
                return false;
            }
        }
        return true;
    }
    
}