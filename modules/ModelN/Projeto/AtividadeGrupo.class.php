<?php

namespace Projeto;

use Projeto\Atividade as Atividade;

use Core\Model\Group as Group;
use Core\Model\User as User;

class AtividadeGrupo extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade_grupo";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'           => 'id',
        'atividadeID'  => 'atividade_id',
        'groupID'      => 'group_id',
        'userID'       => 'user_id',
        'execucao'     => 'execucao'
    );
    
    protected $ID;
    protected $atividadeID;
    protected $atividadeObj;
    protected $groupID;
    protected $groupObj;
    protected $userID;
    protected $userObj;
    protected $execucao; # [H]omologação   [D]esenvolvimento
    
    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = new Atividade($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function getGroupObj(){
        if( !$this->groupObj ){
            $this->groupObj = Group::getByID($this->groupID);
        }
        return $this->groupObj;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
    protected function triggerBeforeSave(){
        if( !$this->groupID and !$this->userID ){
            $this->raise("O grupo ou usuário deve ser informado para o grupo de execução");
        }
    }
    
    public static function deleteByAtividade( $atividade = null ){
        $atividades = self::listaByAtividade($atividade);
        if( !$atividades ){
            return false;
        }
        foreach( $atividades as $Atividade ){
            if( !$Atividade->save("D") ){
                return false;
            }
        }
        return true;
    }
    
}