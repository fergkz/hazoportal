<?php

namespace Projeto;

class TipoDemanda extends \System\MyModel{
    
    public static $daoTable   = "proj_tipo_demanda";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'        => "id",
        'titulo'    => "titulo",
        'descricao' => "descricao"
    );
    
    public $ID;
    public $titulo;
    public $descricao;
    
    protected function triggerBeforeSave(){
        $this->ID = strtoupper($this->ID);
    }
    
}