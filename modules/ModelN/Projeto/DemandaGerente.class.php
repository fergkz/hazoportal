<?php

namespace Projeto;

use Projeto\Demanda as Demanda;
use Core\Model\User as User;

class DemandaGerente extends \System\MyModel{
    
    public static $daoTable   = "proj_demanda_gerente";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'         => 'id',
        'demandaID'  => 'demanda_id',
        'userID'     => 'user_id'
    );
    
    protected $ID;
    protected $demandaID;
    protected $demandaObj;
    protected $userID;
    protected $userObj;
    
    public function getDemandaObj(){
        if( !$this->demandaObj ){
            $this->demandaObj = new Demanda($this->demandaID);
        }
        return $this->demandaObj;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
    public static function listaByDemanda( $demanda ){
        $Demanda = is_object($demanda) ? $demanda : new Demanda($demanda);
        $sql = "select id from proj_demanda_gerente where demanda_id = :demanda_id";
        $bind['demanda_id'] = $Demanda->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = new self($row['id']);
        }
        return count($dados) ? $dados : false;
    }
    
    public static function clearByDemanda( $demanda ){
        $gerentes = self::listaByDemanda($demanda);
        if( $gerentes ){
            foreach( $gerentes as $Gerente ){
                $Gerente->save("D");
            }
        }
    }
    
    public static function isGerenteByDemanda( $demanda, $gerenteUser = null ){
        if( $gerenteUser ){
            $User = is_object($gerenteUser) ? $gerenteUser : User::getByID($gerenteUser);
        }else{
            $User = User::online();
        }
        if( $User->getGroupObj()->getMode() == "S" ){
            return true;
        }
        $gerentes = self::listaByDemanda($demanda);
        if( $gerentes ){
            foreach( $gerentes as $Gerente ){
                if( $Gerente->getUserID() == $User->getID() ){
                    return true;
                }
            }
        }
    }
    
    public static function isGerenteLogadoByDemanda( $demanda ){
        return self::isGerenteByDemanda($demanda);
    }
    
    public static function getList( $whereColumns = array( ), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array( ), $join = null )
    {
        $join = "
            join 
        ";
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order);
    }
    
}