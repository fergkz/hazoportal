<?php

namespace Projeto;

class AtividadeEtapa extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade_etapa";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'                => "id",
        'descricao'         => "descricao",
        'proximaEtapaID'    => "proxima_etapa",
        'permiteSelecionar' => "permite_selecionar"
    );
    
    protected $ID;
    protected $descricao;
    protected $proximaEtapaID;
    protected $proximaEtapaObj;
    protected $permiteSelecionar;
    
    public function getProximaEtapaObj(){
        if( !$this->proximaEtapaObj ){
            $this->proximaEtapaObj = new self($this->proximaEtapaID);
        }
        return $this->proximaEtapaObj;
    }
    
}