<?php

namespace Projeto;

use Projeto\Atividade as Atividade;
use Projeto\AtividadeEtapa as Etapa;

use Core\Model\User as User;

class Operacao extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade_operacao";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'                => 'id',
        'atividadeID'       => 'atividade_id',
        'userID'            => 'user_id',
        'dataInicio'        => 'data_inicio',
        'dataFinal'         => 'data_final',
        'descricao'         => 'descricao',
        'atividadeEtapaID'  => 'etapa_id'
    );
    
    protected $ID;
    protected $atividadeID;
    protected $atividadeObj;
    protected $userID;
    protected $userObj;
    protected $dataInicio;
    protected $dataFinal;
    protected $descricao;
    protected $atividadeEtapaID;
    protected $atividadeEtapaObj;
    
    protected $anexos;
    
    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = new Atividade($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }

    public function getAtividadeEtapaObj(){
        if( !$this->atividadeEtapaObj ){
            $this->atividadeEtapaObj = new Etapa($this->atividadeEtapaID);
        }
        return $this->atividadeEtapaObj;
    }
    
    public function listaAnexos()
    {
        $res = Anexo::getList(array(
            'dao.atividade_operacao_id = ?' => $this->ID
        ));
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'];
        }else{
            return false;
        }
    }
    
    protected function triggerBeforeSave(){
        if( $this->daoAction == 'I' || $this->daoAction = 'U' ){
            if( !$this->dataInicio ){
                $this->dataInicio = date("Y-m-d H:i:s");
            }
            if( !$this->userID ){
                $this->userID = User::online()->getID();
            }
            if( !$this->atividadeEtapaID && $this->atividadeID ){
                $Atv = new Atividade($this->atividadeID);
                $this->atividadeEtapaID = $Atv->getEtapaID();
            }
        }
        
        if( $this->daoAction == 'D' ){
            Anexo::deleteByOperacao($this);
        }
    }
    
    public function abre(){
        return $this->save();
    }
    
    public function fecha(){
        $this->dataFinal = date("Y-m-d H:i:s");
        return $this->save();
    }
    
    public static function getEmAbertoByAtividadeLogado( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : new Atividade($atividade);
        $User = User::online();
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id
                   and user_id = :user_id
                   and data_final is null";
        $bind['atividade_id'] = $Atividade->getID();
        $bind['user_id']      = $User->getID();
        $res = _query($sql, $bind);
        if( @$res[0]['id'] ){
            return new self($res[0]['id']);
        }else{
            return false;
        }
    }
    
    public static function getEmAbertoByAtividade( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : new Atividade($atividade);
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id
                   and data_final is null";
        $bind['atividade_id'] = $Atividade->getID();
        $res = _query($sql, $bind);
        if( @$res[0]['id'] ){
            return new self($res[0]['id']);
        }else{
            return false;
        }
    }
    
    public static function listaByAtividade( $atividade ){
        $Atividade = is_object($atividade) ? $atividade : new Atividade($atividade);
        $sql = "select id 
                  from proj_atividade_operacao
                 where atividade_id = :atividade_id 
                 order by data_inicio asc";
        $bind['atividade_id'] = $Atividade->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = new self($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function deleteByAtividade( $atividade ){
         $operacoes = self::listaByAtividade($atividade);
         if( $operacoes ){
             foreach( $operacoes as $Operacao ){
                 if( !$Operacao->save('D') ){
                     return false;
                 }
             }
         }
         return true;
    }
    
}