<?php

namespace Projeto;

use Projeto\Demanda as Demanda;
use Projeto\Atividade as Atividade;
use Projeto\AtividadeEtapa as Etapa;
use Projeto\Anexo as Anexo;
use Pessoa\Cliente as Cliente;

use System\User as User;
use Lib\Format as Format;

class Atividade extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'                 => 'id',
        'demandaID'          => 'demanda_id',
        'parentAtividadeID'  => 'parent_atividade_id',
        'titulo'             => 'titulo',
        'descricao'          => 'descricao',
        'duracaoEstimada'    => 'duracao_estimada',
        'etapaID'            => 'etapa_id',
        'prioridade'         => 'prioridade',
        'cadastroUserID'     => 'cad_user_id',
        'clienteID'          => 'cliente_id',
        'dataPrevisaoInicio' => 'data_prev_inicio',
        'dataPrevisaoFinal'  => 'data_prev_final'
    );
    
    protected $ID;
    protected $demandaID;
    protected $demandaObj;
    protected $parentAtividadeID;
    protected $parentAtividadeObj;
    protected $titulo;
    protected $descricao;
    protected $duracaoEstimada;
    protected $etapaID;
    protected $etapaObj;
    protected $prioridade;
    protected $clienteID;
    protected $clienteObj;
    protected $cadastroUserID;
    protected $cadastroUserObj;
    protected $dataPrevisaoInicio;
    protected $dataPrevisaoFinal;

    protected $atividadesRequeridas;
    protected $gruposDesenvolvimento;
    protected $gruposHomologacao;
    protected $operacoes;
    protected $anexos;
    
    public function getDemandaObj(){
        if( !$this->demandaObj ){
            $this->demandaObj = new Demanda($this->demandaID);
        }
        return $this->demandaObj;
    }

    public function getParentAtividadeObj(){
        if( !$this->parentAtividadeObj ){
            $this->parentAtividadeObj = new Atividade($this->parentAtividadeID);
        }
        return $this->parentAtividadeObj;
    }
    
    public function getCadastroUserObj()
    {
        if( !$this->cadastroUserObj ){
            $this->cadastroUserObj =  new User($this->cadastroUserID);
        }
        return $this->cadastroUserObj;
    }
    
    public function getDuracaoEstimadaHoras(){
        return Format::converterMinutosEmHoras($this->duracaoEstimada);
    }

    public function getEtapaObj(){
        if( !$this->etapaObj ){
            $this->etapaObj = new Etapa($this->etapaID);
        }
        return $this->etapaObj;
    }

    public function getClienteObj(){
        if( !$this->clienteObj ){
            $this->clienteObj = new Cliente($this->clienteID);
        }
        return $this->clienteObj;
    }
    
    public function getAtividadesRequeridas(){
        if( !$this->atividadesRequeridas ){
            $this->atividadesRequeridas = AtividadeRequerida::listaByAtividadeDependente( $this->ID );
        }
        return $this->atividadesRequeridas;
    }
    
    public function getGruposDesenvolvimento(){
        if( !$this->gruposDesenvolvimento ){
            $grupos = AtividadeGrupo::listaByAtividade( $this->ID );
            if( $grupos ){
                foreach( $grupos as $row ){
                    if( $row->getExecucao() != 'D' ){
                        continue;
                    }
                    $this->gruposDesenvolvimento[] = $row;
                }
            }
        }
        return $this->gruposDesenvolvimento;
    }
    
    public function getGruposHomologacao(){
        if( !$this->gruposHomologacao ){
            $grupos = AtividadeGrupo::listaByAtividade( $this->ID );
            if( $grupos ){
                foreach( $grupos as $row ){
                    if( $row->getExecucao() != 'H' ){
                        continue;
                    }
                    $this->gruposHomologacao[] = $row;
                }
            }
        }
        return $this->gruposHomologacao;
    }
    
    public function listaOperacoes()
    {
        $res = Operacao::getList(array(
            'dao.atividade_id = ?' => $this->ID
        ), null, null, null, array(
            'dao.data_inicio desc'
        ));
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'];
        }else{
            return false;
        }
    }
    
    public function getOperacaoEmExecucao(){
        return Operacao::getEmAbertoByAtividade($this);
    }
    
    public function getTempoOperacao( $formatoRetorno = "H:i:s" ){
        $operacoes = Operacao::listaByAtividade($this);
        $tempo = 0;
        if( $operacoes ){
            foreach( $operacoes as $Operacao ){
                $tempo += Format::subDatas(_coalesce($Operacao->getDataFinal(), date("Y-m-d H:i:s")), $Operacao->getDataInicio(), "s");
            }
        }
        switch( $formatoRetorno ){
            case "H:i:s":
                return Format::formataSegundosHoras($tempo);
                break;
            case "s":
                return $tempo;
                break;
            case "m":
                return $tempo/60;
                break;
        }
    }
    
    public function getPercTempoOperacao(){
        $perc1 = $this->duracaoEstimada / 100;
        $perc = $perc1 * $this->getTempoOperacao("m");
        return number_format(_coalesce($this->getTempoOperacao("m")) / _coalesce($this->duracaoEstimada, 1) * 100, 2);
    }
    
    public function listaAnexos(){
        if( !$this->anexos ){
            $this->anexos = Anexo::listByAtividade($this, false);
        }
        return $this->anexos;
    }
    
    protected function triggerBeforeSave(){
        
        if( $this->daoAction == 'D' ){
            AtividadeGrupo::deleteByAtividade( $this->ID );
            AtividadeRequerida::deleteByAtividadeDependente( $this->ID );
            AtividadeRequerida::deleteByAtividadeRequerida( $this->ID );
            Operacao::deleteByAtividade( $this );
            Anexo::deleteByAtividade( $this );
        }
        
        if( $this->daoAction == 'I' ){
            if( $this->demandaID && !(int)$this->prioridade ){
                $sql = "select max(prioridade)+1 as cont from proj_atividade where demanda_id = :demanda_id";
                $bind['demanda_id'] = $this->demandaID;
                $res = _query($sql, $bind);
                $this->prioridade = $res[0]['cont'];
            }
            $this->cadastroUserID = User::online()->getID();
        }
        
        if( in_array($this->daoAction, array('I','U')) ){
            if( $this->demandaID and $this->clienteID ){
                $this->raise("Não é possível informar um cliente e uma demanda para a mesma atividade");
            }
        }
    }
    
    protected function triggerAfterSave(){
        if( $this->demandaID ){
            $this->getDemandaObj()->changeStatus();
        }
        if( @$this->old->demandaID ){
            Demanda::getInstance($this->old->demandaID)->changeStatus();
        }
    }
    
    public function atualizarProximaEtapa(){
        if( $this->etapaID == "V" ){
            $sql = "select count(*) as cont 
                      from proj_atividade_grupo
                     where atividade_id = :atividade_id
                       and execucao = 'H'";
            $bind['atividade_id'] = $this->ID;
            $res = _query($sql, $bind);
            if( $res[0]['cont'] > 0 ){
                $this->etapaID = "H";
                return $this->save();
            }else{
                $this->etapaID = "U";
                return $this->save();
            }
        }
    }
    
    public function estaEmExecucao(){
        return Operacao::getEmAbertoByAtividade($this);
    }
    
    public static function listaByDemanda( $demanda = null ){
        $id = is_object($demanda) ? $demanda->getID() : $demanda;
        
        $res = Atividade::getList(array(
            'dao.demanda_id = ?' => $id
        ), null, null, null, array(
            "field(dao.etapa_id, 'U', 'C')",
            'dao.prioridade',
            'dao.titulo'
        ));
        
        if( $res['cont_total'] > 0 ){
            return $res['rows'];
        }else{
            return false;
        }
    }
    
    public static function listaSemDemanda( $User = null ){
        if( $User ){
            $bind['user_id'] = is_object($User) ? $User->getID() : $User;
        }else{
            $bind['user_id'] = $User = User::online()->getID();
        }
        $sql = "select id from proj_atividade 
                 where demanda_id is null and cad_user_id = :user_id
                 order by field(etapa_id, 'U', 'C'), prioridade, titulo";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = new Atividade($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function getByDemandaAtividade( $demanda = null, $atividade = null ){
        $Atividade = is_object($atividade) ? $atividade : new Atividade($atividade);
        $Demanda   = is_object($demanda)   ? $demanda   : new Demanda($demanda);
        if( !$Atividade || !$Demanda || $Atividade->getDemandaID() != $Demanda->getID() ){
            return false;
        }else{
            return $Atividade;
        }
    }
    
    public static function listaAtividadesDesenvolvimentoLogado( $comDemanda = true ){
        return self::listaAtividadesLogado("D", $comDemanda);
    }
    
    public static function listaAtividadesHomologacaoLogado( $comDemanda = true ){
        return self::listaAtividadesLogado("H", $comDemanda);
    }
    
    protected static function listaAtividadesLogado( $etapaID, $comDemanda = true  ){
        $sql = "select a.id as atividade_id, d.id as demanda_id
                  from proj_atividade a
                  left join proj_demanda d on d.id = a.demanda_id";
        if( $etapaID == "D" ){
            $where[] = "a.etapa_id in ('D','P','I')";
            $where[] = "a.id in ( select atividade_id 
                                    from proj_atividade_grupo 
                                   where ( group_id = :group_id
                                           or
                                           user_id = :user_id )
                                     and execucao = 'D')";
        }elseif( $etapaID == "H" ){
            $where[] = "a.etapa_id in ('H','G','V')";
            $where[] = "a.id in ( select atividade_id 
                                    from proj_atividade_grupo 
                                   where ( group_id = :group_id
                                           or
                                           user_id = :user_id )
                                     and execucao = 'H')";
        }
        if( $comDemanda ){
            $where[] = "d.status = 'A'";
        }else{
            $where[] = "d.status is null";
        }
        $bind['group_id'] = User::online()->getGroupID();
        $bind['user_id']  = User::online()->getID();
        $sql .= " where ".implode(" and ", $where)." order by d.prioridade, d.titulo, a.prioridade";
        $res = _query($sql, $bind);
        $dados = array();
        $cont = 0;
        foreach( $res as $row ){
            if( empty($dados[$row['demanda_id']]['Demanda']) ){
                $dados[$row['demanda_id']]['Demanda'] = new Demanda($row['demanda_id']);
            }
            $dados[$row['demanda_id']]["atividades"][] = new self($row['atividade_id']);
            $cont++;
        }
        return count($dados) > 0 ? $dados : false;
    }
    
    public static function permiteLogadoDesenvolver( $atividade = null ){
        return self::permiteLogadoExecutar($atividade, "D");
    }
    
    public static function permiteLogadoHomologar( $atividade = null ){
        return self::permiteLogadoExecutar($atividade, "H");
    }
    
    private static function permiteLogadoExecutar( $atividade, $mode ){
        $Atividade = is_object($atividade) ? $atividade : new self($atividade);
        if( !$Atividade ){
            return false;
        }
        $sql = "select count(*) as cont
                  from proj_atividade_grupo g
                  join proj_atividade a on a.id = g.atividade_id
                 where g.atividade_id = :atividade_id
                   and ( g.group_id = :group_id or g.user_id = :user_id ) ";
        $bind['atividade_id'] = $Atividade->getID();
        $bind['group_id']     = User::online()->getGroupID();
        $bind['user_id']      = User::online()->getID();
        if( $mode == "D" ){
            $sql .= "and a.etapa_id in ('D','P') ";
        }elseif( $mode == "H" ){
            $sql .= "and a.etapa_id in ('H','V') ";
        }

        $res = _query($sql, $bind);
        
        return $res[0]['cont'] > 0 ? true : false;
    }
    
    public static function getIndependenteAberta( $user = null ){
        if( $user ){
            $bind['user_id'] = is_object($user) ? $user->getID() : $user;
        }else{
            $bind['user_id'] = User::online()->getID();
        }
        $sql = "select a.id 
                  from proj_atividade a
                 where a.etapa_id = 'I'
                   and a.cad_user_id = :user_id";
        $res = _query($sql, $bind);
        if( $res and $res[0] ){
            return new self($res[0]['id']);
        }
        return false;
    }
    
}