<?php

namespace Projeto;

use Core\Model\User as User;

class AtividadePadroes extends \System\MyModel{
    
    public static $daoTable   = "proj_atividade_padroes";
    public static $daoPrimary = array('ID' => "id");
    public static $daoCols    = array(
        'ID'      => "id",
        'userID'  => "user_id",
        'content' => "content"
    );
    
    protected $ID;
    protected $userID;
    protected $userObj;
    protected $content;
    
    public function getUserObj(){
        if( !$this->userObj ){
            $this->userObj = User::getByID($this->userID);
        }
        return $this->userObj;
    }
    
    public function setContent( $content ){
        $this->content = serialize($content);
        return $this;
    }

    public function getContent(){
        return $this->content ? unserialize($this->content) : array();
    }
    
    public function triggerBeforeSave(){
        if( !$this->userID ){
            $this->userID = User::online()->getID();
        }
    }
    
    public static function getByLogado(){
        $sql = "select id from proj_atividade_padroes where user_id = :user_id";
        $bind['user_id'] = User::online()->getID();
        $res = _query($sql, $bind);
        return new self( @$res[0]['id'] );
    }
    
}