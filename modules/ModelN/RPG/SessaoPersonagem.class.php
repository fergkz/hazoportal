<?php

namespace RPG;

use System\User as User;

class SessaoPersonagem extends \System\MyModel
{    
    public static $daoTable = "rpg_sessao_personagem";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'     => 'id',
        'sessaoID'   => 'sessao_id',
        'userID'   => 'user_id',
        'nome' => 'nome',
        'anexoImagemID' => 'anexo_id_imagem',
        'tipo' => 'tipo'
    );

    protected $ID;
    protected $sessaoID;
    protected $userID;
    protected $userObj;
    protected $nome;
    protected $anexoImagemID;
    protected $tipo;
    
    public function getUserObj()
    {
        return new User($this->userID);
    }

}