<?php

namespace RPG;

class Sessao extends \System\MyModel
{    
    public static $daoTable = "rpg_sessao";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'     => 'id',
        'data'   => 'data',
        'titulo' => 'titulo',
        'resumo' => 'resumo',
        'userCadastroID' => 'user_id_cad'
    );

    protected $ID;
    protected $data;
    protected $titulo;
    protected $resumo;
    protected $userCadastroID;
    
    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array() )
    {
        $join = "
            join rpg_sessao_personagem per on per.sessao_id = dao.id
        ";

        $groupBy[] = "dao.id";
        
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
    }
    
    public function setChat( $personagemID, $mensagem, $tipo )
    {
        $Chat = new Chat();
        $Chat->setSessaoPersonagemID( $personagemID );
        $Chat->setMessage( $mensagem );
        $Chat->setData( date('Y-m-d H:i:s') );
        $Chat->setTipo( $tipo );
        
        return $Chat->save();
    }
    
}