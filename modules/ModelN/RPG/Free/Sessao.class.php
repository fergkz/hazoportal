<?php

namespace RPG\Free;

use Core\System\Functions as CoreFunctions;

class Sessao extends \System\MyModel
{    
    public static $daoTable = "rpgn_sessao";
    public static $daoPrimary = array('token' => 'token');
    public static $daoCols = array(
        'token'  => 'token',
        'data'   => 'data',
        'titulo' => 'titulo',
        'resumo' => 'resumo'
    );

    protected $token;
    protected $data;
    protected $titulo;
    protected $resumo;
    
    public static function getList( $whereColumns = array(), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array() )
    {
        $join = "
            join rpgn_personagem per on per.sessao_token = dao.token
        ";

        $groupBy[] = "dao.token";
        
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
    }
    
    public function setChat( $personagemToken, $mensagem, $tipo )
    {   
        if( !$mensagem ){
            return false;
        }
        
        $Chat = new Chat();
        $Chat->setPersonagemToken( $personagemToken );
        $Chat->setMessage( $mensagem );
        $Chat->setData( date('Y-m-d H:i:s') );
        $Chat->setTipo( $tipo );
        
        return $Chat->save();
    }
    
    protected function triggerBeforeSave()
    {
        
        if( $this->daoAction == 'I' ){
            
            $this->token = substr(str_replace("-", "X", CoreFunctions::crypt(time(), "RPG_ACCESS_TOKEN", 25)) ,10, 30);
            $this->data = date("Y-m-d H:i:s");
        }
        
        if( !$this->titulo ){
            $this->raise("O nome da sala deve ser informado");
        }
        
    }
    
    public function geraFilesFolder()
    {   
        $filename = path.ds.$GLOBALS['url_path_images'];

        if( !file_exists($filename) ){
            mkdir($filename, 0777, true);
            chmod($filename, 0777);
        }
        
        $filename .= ds.substr($this->getData(), 0, 10);

        if( !file_exists($filename) ){
            mkdir($filename, 0777, true);
            chmod($filename, 0777);
        }
        
        return $filename;
    }
    
    public function listaPersonagens( $status = null ){
        $where['dao.sessao_token = ?'] = $this->token;
        $where['dao.token <> ?'] = Personagem::getOnline()->getToken();
        
        if( $status ){
            $where['dao.status = ?'] = $status;
        }
        
        return Personagem::getList($where, null, 0, null, array('dao.data_ativacao asc'));
    }
    
}