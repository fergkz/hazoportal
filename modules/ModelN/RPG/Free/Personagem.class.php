<?php

namespace RPG\Free;

use Core\System\Functions as CoreFunctions;
use Core\System\Mail as CoreMail;
use MVC\Session as Session;

use RPG\Free\Sessao as Sessao;

class Personagem extends \System\MyModel
{    
    public static $daoTable = "rpgn_personagem";
    public static $daoPrimary = array('token' => 'token');
    public static $daoCols = array(
        'token'          => 'token',
        'personagemNome' => 'nome_personagem',
        'usuarioNome'    => 'nome_usuario',
        'tipo'           => 'tipo', # Mestre, Personagem, NPC
        'sessaoToken'    => 'sessao_token',
        'email'          => 'email',
        'status'         => 'status', # Pendente, Bloqueado, Ativo, Inativo
        'dataAtivacao'   => 'data_ativacao'
    );
    public static $tipoDescricao = array(
        "M" => "Mestre",
        "P" => "Personagem",
        "N" => "NPC"
    );
    public static $statusDescricao = array(
        "P" => "Pendente",
        "B" => "Bloqueado",
        "A" => "Ativo",
        "I" => "Inativo"
    );

    protected $token;
    protected $personagemNome;
    protected $usuarioNome;
    protected $tipo;
    protected $sessaoToken;
    protected $email;
    protected $status;
    protected $dataAtivacao;
    
    public function getAnexoImagemObj()
    {
        $list = Anexo::getList(array(
            'dao.personagem_token = ?' => $this->token,
            'dao.principal = ?' => "S"
        ), null, 0, 1);
        
        return $list['cont_total'] > 0 ? $list['rows'][0] : false;
    }
    
    public function getSessaoObj()
    {
        return new Sessao( $this->sessaoToken );
    }
    
    public static function getOnline()
    {
        return Session::get("rpg_personagem_online");
    }
    
    public function setOnline()
    {
        Session::set("rpg_personagem_online", $this);
    }
    
    protected function triggerBeforeSave()
    {
        if( $this->daoAction == 'I' ){
            
            $this->token = substr(str_replace("-", "X", CoreFunctions::crypt(time(), "RPG_ACCESS_TOKEN", 35)) ,10, 40);
            $this->status = "P";
            
        }
        
        if( !$this->email ){
            $this->raise("O e-mail deve ser informado");
        }
        
        if( $this->status === "A" ){
            $this->dataAtivacao = date("Y-m-d H:i:s");

            if( !$this->personagemNome ){
                $this->raise("O nome do personagem deve ser informado");
            }
            if( !$this->usuarioNome ){
                $this->raise("O nome completo deve ser informado");
            }
            
            if( !$this->getAnexoImagemObj() ){
                $this->raise("A imagem do personagem deve ser informada");
            }
        }
        
    }
    
    public function triggerAfterSave()
    {
        if( $this->daoAction == 'I' ){
            $Mail = new CoreMail();

            $lista = Sessao::getList(array(
                "dao.token = ?" => $this->sessaoToken
            ), null, 0, 1);
            $Sessao =& $lista['rows'][0];

            $render['url_acesso'] = url_mode."/cadastro/acesso-email/".$Sessao->getToken()."/".$this->token;
            $render['tipo'] = $this->tipo;
            $render['sala_nome'] = $Sessao->getTitulo();

            $Mail->setAddress( $this->email );
            $Mail->setMessageTemplate("private/email/rpg-free/email-personagem-novo-cadastro.twig", $render);
            $Mail->setSubject("RPG Free - ".$render['sala_nome']);
            
            if( $Mail->phpmailer->ErrorInfo ){
                $this->raise($Mail->phpmailer->ErrorInfo);
            }
            
            if( !$Mail->send() ){
                $this->raise("Falha ao enviar e-mail");
            }
            
        }
    }

}