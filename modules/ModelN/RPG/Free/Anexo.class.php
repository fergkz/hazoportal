<?php

namespace RPG\Free;

use Lib\WideImage\Image as Image;

class Anexo extends \System\MyModel
{    
    public static $daoTable = "rpgn_anexo";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'              => 'id',
        'content'         => 'content',
        'chatID'          => 'chat_id',
        'name'            => 'name',
        'type'            => 'type',
        'ref'             => 'ref', # [I]magem, [T]exto
        'sessaoToken'     => 'sessao_token',
        'personagemToken' => 'personagem_token',
        'principal'       => 'principal',
        'extensao'        => 'extensao',
        'urlPath'         => 'url_path',
        'contChange'      => 'cont_change'
    );

    protected $ID;
    protected $content;
    protected $chatID;
    protected $name;
    protected $type;
    protected $ref;
    protected $sessaoToken;
    protected $personagemToken;
    protected $principal;
    protected $extensao;
    protected $urlPath;
    protected $contChange;
    
    private $saveFile = true;
    
    public function getImagemUrl($size)
    {
        return url."/{$GLOBALS['url_path_images']}/{$this->urlPath}/{$size}.{$this->extensao}";
    }
    
    public function geraArquivo()
    {
        $sizes = array(50,150,300,450,600);
            
        if( $this->personagemToken ){

            $Personagem = new Personagem( $this->personagemToken );

            $Sessao = $Personagem->getSessaoObj();

            $filename = $Sessao->geraFilesFolder().ds.$Personagem->getToken();
            if( !file_exists($filename) ){
                mkdir($filename, 0777, true);
                chmod($filename, 0777);
            }

            $filename .= ds."images";
            if( !file_exists($filename) ){
                mkdir($filename, 0777, true);
                chmod($filename, 0777);
            }

            $filename .= ds.$this->ID;
            if( !file_exists($filename) ){
                mkdir($filename, 0777, true);
                chmod($filename, 0777);
            }

            foreach( $sizes as $size ){
                $filenamePng = $filename.ds.$size.".".$this->extensao;
                
                if( file_exists($filenamePng) ){
                    unlink($filenamePng);
                }
                
                Image::loadFromString($this->content)
                    ->resize($size)
                    ->saveToFile( $filenamePng );
                chmod($filenamePng, 0777);
                    
                if( $size <= 300 ){
                    $Image = Image::loadFromString($this->content);

                    if( $Image->getWidth() > $Image->getHeight() ){
                        $Image = $Image->resize(null, $size);
                    }elseif( $Image->getWidth() <= $Image->getHeight() ){
                        $Image = $Image->resize($size);
                    }

                    $filenamePng = $filename.ds."crop_{$size}.".$this->extensao;
                    $Image->crop('center','center',$size,$size)->saveToFile($filenamePng);
                    chmod($filenamePng, 0777);
                }
            }

            $this->urlPath = substr($Sessao->getData(),0,10)."/".$Personagem->getToken()."/images/".$this->ID;
            $this->saveFile = false;
            $this->save();
        }
        
        elseif( $this->sessaoToken ){
            $Sessao = new Sessao( $this->sessaoToken );
            
            $filename = $Sessao->geraFilesFolder().ds."files";
            if( !file_exists($filename) ){
                mkdir($filename, 0777, true);
                chmod($filename, 0777);
            }

            $filename .= ds.$this->ID;
            if( !file_exists($filename) ){
                mkdir($filename, 0777, true);
                chmod($filename, 0777);
            }
            
            
            if( $this->ref == 'I' ){
                
                foreach( $sizes as $size ){
                    $filenamePng = $filename.ds.$size.".".$this->extensao;

                    if( file_exists($filenamePng) ){
                        unlink($filenamePng);
                    }

                    $Image = Image::loadFromString($this->content)
                        ->resize($size)
                        ->saveToFile( $filenamePng );
                    chmod($filenamePng, 0777);
                    
                    if( $size <= 300 ){
                        $Image = Image::loadFromString($this->content);
                        
                        if( $Image->getWidth() > $Image->getHeight() ){
                            $Image = $Image->resize(null, $size);
                        }elseif( $Image->getWidth() <= $Image->getHeight() ){
                            $Image = $Image->resize($size);
                        }
                        
                        $filenamePng = $filename.ds."crop_{$size}.".$this->extensao;
                        $Image->crop('center','center',$size,$size)->saveToFile($filenamePng);
                        chmod($filenamePng, 0777);
                    }
                    
                }
                
                $this->urlPath = substr($Sessao->getData(),0,10)."/files/".$this->ID;

            }
            elseif( $this->ref == 'T' ){
                $filename .= ds."content.html";
                file_put_contents($filename, $this->content);
                chmod($filename, 0777);
                $this->urlPath = substr($Sessao->getData(),0,10)."/files/{$this->ID}/content.html";
                
            }

            $this->saveFile = false;
            $this->save();
            
        }
        
    }
    
    protected function triggerBeforeSave()
    {
        switch( $this->daoAction ){
            case "I":
                $this->contChange = 1;
                break;
            default:
                $this->contChange++;
                break;
        }
    }
    
    protected function triggerAfterSave()
    {
        if( $this->saveFile ){
            $this->geraArquivo();
            $this->saveFile = true;
        }
    }
    
}