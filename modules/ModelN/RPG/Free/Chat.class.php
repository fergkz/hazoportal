<?php

namespace RPG\Free;

class Chat extends \System\MyModel
{    
    public static $daoTable = "rpgn_chat";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'              => 'id',
        'data'            => 'data',
        'message'         => 'msg',
        'tipo'            => 'tipo',
        'personagemToken' => 'personagem_token'
    );

    protected $ID;
    protected $data;
    protected $message;
    protected $tipo;
    protected $personagemToken;
    protected $personagemObj;
    
    public function getPersonagemObj(){
        $this->personagemObj = new Personagem( $this->personagemToken );
        return $this->personagemObj;
    }
    
    public static function getList( $whereColumns = array( ), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array( ) )
    {
        $join = "
            join rpgn_personagem per on per.token = dao.personagem_token
        ";
        
        $groupBy = null;
        
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
    }
    
}