<?php

namespace RPG;

class Anexo extends \System\MyModel
{    
    public static $daoTable = "rpg_anexo";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'     => 'id',
        'content'   => 'content',
        'chatID' => 'chat_id',
        'sessaoID' => 'sessao_id',
        'name' => 'name',
        'type' => 'type',
        'ref' => 'ref'
    );

    protected $ID;
    protected $content;
    protected $chatID;
    protected $sessaoID;
    protected $name;
    protected $type;
    protected $ref;
    
}