<?php

namespace RPG;

class Chat extends \System\MyModel
{    
    public static $daoTable = "rpg_chat";
    public static $daoPrimary = array('ID' => 'id');
    public static $daoCols = array(
        'ID'     => 'id',
        'data'   => 'data',
        'message' => 'msg',
        'tipo' => 'tipo',
        'sessaoPersonagemID' => 'sessao_personagem_id'
    );

    protected $ID;
    protected $data;
    protected $message;
    protected $tipo;
    protected $sessaoPersonagemID;
    protected $sessaoPersonagemObj;
    
    public function getSessaoPersonagemObj(){
        $this->sessaoPersonagemObj = new SessaoPersonagem( $this->sessaoPersonagemID );
        return $this->sessaoPersonagemObj;
    }
    
    public static function getList( $whereColumns = array( ), $loadAttributes = null, $rowStart = 0, $rowLimit = null, $order = array( ) )
    {
        $join = "
            join rpg_sessao_personagem per on per.id = dao.sessao_personagem_id
        ";
        
        $groupBy = null;
        
        return parent::getList($whereColumns, $loadAttributes, $rowStart, $rowLimit, $order, $join, $groupBy);
    }
    
    
    
}