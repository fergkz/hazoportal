<?php

use MVC\Controller as Controller;
use Model\Pessoa\Pessoa as Pessoa;

class PessoasController extends Controller{
    
    public function indexAction( $consulta = false ){
        
        $render = array();
        
        if( $consulta ){
            $render['pessoas'] = Pessoa::consulta($_POST['term']);
        }
        
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $pessoaID = null, $ajax = false ){
        $Pessoa = Pessoa::getByID($pessoaID);
        
        if( !$Pessoa ){
            return 404;
        }
        
        $render['Pessoa'] = &$Pessoa;
        
        if( $ajax ){
            $this->view()->setTemplate("pessoas/visualizacao-ajax.html")->display($render);
        }else{
            $this->view()->display($render);
        }
    }
    
    public function cadastroAction( $pessoaID = null ){

        $Pessoa = Pessoa::getInstance($pessoaID);
        
        if( $_POST ){
            
            $Pessoa->setCpf($_POST['cpf'])
                   ->setCnpj($_POST['cnpj'])
                   ->setRazao($_POST['razao'])
                   ->setFantasia($_POST['fantasia']);
            if( $Pessoa->save() ){
                _setSuccess("Cadastro salvo com sucesso!");
                $this->redirect(url."/u/pessoas/visualizacao/$/".$Pessoa->getID());
            }
            
        }
        
        $render['Pessoa'] = &$Pessoa;
        
        $this->view()->display($render);
    }
    
    public function consultaAction(){
        $res = Pessoa::search("full");
        die($res);
    }
    
}