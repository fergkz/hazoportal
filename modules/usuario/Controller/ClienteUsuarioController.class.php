<?php

use Pessoa\Cliente as Cliente;
use Pessoa\ClienteUsuario as ClienteUsuario;

use System\User as User;

class ClienteUsuarioController extends \System\MyController
{
    public function __construct()
    {
        parent::__construct();
        
        $this->breadcrumb[] = array( 'title' => "Clientes", 'url' => url_module );
    }
    
    /**
     * @return View
     */
    public function novoAction( $clienteID = null )
    {
        $Cliente = new Cliente($clienteID);
        
        if( !$Cliente->getID() ){
            return 404;
        }
        
        $render['Cliente'] = $Cliente;
        
        $this->breadcrumb[] = array( 'title' => "Visualização de Cliente", 'url' => url_mode."/clientes/visualizacao/".$clienteID );
        $this->breadcrumb[] = array( 'title' => "Cadastro de Cliente" );
        $this->view()->setTemplate('cliente-usuario/cadastro.twig')->display($render);
    }
    
    /**
     * @return Json
     */
    public function criarAction( $mode = null )
    {
        $render['status'] = false;
        
        $Cliente = new Cliente($this->post('cli_id'));
        
        if( !$Cliente->getID() ){
            return 404;
        }
        
        if( $this->post('user_id') ){
            
            #Add user to client
            
            $User = new User( $this->post('user_id') );
            
            if( !$User->getID() || $User->getGroupID() !== group_cliente ){
                $this->error('O usuário informado é inválido')->json();
            }
            
            $CliUsuario = new ClienteUsuario();
            $CliUsuario->setClienteID($Cliente->getID());
            $CliUsuario->setUserID($User->getID());

            if( $CliUsuario->save() ){
                $render['status'] = true;
                $this->success('Usuário anexado ao cliente com sucesso');
            }
            
        }else{
            
            #Create user to client
            
            if( $this->post('user_senha') !== $this->post('user_rsenha') ){
                $this->error('As senhas não conferem')->json();
            }
            
            $User = new User();
            $User->setName( $this->post('user_name') );
            $User->setLogin( $this->post('user_login') );
            $User->setEmail( $this->post('user_email') );
            $User->setPassword( $this->post('user_senha') );
            $User->setStatus( $this->post('user_status') );
            $User->setGroupID( group_cliente );
            
            if( $User->save() ){
            
                $CliUsuario = new ClienteUsuario();
                $CliUsuario->setClienteID($Cliente->getID());
                $CliUsuario->setUserID($User->getID());

                if( $CliUsuario->save() ){
                    $render['status'] = true;
                    $this->success('Usuário criado com sucesso');
                }
                
            }
            
        }
        
        $render['redirect'] = url_mode."/clientes/visualizacao/".$Cliente->getID();
        
        $this->json($render, true);
    }
    
    /**
     * @return Json
     */
    public function searchUsuarioAction()
    {
        $render = array();
        
        $res = User::getList(array(
            'dao.name like ?' => "%".$this->get('term')."%",
            'dao.group_id = ?' => group_cliente
        ), array(
            'ID', 'name'
        ), 0, 10, array('dao.name'));
        
        foreach( $res['rows'] as $User ){
            $render[] = array(
                'id' => $User->getID(),
                'label' => $User->getName()
            );
        }
        $this->json($render);
    }
    
    /**
     * @return Json
     */
    public function excluirAction( $id = null )
    {
        $render['status'] = false;
        
        $ClienteUsuario = new ClienteUsuario($id);
        if( !$ClienteUsuario->getID() ){
            $this->error('Usuário inválido')->json();
        }
        
        if( $ClienteUsuario->save('D') ){
            $this->success('Usuário desanexado com sucesso');
            $render['status'] = true;
        }
        
        $this->json($render);
    }
    
}












































