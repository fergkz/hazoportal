<?php

use Pessoa\Cliente as Cliente;
use Pessoa\Pessoa as Pessoa;

use Lib\Format as Format;
use Lib\Validate as Validate;
use System\User as User;

class ClientesController extends \System\MyController
{
    public function __construct()
    {
        parent::__construct();
        
        $this->breadcrumb[] = array( 'title' => "Clientes", 'url' => url_module );
    }
    
    
    public function indexAction( $pesquisa = null )
    {
        $this->breadcrumb[] = array( 'title' => 'Pesquisa de diplomas' );
        $render = array();
        
        $pagina = $this->get('pagina') ? $this->get('pagina') : 1;
        
        if( ($this->get('text') !== $this->get('old-text') 
                || $this->get('limite') !== $this->get('old-limite'))
                && (String)$pagina != (String)'1'
                && $this->get('old-text') 
                && $this->get('old-limite')){
            $vals = array();
            foreach( $this->get() as $ind => $val ){
                $vals[] = urlencode($ind)."=".($ind == 'pagina' ? '1' : urlencode($val));
            }
            $this->redirect(url_module."?".implode("&", $vals));
        }
        
        $render['limite'] = $this->get('limite') ? $this->get('limite') : 10;
        $render['inicio'] = 0;
        if( $pagina ){
            $render['inicio'] = $pagina * $render['limite'] - $render['limite'];
        }
        $render['ordenacao'] = array( $this->get('ordenacao') ? $this->get('ordenacao') : 'dao.id' );
        $where = array(
            "dao.id is not null"
        );
        
        if( $this->get('text') ){
            $tmp = explode(" ", trim($this->get('text')));
            foreach( $tmp as $row ){
                $cond = "(pes.razao like ? or pes.fantasia like ? or pes.cpf like ? or dao.id = ?)";
                $where[$cond] = array("%{$row}%", "%{$row}%", "%{$row}%", "{$row}");
            }
        }
        
        $render['clientes'] = Cliente::getList($where, null, $render['inicio'], $render['limite'], $render['ordenacao']);
        
        if( $render['clientes']['cont_total'] > $render['limite'] ){
            $render['pag_qtde_paginas'] = ceil($render['clientes']['cont_total'] / $render['limite']? : 1);
        }else {
            $render['pag_qtde_paginas'] = 1;
        }
        
        $this->view()->setTemplate('clientes/lista.twig')->display($render);
    }
    
    /**
     * @return View
     */
    public function visualizacaoAction( $id = false )
    {
        $this->breadcrumb[] = array( 'title' => "Visualização de Cliente" );
        
        $Cliente = new Cliente($id);
        
        if( !$Cliente->getID() ){
            return 404;
        }
        
        $render['Cliente'] = $Cliente;
        $render['Pessoa']  = $Cliente->getPessoaObj();
        $this->view()->setTemplate('clientes/visualizacao.twig')->display($render);
    }
    
    /**
     * @return View
     */
    public function novoAction()
    {
        $this->breadcrumb[] = array( 'title' => "Cadastro de Cliente" );
        $this->view()->setTemplate('clientes/cadastro.twig')->display();
    }
    
    /**
     * @return Json
     */
    public function criarAction()
    {
        $render['status'] = false;
        
        $cpfCnpj = (String)Format::somenteNumeros( $this->post('pes_cpf_cnpj') );
        
        ## Begin:: Validações
        if( $cpfCnpj && !Validate::isCPF($cpfCnpj) && !Validate::isCNPJ($cpfCnpj) ){
            $this->error('CPF/CNPJ inválido')->json();
        }
        
        if( !$this->post('pes_razao') ){
            $this->error('A razão social deve ser informada')->json();
        }
        ## End:: Validações
        
        switch( strlen($cpfCnpj) ){
            case 11:
                $cpf = $cpfCnpj;
                $cnpj = null;
                break;
            case 14:
                $cnpj = $cpfCnpj;
                $cpf = null;
                break;
            default:
                $cnpj = null;
                $cpf = null;
                break;
        }
        
        $Cliente = new Cliente;
        $Pessoa = new Pessoa();
        
        $Pessoa->setCpf( $cpf );
        $Pessoa->setCnpj( $cnpj );
        $Pessoa->setRazao( $this->post('pes_razao') );
        $Pessoa->setFantasia( $this->post('pes_fantasia') );
        
        if( $Pessoa->save() ){
            
            $Cliente->setPessoaID( $Pessoa->getID() );
            $Cliente->setStatus( $this->post('cli_status') );
            $Cliente->setGerenteUserID( $this->post('cli_gerente') );
            
            if( $Cliente->save() ){
                $this->success('Cliente criado com sucesso!');
                $render['status'] = true;
                $render['redirect'] = url_module."/visualizacao/".$Cliente->getID();
                $this->json($render);
            }
        }
        
        $this->json($render, true);
    }
    
    /**
     * @return View
     */
    public function edicaoAction( $id = null )
    {
        $this->breadcrumb[] = array( 'title' => "Visualização de Cliente", 'url' => url_module."/visualizacao/".$id );
        $this->breadcrumb[] = array( 'title' => "Edição de Cliente" );
        
        $Cliente = new Cliente($id);
        
        if( !$Cliente->getID() ){
            return 404;
        }
        
        $render['Cliente'] = $Cliente;
        $render['Pessoa']  = $Cliente->getPessoaObj();
        $this->view()->setTemplate('clientes/cadastro.twig')->display($render);
    }
    
    /**
     * @return Json
     */
    public function alterarAction( $id = null )
    {
        $render['status'] = false;
        
        $Cliente = new Cliente($id);
        
        if( !$Cliente->getID() ){
            return 404;
        }
        
        $cpfCnpj = (String)Format::somenteNumeros( $this->post('pes_cpf_cnpj') );
        
        ## Begin:: Validações
        if( $cpfCnpj && !Validate::isCPF($cpfCnpj) && !Validate::isCNPJ($cpfCnpj) ){
            $this->error('CPF/CNPJ inválido')->json();
        }
        
        if( !$this->post('pes_razao') ){
            $this->error('A razão social deve ser informada')->json();
        }
        ## End:: Validações
        
        switch( strlen($cpfCnpj) ){
            case 11:
                $cpf = $cpfCnpj;
                $cnpj = null;
                break;
            case 14:
                $cnpj = $cpfCnpj;
                $cpf = null;
                break;
            default:
                $cnpj = null;
                $cpf = null;
                break;
        }
        
        
        $Pessoa = $Cliente->getPessoaObj();
        $Pessoa->setCpf( $cpf );
        $Pessoa->setCnpj( $cnpj );
        $Pessoa->setRazao( $this->post('pes_razao') );
        $Pessoa->setFantasia( $this->post('pes_fantasia') );
        
        if( $Pessoa->save() ){
            
            $Cliente->setStatus( $this->post('cli_status') );
            $Cliente->setGerenteUserID( $this->post('cli_gerente') );
            
            if( $Cliente->save() ){
                $this->success('Cliente alterado com sucesso!');
                $render['status'] = true;
                $render['redirect'] = url_module."/visualizacao/".$Cliente->getID();
                $this->json($render);
            }
        }
        
        $this->json($render, true);
    }
    
    /**
     * @return Json
     */
    public function searchGerenteAction()
    {   
        $render = array();
        $whereColumns = null;
        $rowStart = 0;
        $rowLimit = 11;
        $order = array('dao.name');
        
        if( $this->get('id') ){
            $whereColumns['dao.id = ?'] = $this->get('id');
        }else{
            $tmp = explode(" ", trim($this->get('term')));

            $where = $bind = array();
            
            if( $tmp && count($tmp) > 0 ){
                foreach( $tmp as $row ){
                    $where[] = "( dao.name like ? )";
                    $bind[] = "%{$row}%";
                }
                $whereColumns[implode(" and ", $where)] = $bind;
            }
        }
        
        $lista = User::getList($whereColumns, null, $rowStart, $rowLimit, $order);
        
        foreach( $lista['rows'] as $Obj ){
            $render[] = array(
                'id'    => $Obj->getID(),
                'label' => $Obj->getName()." - ".$Obj->getGroupObj()->getName()
            );
        }
        $this->json($render);
    }
}