<?php

use Core\System\Mail as Mail;

class PaginasController extends \System\MyController
{    
    public function sobreAction()
    {
        $render['selected'] = "sobre";
        $this->view()->setTemplate('paginas/sobre.twig')->display($render);
    }
    
    public function portfolioAction()
    {
        $render['selected'] = "portfolio";
        $this->view()->setTemplate('paginas/portfolio.twig')->display($render);
    }
    
    public function contatoAction( $action = null )
    {
        if( !$action ){
            $render['selected'] = "contato";
            $this->view()->setTemplate('paginas/contato.twig')->display($render);
        }elseif( $action == 'enviar' ){
            ini_set('display_errors', 0);
            
            $render['status'] = false;
            
            $Mail = new Mail();
            
            $Mail->setAddress('fergkz@gmail.com');
            
            $msg = "
            
                <p><b>Nome: </b>".$this->post('name')."</p>
                <p><b>E-mail: </b>".$this->post('email')."</p>
                <p><b>Website: </b>".$this->post('web')."</p>
                <p><b>Mensagem: </b></p>
                <p>".$this->post('comments')."</p>

            ";
            
            $Mail->setMessage($msg);
            $Mail->setSubject("Contato: hazo.com.br");
            
            if( $Mail->send() ){
                $render['status'] = true;
            }else{
                $render['msg'] = "Falha ao enviar mensagem. Tente novamente mais tarde.";
            }
            
            $this->json($render);
        }else{
            return 404;
        }
    }
}