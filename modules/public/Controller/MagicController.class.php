<?php

ini_set("display_errors", 0);

class MagicController extends MVC\Controller{
    
    public function locationAction(){
        
        $render = array();
        
        if( $_POST ){
            
            $texto = $_POST['cons'];
            
            $render['cards'] = Model\Singles\MagicCard::search($texto);
            
        }

        $this->view()->display($render);
        
    }
    
    public function bombAction(){
        $render = array();
        $this->view()->display($render);
    }

    public function cardInfoAction(){
//        $render['ind'] = Model\Singles\MagicCard::getMaxIndice();
        $render['ind'] = 4585;
        $this->view()->display($render);
    }
    
    public function cardInfoJsonAction( $card ){
        $names = $this->getCardsName($card);
        
        $Card = Model\Singles\MagicCard::getByIndice($card);
        
        if( $Card->imgContent and $Card->imgContent != '""'){
            die(json_encode(array("last"=>$card+1)) );
        }
        
        $cards['name']  = $names['name'];
        $cards['cost']  = $names['cost'];
        $cards['pow']   = $names['pow'];
        $cards['tgh']   = $names['tgh'];
        $cards['site']  = $this->getCardsSiteData( $cards['name'] );
        $cards['infos'] = $this->getCardsInfos( $cards['name'] );
        
//        $cards['site']['img_content'] = null;
        
        @$Card->indice           = $card;
        @$Card->name             = $cards['name'];
        @$Card->namePt           = ($cards['infos']['name_pt'] ?: $cards['site']['nome_portugues']) ?: $cards['name'];
        @$Card->cost             = $cards['cost'];
        @$Card->pow              = $cards['pow'];
        @$Card->tgh              = $cards['tgh'];
        @$Card->imgUrl           = $cards['site']['img_url'];
        @$Card->imgName          = $cards['site']['img_name'];
        @$Card->imgExt           = $cards['site']['img_ext'];
        @$Card->imgContent       = $cards['site']['img_content'];
//        @$Card->type             = $cards[''];
        @$Card->typePt           = $cards['infos']['type'] ?: $cards['site']['tipo'];
        @$Card->descr            = $cards[''];
        if( @$cards['infos']['rulelist'] ){
            @$Card->descrPt      = implode('<br/>', $cards['infos']['rulelist']['rule'])."<br/><i>".$cards['infos']['flavor']."</i>";
        }else{
            @$Card->descrPt      = $cards['site']['descricao'];
        }
        @$Card->serieImgUrl      = $cards['site']['serie_img'];
        @$Card->serieImgName     = @end(explode('/', $cards['site']['serie_img']));
        @$Card->serieImgExt      = @end(explode('.', $cards['site']['serie_img']));
        @$Card->serieImgContent  = @file_get_contents($cards['site']['serie_img']);
        @$Card->serieTitle       = $cards['site']['serie_title'];
        @$Card->serieTitlePt     = $cards['site']['serie_title'];
        @$Card->raridade         = $cards['site']['raridade'];
        @$Card->raridadePt       = $cards['site']['raridade'];
        @$Card->number           = $cards['site']['numero'];
        
        if( !$Card->save() ){
            debug(_getErrors());
        }
        
//        $json =  json_encode($cards);
//        $json = Core\System\JsonObject::indentJson($json);

        $imgUrl = url."/magic/view-image/".$Card->ID."?.".$Card->imgExt;
        $img = "<a href='{$imgUrl}'><img src='{$imgUrl}' style='height: 14px; margin: 5px;'/></a>";
//        echo "<a href='{$imgUrl}'><img src='{$imgUrl}' style='height: 100px; margin: 5px;'/></a>";
        $html = "<td class='t1'>{$card}</td><td class='t2'>{$Card->name}</td><td>$Card->namePt</td><td>$img</td>";
        
        $json = array(
            "content" => $html,
            "last" => $Card->indice+1
        );
        
        echo json_encode($json);
        
    }

    private function getCardsSiteData( $name ){
        $name = urlencode($name);
        $content = utf8_encode(file_get_contents("http://www.magicjebb.com.br/site/busca.php?nome={$name}"));

        $fullText = $this->getBetweenContent($content, '<a href="detalhes.php?', '"', 50);
        if( $fullText and $fullText[0] ){

            # ENCONTRA TABELA
            $link = "http://www.magicjebb.com.br/site/detalhes.php?".$fullText[0];
            $content = utf8_encode(file_get_contents($link));
            $fullText = $this->getBetweenContent($content, 'class="cardDetalhes">', '</table>', 50);
            $table = $fullText[0];

            foreach( $fullText as $table ){
                $tmp = $this->getBetweenContent($table, 'src="', '"', 50);
                $data['img_url'] = $tmp[0];
                
                if( $data['img_url'] ){
                    $data['img_name'] = @end(explode("/",$data['img_url']));
                    $data['img_ext'] = @end(explode(".",$data['img_name']));
                    $data['img_content'] = file_get_contents($data['img_url']);
                    
                }
                
                # OUTROS DADOS
                
                $content = $this->getBetweenContent($table, 'Nome em Inglês:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td width="250">', null, 50);
                $data['nome_ingles'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Nome em Português:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '">', null, 50);
                $data['nome_portugues'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Custo de Mana:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['custo_mana'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Tipo:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['tipo'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Texto:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['descricao'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'P/T:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['p_t'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Série:</td>', '</td>', 50);
                $serie = $this->getBetweenContent($content[0], '<td>', null, 50);
                $tmp = $this->getBetweenContent($serie, '<img src="', '"', 50);
                $data['serie_img'] = trim($tmp[0]);
                $tmp = $this->getBetweenContent($serie, 'title="', '"', 50);
                $data['serie_title'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Raridade:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['raridade'] = $tmp[0];

                $content = $this->getBetweenContent($table, 'Card:</td>', '</td>', 50);
                $tmp = $this->getBetweenContent($content[0], '<td>', null, 50);
                $data['numero'] = $tmp[0];
                
                return $data;
            }
        }
        return false;
    }
    
    private function getCardsName( $ind = null ){
        
        $path = path.ds.'modules'.ds.'public'.ds.'Controller'.ds.'cards'.ds.'mtg-data'.ds;
        
        $xml = simplexml_load_string(file_get_contents($path.'cards.xml'));
        
        $i = 0;
        foreach( $xml->card as $row ){
            $row = (Array)$row;
            $names[$i]['name'] = $row['name'];
            @$names[$i]['cost'] = $row['cost'];
            @$names[$i]['pow']  = $row['pow'];
            @$names[$i]['tgh']  = $row['tgh'];
            $i++;
        }
        
        if( $ind === null ){
            return $names;
        }else{
            return $names[$ind];
        }
        
    }
    
    private function getCardsInfos( $cardname ){
        
        $path = path.ds.'modules'.ds.'public'.ds.'Controller'.ds.'cards'.ds.'mtg-data'.ds;
        $xml = simplexml_load_string(file_get_contents($path.'language_portuguese_(brazil).xml'));
        
        foreach( $xml->card as $row ){
            $attr = (Array)$row;
            
            if( trim($attr['@attributes']['name']) == trim($cardname) ){
                $set = (Array)$attr['set'];
                
                $data['name'] = $cardname;
                $data['name_pt'] = $set['name'];
                $data['type'] = $set['type'];
                $data['rulelist'] = (Array)$set['rulelist'];
                $data['flavor'] = $set['flavor'];
                
                return $data;
            }
            
        }
        return false;
        
    }

    private function getBetweenContent( $content, $start, $end = null, $limit = null ){
        $in = false;
        $out = true;
        $text = "";
        $contents = array( );
        for( $i = 0; $i < strlen($content); $i++ ){
            if( $out and substr($content, $i, strlen($start)) == $start ){
                $i = $i + strlen($start) - 1;
                $in = true;
                $out = false;
                $text = "";
                continue;
            }
            if( $in and $end ){
                if( substr($content, $i, strlen($end)) != $end ){
                    $text .= substr($content, $i, 1);
                }else{
                    $out = true;
                    $in = false;
                    $contents[] = $text;
                    if( $limit and count($contents) >= $limit ){
                        break;
                    }
                }
            }elseif( $in ){
                $text .= substr($content, $i, 1);
            }
        }
        if( !$end ){
            $contents[] = $text;
        }
        return $contents;
    }

    public function viewImageAction( $id ){
        
        $Card = new Model\Singles\MagicCard();
        $Card->ID = $id;
        $Card->load();
        
//        debug($Card);
        
        if( !$Card ){
            return 404;
        }
        
//        header("Content-disposition: attachment; filename={$slug}.jpg");
        header('Content-type: image/'.$Card->imgExt);
//        header('Content-type: image/jpeg');
        die($Card->imgContent);
    }
    
}