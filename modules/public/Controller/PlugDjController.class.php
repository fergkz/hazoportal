<?php

//ini_set("display_errors", 0);
//
//class PlugDjController extends MVC\Controller{
//    
//    public function getFraseAction()
//    {
//        header("Access-Control-Allow-Origin: http://plug.dj");
        header('Access-Control-Allow-Origin: http://admin.example.com');  //I have also tried the * wildcard and get the same response
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
        header('content-type: application/json; charset=utf-8');
        header("access-control-allow-origin: *");
        
        $page = rand(1, 2);
        $content = file_get_contents("http://www.osvigaristas.com.br/frases/engracadas/pagina{$page}.html");
        
        $dom = new \DOMDocument();
        @$dom->loadHTML($content);
        $dom->preserveWhiteSpace = true;
        
        $nodes = $dom->getElementsByTagName("q");
        if( $nodes ){
            $arr['text'] = utf8_decode($nodes->item( rand(0, 19) )->nodeValue);
            die( $_GET['callback']."(".json_encode($arr).");" );
        }
//    }
//    
//}