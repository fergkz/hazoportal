<?php

class IndexController extends \System\MyController
{
    public function indexAction()
    {
        $render['selected'] = "index";
        $this->view()->setTemplate('index/index.twig')->display($render);
    }
}