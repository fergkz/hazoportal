<?php

use MVC\Controller as Controller;
use Model\Pessoa\Cliente as Cliente;
use Core\Model\User as User;

class PortalController extends Controller{

    public function indexAction(){
        if( User::online() ){
            if( User::online()->getGroupObj()->getDefaultUrl() ){
                $this->redirect(url.User::online()->getGroupObj()->getDefaultUrl());
            }else{
                $this->redirect(url."/".lower(User::online()->getGroupObj()->getMode()));
            }
        }else{
            $this->redirect(url."/session/login");
        }
    }

}