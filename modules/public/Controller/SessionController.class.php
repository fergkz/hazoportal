<?php

use System\User as User;

class SessionController extends System\MyController
{    
    public function loginAction()
    {
        $render['errors'] = $this->error();
        $this->view()->display($render);
    }
    
    public function entrarAction()
    {
        $render['status'] = false;
        
        if( $this->post('login') && $this->post('senha') ){
            
            $user = User::login($this->post('login'), $this->post('senha'));
            
            if( $user ){
                $render['status'] = true;
                $name = explode(" ", $user->getName());
                $render['name'] = $name[0];
                $render['link_pannel'] = url.'/'.strtolower($user->getGroupObj()->getMode());
            }else{
                $render['msg'] = "Usuário ou senha incorretos";
            }
            
        }else{
            $render['msg'] = "O login e a senha devem ser informados";
        }
        
        $this->json($render);
    }
    
    public function sairAction()
    {
        $render['status'] = true;
        User::logout();
        $this->json($render);
    }
}