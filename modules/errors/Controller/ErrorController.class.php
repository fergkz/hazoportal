<?php

use MVC\Controller as Controller;

class ErrorController extends Controller{
    
    public function error403Action(){
        $render['mensagem']    = "Você não tem permissão para acessar esta página";
        $render['codigo']      = "403";
        $render['status']      = "alert";
        $render['show_login']  = true;
        $render['show_inicio'] = true;
        $this->view()->setTemplate(_getModePath('ERROR', false)."/View/index.html")->display($render);
    }
    
    public function error404Action(){
        $render['mensagem']    = "A página que você solicitou não foi encontrada";
        $render['codigo']      = "404";
        $render['status']      = "alert";
        $render['show_login']  = false;
        $render['show_inicio'] = true;
        $this->view()->setTemplate(_getModePath('ERROR', false)."/View/index.html")->display($render);
    }
    
}