<?php

use Core\System\Functions as Functions;
use Core\Model\Menu as Menu;
use Core\Model\User as User;
use MVC\Session as Session;

function _dashboardMenuArray(){
    return Menu::getBranchByGroup(User::online()->getGroupID());
}

function _twigSetSessionTransaction($ind, $value = null){
    if( $value === null ){
        Session::del("transaction_".$ind);
    }else{
        Session::set("transaction_".$ind, $value);
    }
}

function _twigGetSessionTransaction( $ind ){
    return Session::get("transaction_".$ind);
}

function _formatTwigDebugTabs( $debugVars = null, $first = false ){
    if( !$debugVars || !is_array($debugVars) ){
        echo "NULL";
        return;
    }
    if( $first ){
        $debugVars['_GLOBALS'] = $GLOBALS;
        $debugVars['_SERVER'] = $_SERVER;
    }
    $rand = rand(11111,99999);
    ?>
    <div class="tabsdebgsup">
        <ul>
            <?php foreach( $debugVars as $ind => $var ){ ?>
                <?php if( strtoupper($ind) != "GLOBALS" ){ ?>
            <li><a href="#hztbs_<?php echo Functions::crypt($ind,$rand); ?>"><?php echo ($first ? "\$" : '').$ind; ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
            <?php foreach( $debugVars as $ind => $var ){ ?>
                <div id="hztbs_<?php echo Functions::crypt($ind,$rand); ?>"  style="overflow: auto;" >
                    <?php if( strtoupper($ind) != "GLOBALS" ){ ?>
                        <?php if( is_array($var) ){ _formatTwigDebugTabs($var); }else{ ?>

                                <pre><?php $var ? print_r($var) : "NULL"; ?></pre>

                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
    </div>
    <?php
}