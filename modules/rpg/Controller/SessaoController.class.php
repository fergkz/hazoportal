<?php

use RPG\Sessao as Sessao;
use RPG\Anexo as Anexo;
use RPG\SessaoPersonagem as SessaoPersonagem;
use RPG\Chat as Chat;

use Lib\Format as Format;
use Model\Pessoa\Usuario as Usuario;
use System\User as User;

class SessaoController extends Config
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function salaAction( $id )
    {
        $Sessao = new Sessao($id);
        
        if( !$Sessao || !$Sessao->getID() ){
            return 404;
        }
        
        $render['Sessao'] = $Sessao;
        $render['PersOnline'] = $this->loadPersonagem($id);
        
        $lista = Chat::getList(array(
            "per.sessao_id = ?" => $Sessao->getID()
        ), null, null, null, array('dao.id asc'));
        $render['chats'] = $lista['rows'];
        
        $lista = Anexo::getList(array(
            "dao.sessao_id = ?" => $Sessao->getID()
        ), null, null, null, array('dao.id asc'));
        $render['anexos_mestre'] = $lista['rows'];
        
        $lista = SessaoPersonagem::getList(array(
            "dao.sessao_id = ?" => $Sessao->getID(),
            "dao.user_id <> ?" => User::online()->getID()
        ), null, null, null, array('dao.id asc'));
        $render['outros_pers'] = $lista['rows'];
        
        $this->view()->setTemplate('sessao/sala.twig')->display($render);
    }
    
    /**
     * @return json
     */
    public function criarAction()
    {
        $render['status'] = false;

        $Sessao = new Sessao();
        $Sessao->setData( Format::dataToDb( trim($this->post('data')).' '.$this->post('hora').':00' ) );
        $Sessao->setTitulo( $this->post('titulo') );
        $Sessao->setResumo( $this->post('resumo') );
        $Sessao->setUserCadastroID( User::online()->getID() );

        if( $Sessao->save() ){
            $render = array(
                'status' => true,
                'id'     => $Sessao->getID()
            );
        }

        $this->json($render, 1);
    }
    
    /**
     * @return view
     */
    public function cadastroAction( $id = null )
    {
        $Sessao = new Sessao($id);
        if( !$Sessao || !$Sessao->getID() ){
            return 404;
        }
        $render['Sessao'] = $Sessao;
        
        $PersMestre = SessaoPersonagem::getList(array(
            'dao.sessao_id = ?' => $id,
            "dao.tipo = 'M'"
        ), null, 0, 1);
        if( $PersMestre['cont_total'] > 0 ){
            $render['PersMestre'] = $PersMestre['rows'][0];
        }
        
        $render['personagens'][0] = new SessaoPersonagem();
        $render['personagens'][0]->setNome("Novo Personagem");
        
        $personagens = SessaoPersonagem::getList(array(
            'dao.sessao_id = ?' => $id,
            "dao.tipo = 'P'"
        ));
        
        if( $personagens['cont_total'] > 0 ){
            $render['personagens'] = array_merge($render['personagens'], $personagens['rows']);
        }
        
        $this->view()->setTemplate('sessao_p/cadastro.twig')->display($render);
    }
    
    /**
     * @return json/search
     */
    public function searchUsuarioAction()
    {
        echo Usuario::search();
    }
    
    /**
     * @return json
     */
    public function cadastrarMestreAction()
    {
        $render['status'] = false;
        
        $Personagem = new SessaoPersonagem( $this->post('id') );
        
        if( !$Personagem->getAnexoImagemID() && @empty($_FILES['arquivo']['name']) ){
            $this->error('Arquivo inválido')->json();
        }elseif( !@empty($_FILES['arquivo']['name']) ){
            $Anexo = new Anexo();
            $Anexo->setContent( file_get_contents($_FILES['arquivo']['tmp_name']) );
            $Anexo->setName( $_FILES['arquivo']['name'] );
            $Anexo->setType( $_FILES['arquivo']['type'] );
            $Anexo->setRef( 'I' );
            
            if( $Anexo->save() ){
                
                $Personagem->setSessaoID( $this->post('sessao') );
                $Personagem->setUserID( $this->post('usuario') );
                $Personagem->setNome( $this->post('nome') );
                $Personagem->setAnexoImagemID( $Anexo->getID() );
                $Personagem->setTipo( 'M' );
                
                if( $Personagem->save() ){
                    $render['status'] = true;
                    $this->success("Dados atualizados com sucesso");
                }
                
            }
            
        }else{
            $Personagem->setUserID( $this->post('usuario') );
            $Personagem->setNome( $this->post('nome') );

            if( $Personagem->save() ){
                $render['status'] = true;
                $this->success("Dados atualizados com sucesso");
            } 
        }
        
        $this->json($render, 1);
    }
    
    /**
     * @return json
     */
    public function cadastrarPersonagemAction()
    {
        $render['status'] = false;
        
        $Personagem = new SessaoPersonagem( $this->post('id') );
        
        if( !$Personagem->getAnexoImagemID() && @empty($_FILES['arquivo']['name']) ){
            $this->error('Arquivo inválido')->json();
        }elseif( !@empty($_FILES['arquivo']['name']) ){
            $Anexo = new Anexo();
            $Anexo->setContent( file_get_contents($_FILES['arquivo']['tmp_name']) );
            $Anexo->setName( $_FILES['arquivo']['name'] );
            $Anexo->setType( $_FILES['arquivo']['type'] );
            $Anexo->setRef( 'I' );
            
            if( $Anexo->save() ){
                
                $Personagem->setSessaoID( $this->post('sessao') );
                $Personagem->setUserID( $this->post('usuario') );
                $Personagem->setNome( $this->post('nome') );
                $Personagem->setAnexoImagemID( $Anexo->getID() );
                $Personagem->setTipo( 'P' );
                
                if( $Personagem->save() ){
                    $render['status'] = true;
                    $this->success("Dados atualizados com sucesso");
                }
                
            }
            
        }else{
            $Personagem->setUserID( $this->post('usuario') );
            $Personagem->setNome( $this->post('nome') );

            if( $Personagem->save() ){
                $render['status'] = true;
                $this->success("Dados atualizados com sucesso");
            } 
        }
        
        $this->json($render, 1);
    }
    
    
    /**
     * @return view
     */
    public function anexoAction( $id )
    {
        $Anexo = new Anexo($id);
        
        if( !$Anexo || !$Anexo->getID() ){
            return 404;
        }
        
        header("content-type: ".$Anexo->getType());
        echo $Anexo->getContent();
        exit;
    }
    
    /**
     * @return json
     */
    public function showRandAction( $id, $dice )
    {
        $PersOnline = $this->loadPersonagem($id);
        
        $render['number'] = rand( 1, $dice );
        
        $Sessao = new Sessao($id);
        $Sessao->setChat($PersOnline->getID(), "Rolando d{$dice}: tirou <b>{$render['number']}</b>", "D");
        
        $this->json($render, 1);
    }
    
    /**
     * @return json
     */
    public function mestreAddContentAction( $sessaoID = null, $type = "file", $ref = null )
    {
        $render['false'] = true;
        
        $Sessao = new Sessao($sessaoID);
        if( !$Sessao || !$Sessao->getID() ){
            $this->error('Sessão inválida')->json();
        }
        
        if( $ref == 'I' && @empty($_FILES['arquivo']['name']) ){
            $this->error('Arquivo inválido')->json();
        }else{
            $Anexo = new Anexo();
            $Anexo->setContent( $ref == 'I' ? file_get_contents($_FILES['arquivo']['tmp_name']) : $this->post('content') );
            $Anexo->setName( $ref == 'I' ? $_FILES['arquivo']['name'] : "Conteúdo" );
            $Anexo->setType( $type === 'file' ? $_FILES['arquivo']['type'] : $type );
            $Anexo->setRef( $ref );
            
            $Anexo->setSessaoID( $sessaoID );
            
            if( $Anexo->save() ){
                $render['status'] = true;
                $this->success("Dados atualizados com sucesso");
            }
        }
        
        $this->json($render, 1);
    }
   
    /**
     * @return json
     */
    public function getNextMestreAnexoAction( $sessaoID = null, $id = 0 )
    {
        $render['status'] = false;
        
        $Sessao = new Sessao($sessaoID);
        
        if( !$Sessao || !$Sessao->getID() ){
            $this->error('Sessão inválida')->json();
        }
        
        $lista = Anexo::getList(array(
            "dao.id > ?" => urldecode($id),
            "dao.sessao_id = ?" => $sessaoID
        ), null, 0, 1, array('dao.id asc'));
        
        if( $lista['cont_total'] > 0 ){
            
            $Anexo = $lista['rows'][0];
            
            $render = array(
                'status'  => true,
                'id'      => $Anexo->getID(),
                'content' => $Anexo->getContent(),
                'name'    => $Anexo->getName(),
                'type'    => $Anexo->getType(),
                'url'     => url_module."/anexo/".$Anexo->getID()."?.png",
                'ref'     => $Anexo->getRef()
            );

        }
        
        $this->json($render);
    }
    
}