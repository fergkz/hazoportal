function lgLoad(status, msg){
    if( status ){
        $('.btnsub').val("Aguarde...");
    }else{
        $('.error').text(msg).show();
        $('.btnsub').val("Entrar");
    }
}

$().ready(function(){
    
    $(".form-horizontal").ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            lgLoad(true);
        },
        success: function( data ){
    
            if( data.status ){
                window.location = hazoParam.url+"/rpg";
            }else{
                lgLoad(false, data.msg);
            }
    
        }
    });
    
    $(".logoff").bind('click', function(e){
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: hazoParam.url+"/session/sair",
            success: function(){
                usuLog(false);
            }
        });
    });
    
});