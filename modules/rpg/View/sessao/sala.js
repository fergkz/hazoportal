function resizeCalc(){
    
//    $('.char-chat').height('500px');
    nhei = $(window).height() - $('.tools').height() - 2/*border tools*/ - 10/*chat padding*/ + 10/*padding receive*/;
    
    $('.char-chat').height( nhei );
    $('.master-role').height( nhei );
    
    inside = ($('.master-role .inside').height() ? 100 : 0);
    $('.master-role .body').height( $('.master-role').height() - $('.master-role .title').height() - inside);
    
//    $('.char-chat .call textarea').width( $('.char-chat').width() - 2/*this.border*/ - 4/*this.margin*/ - 8/*this.padding*/ );
//    $('.char-chat .call textarea').width( $('.char-chat').width() - 2/*this.border*/ - 4/*this.margin*/ );
    $('.char-chat .call textarea').width( $('.char-chat').width() - 2/*this.border*/ - 8/*this.margin*/ );
//    $('.char-chat .call textarea').hide();
    $('.char-chat .call').height( 38 );
    
    $('.receive').height( nhei - 54/*call.height*/ );
    
    $('.rol').width( $(window).width() - $('.master-role').width() - $('.char-chat').width() - 4.5 );
    
}

function rol(val, cont){
    var numLow = 1;
    var numHigh = val;

    var adjustedHigh = (parseFloat(numHigh) - parseFloat(numLow)) + 1;

    var numRand = Math.floor(Math.random()*adjustedHigh) + parseFloat(numLow);
    
    if( cont === '' || cont === undefined || cont === 0 ){
        rol(val, 1);
    }else{
        
        if( cont < 30 ){
            $('.field').text(numRand).css('color', '#AAA');
            
            if( cont < 10 ){
                rolTime = 10;
            }else if( cont < 5 ){
                rolTime = 20;
            }else if( cont < 10 ){
                rolTime = 30;
            }else if( cont < 15 ){
                rolTime = 40;
            }else if( cont < 20 ){
                rolTime = 50;
            }else if( cont < 25 ){
                rolTime = 60;
            }else if( cont < 30 ){
                rolTime = 70;
            }
            
            
            setTimeout(function(){
                rol(val, cont+1);
            }, rolTime);
        }else{
            load = '<div style="line-height:10px;font-size:10px;margin: 95px 0;">Carregando valor...</div>';
            $('.field').html(load).css('color', '#AAA');
            
            $.ajax({
                dataType: 'json',
                url: hazoParam.url+"/rpg/sessao/show-rand/"+hazoParam.sessao_id+"/"+val,
                success: function( data ){
                    $('.field').text(data.number).css('color', '#555');
                }
            });
        }
    }
    
}

function getNextChat( settime ){
    
    var ultimo = $('ul.chat li:last-child').attr('data-id');
    
    if( ultimo === '' || ultimo === null || ultimo === undefined ){
        ultimo = "0";
    }
    
    $.ajax({
        dataType: 'json',
        url: hazoParam.url+"/rpg/chat/get-next/"+hazoParam.sessao_id+"/"+ultimo,
        async: true,
        success: function( data ){
            if( data.status ){
            
                li = "<li data-id='"+data.id+"' id='chat_"+data.id+"'>";
                li += "<img class='profile' src='"+hazoParam.url+"/rpg/sessao/anexo/"+data.autor_imagem_id+"'/>";
                //li += "<div class='name'>"+data.autor_nome+"</div>";
                li += "<div class='msg'>"+data.message+"</div>";
                li += "</li>";

                $('ul.chat').append(li);
                
                $(".char-chat").find('.receive').animate({ scrollTop: $(".char-chat .receive").get(0).scrollHeight }, 300);
            }
                
            if( settime === true ){
                setTimeout(function(){
                    getNextChat( settime )
                }, 300);
            }
        }
    });
    
}

function sendMessage(){
    var message = $('#send-chat').val();
    
//    $('#send-chat').attr('readonly', true);
    
    $.ajax({
        dataType: 'json',
        type: 'post',
        url: hazoParam.url+"/rpg/chat/send/"+hazoParam.sessao_id,
        data: {message: message},
        async: true,
        success: function( data ){
            
            if( data.status ){
//                $('#send-chat').attr('readonly', false);
                $('#send-chat').val("");
//                getNextChat();
            }
            
        }
    });
    
}

function getNextAnexo(){
    
    var ultimo = $('ul.master-anx li:last-child').attr('data-id');
    
    if( ultimo === '' || ultimo === null || ultimo === undefined ){
        ultimo = "0";
    }
    
    $.ajax({
        dataType: 'json',
        url: hazoParam.url+"/rpg/sessao/get-next-mestre-anexo/"+hazoParam.sessao_id+"/"+ultimo,
        async: true,
        success: function( data ){
            if( data.status ){
            
                li = "<li data-id='"+data.id+"'>";
                
                if( data.ref == 'I' ){
                    li += "<a href='"+data.url+"' rel='gallery1' class='fancybox'><img src='"+data.url+"' class='anx'/></a>";
                }else if( data.ref == 'T' ){
                    li += "<div class='text'>"+data.content+"</div>";
                }
                
                li += "</li>";
                $('img.anx').load(function(){
                    $(".master-role").find('.body').animate({ scrollTop: $(".master-role .body").get(0).scrollHeight+15000 }, 300);
                });

                $('ul.master-anx').append(li);
                $(".fancybox").fancybox();
                
                $(".master-role").find('.body').animate({ scrollTop: $(".master-role .body").get(0).scrollHeight+15000 }, 300);
                
            }
        }
    });
    
}

function fancyTxt(){
    
//    $.fancybox('#m_add_txt', function(){
//        alert('text');
//    });
    
    
    $.fancybox({
        href : '#m_add_txt',
        afterLoad: function(){
            textarea = $('#txt-n-ck').clone().attr('id', "tmp_txt-n-ck");
            
            
            $("#txt-n-ck").remove();
            
            $(textarea).attr('id', "txt-n-ck").ckeditor();
            console.log(textarea);
            
            $('#m_add_txt').find('.textarea').html(textarea);
            
        }
    });
    
//    $("#stuff").fancybox({
//            // [Your settings]
//        onComplete : function() {
//                    // Register your CKeditor here
//                    if (CKEDITOR.instances['user_message']) {                   
//                        CKEDITOR.instances.user_message.destroy();
//                     }
//                     CKEDITOR.replace( 'user_message',{
//                                toolbar : 'Basic',
//                    });
//        }
//    });
}

$().ready(function(){
    
    $(".fancybox").fancybox();
    
    $('.fmrm-mst').ajaxForm({
        dataType: 'json',
        success: function(data) {
            if( data.status ){
                $.fancybox.close();
            }else{
                alert(data.msg);
            }
        }	
    });
    
    resizeCalc();
    
    setTimeout(function(){
        resizeCalc();
    }, 100);
    
//    getNextChat();
//    setInterval(function(){
        getNextChat(true);
//    }, 1000);
    
    setInterval(function(){
        getNextAnexo();
    }, 3000);
    
//    setInterval(function(){
        $(".master-role").find('.body').animate({ scrollTop: $(".master-role .body").get(0).scrollHeight+5000 }, 250);
//    }, 400);
    
    $(".char-chat").find('.receive').animate({ scrollTop: $(".char-chat .receive").get(0).scrollHeight }, 200);
    
    /** send message **/
    
    $("#send-chat").keypress(function(event) {
        if( event.which === 13 ){
            event.preventDefault();
            sendMessage();
        }
    });
    
});

$(window).resize(function() {
    resizeCalc();
});