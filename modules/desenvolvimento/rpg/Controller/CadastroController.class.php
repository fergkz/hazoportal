<?php

use RPG\Free\Sessao as Sessao;
use RPG\Free\Personagem as Personagem;

class CadastroController extends Config
{   
    /**
     * @return view
     */
    public function indexAction()
    {
        $this->view('cadastro/criar-sala.twig')->display();
    }
    
    /**
     * @return json
     */
    public function criarAction()
    {
        $render['status'] = false;
        
        $Sessao = new Sessao();
        
        $Sessao->setTitulo( $this->post('sala') );
        $Sessao->setResumo( $this->post('resumo') );
        
        if( $Sessao->save() ){
            
            $Personagem = new Personagem();
            $Personagem->setUsuarioNome( $this->post('nome') );
            $Personagem->setTipo('M');
            $Personagem->setStatus('P');
            $Personagem->setSessaoToken( $Sessao->getToken() );
            $Personagem->setEmail( $this->post('mail') );

            if( $Personagem->save() ){
                $render['status'] = true;
            }   
        }
        
        $this->json($render, true);
    }
    
    /**
     * Tela de exibição de sessão cadastrada com sucesso
     * @return View
     */
    public function cadastroEfetuadoComSucessoAction()
    {
        $this->view("cadastro/cadastro-efetuado-com-sucesso.twig")->display();
    }
    
    /**
     * Acesso ao endereço da sala da sessão de RPG
     * /desenvolvimento/rpg/sessao/sala/TOKEN_SESSAO/TOKEN_PERSONAGEM
     * @return View
     */
    public function acessoEmailAction( $sessaoToken, $personagemToken )
    {
        $Personagem = new Personagem($personagemToken);
        
        if( $Personagem->getSessaoToken() !== $sessaoToken ){
            return 404;
        }
        $Personagem->setOnline();
        
        $this->redirect(url_mode."/sessao/sala");
    }
    
}