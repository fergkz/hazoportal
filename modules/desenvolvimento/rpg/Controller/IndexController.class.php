<?php

use RPG\Sessao as Sessao;
use System\User as User;

class IndexController extends Config
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function indexAction()
    {
        $this->redirect(url_mode."/sessao/sala");
    }
    
}