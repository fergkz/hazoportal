<?php

use RPG\Free\Personagem as Personagem;
use RPG\Free\Anexo as Anexo;

use Lib\WideImage\Image as Image;

class PersonagemController extends Config
{
    /**
     * @return View
     */
    public function complementoCadastroAction()
    {   
        $render['Personagem'] = $this->Personagem;
        $this->view('personagem/cadastro.twig')->display($render);
    }
    
    /**
     * @return Json
     */
    public function atualizaImagemAction()
    {   
        $Personagem =& $this->Personagem;
        
        $render['status'] = false;
        
        if( $Personagem->getAnexoImagemObj() ){
            $Anexo = $Personagem->getAnexoImagemObj();
        }else{
            $Anexo = new Anexo();
        }

        $Image = Image::loadFromUpload('arquivo');
//        $Image->resize($width, $height);
        
        if( $Image->getWidth() > $Image->getHeight() ){
            $Image = $Image->resize(null, 600);
        }elseif( $Image->getWidth() <= $Image->getHeight() ){
            $Image = $Image->resize(600);
        }
//        $Image->autoCrop();
        
        
        $Anexo->setContent( $Image->asString("png") );
        $Anexo->setName( $_FILES['arquivo']['name'] );
        $Anexo->setType( $_FILES['arquivo']['type'] );
        $Anexo->setRef("I");
        $Anexo->setExtensao("png");
        $Anexo->setPersonagemToken( $Personagem->getToken() );
        $Anexo->setPrincipal("S");
        
        if( $Anexo->save() ){
            
            $Personagem->setAnexoImagemID( $Anexo->getID() );
            
            if( $Personagem->save() ){
                $Personagem->setOnline();
                
                $render['status'] = true;
            }
            
        }

        $this->json($render, true);
    }
    
    /**
     * @return View
     */
    public function imagemPersonagemAction( $token = null, $size = 600 )
    {
        $Personagem = new Personagem($token);
        
        if( !$Personagem->getToken() ){
            return 404;
        }
        
        $Image = Image::load( $Personagem->getAnexoImagemObj()->getContent() );
        $Image->resize($size)->output('png');
        
        exit;
    }
    
    /**
     * @return Json
     */
    public function atualizarDadosAction()
    {   
        $Personagem =& $this->Personagem;
        
        $render['status'] = false;
        
        $Personagem->setPersonagemNome( $this->post('nome_pers') );
        $Personagem->setUsuarioNome( $this->post('nome') );
        $Personagem->setStatus( "A" );
        
        if( $Personagem->save() ){
            $Personagem->setOnline();
            $render['status'] = true;
        }
        

        $this->json($render, true);
    }
    
}