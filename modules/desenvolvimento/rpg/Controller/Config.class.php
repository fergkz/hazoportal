<?php

//use Core\Model\User as User;

//use RPG\SessaoPersonagem as SessaoPersonagem;
use RPG\Free\Personagem as Personagem;
//use MVC\Session as Session;

class Config extends \System\MyController
{
    
    public function __construct($requireOnline = true)
    {
        parent::__construct();
        
        $this->Personagem = Personagem::getOnline();
        
        if( $requireOnline && !$this->Personagem && $GLOBALS['request']['module'] !== 'cadastro' && $GLOBALS['request']['module'] !== 'sessao' && $GLOBALS['request']['submodule'] !== 'sala' ){
            $this->redirect($GLOBALS['url_rpg_mode']."/cadastro");
        }
        
        if( $this->Personagem && $this->Personagem->getStatus() !== 'A' && $GLOBALS['request']['module'] !== 'personagem' ){
            $this->redirect(url_mode."/personagem/complemento-cadastro");
        }
    }
    
}