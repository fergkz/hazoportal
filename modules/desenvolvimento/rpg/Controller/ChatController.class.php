<?php

Use RPG\Free\Sessao as Sessao;
//use RPG\Free\Anexo as Anexo;
//use RPG\Free\Personagem as Personagem;
use RPG\Free\Chat as Chat;

//use Lib\Format as Format;
//use Model\Pessoa\Usuario as Usuario;
//use System\User as User;

class ChatController extends Config
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function salaAction( $token )
    {
        $Sessao = new Sessao($token);
        
        if( !$Sessao || !$Sessao->getToken() ){
            return 404;
        }
        
        $render['Sessao'] = $Sessao;
        $PersOnline = $this->loadPersonagem($id);
        
        $this->view()->setTemplate('sessao/sala.twig')->display($render);
    }
    
    /**
     * @return json
     */
    public function getNextAction( $sessaoToken = null, $oldChatID = 0 )
    {
        $render['status'] = false;
        
        $Sessao = new Sessao( $this->Personagem->getSessaoToken() );
        
        if( !$Sessao || !$Sessao->getToken() ){
            $this->error('Sessão inválida')->json();
        }
        
        $lista = Chat::getList(array(
            "dao.id > ?" => urldecode($oldChatID),
            "per.sessao_token = ?" => $Sessao->getToken()
        ), null, 0, 1, array('dao.id asc'));
        
        if( $lista['cont_total'] > 0 ){
            
            $Chat = $lista['rows'][0];
            
            $render = array(
                'status'           => true,
                'id'               => $Chat->getID(),
                'tipo'             => $Chat->getTipo(),
                'message'          => $Chat->getMessage(),
                'autor_nome'       => $Chat->getPersonagemObj()->getPersonagemNome(),
//                'autor_imagem_id' => $Chat->getPersonagemObj()->getAnexoImagemID(),
                'autor_imagem_url' => $Chat->getPersonagemObj()->getAnexoImagemObj()->getImagemUrl('crop_50').'?'.time().'.'.$Chat->getPersonagemObj()->getAnexoImagemObj()->getExtensao(),
                'data'             => $Chat->getData()
            );

        }
        
        $this->json($render);
    }
    
    /**
     * @return json
     */
    public function sendAction()
    {
        $render['status'] = false;
        
        $Sessao = new Sessao( $this->Personagem->getSessaoToken() );
        
        if( !$Sessao || !$Sessao->getToken() ){
            $this->error('Sessão inválida')->json();
        }
        
        $mensagem = trim(str_replace(array("<",">","&"), array("&lt;","&gt;","&#38;"),$this->post('message')));
        
        if( $Sessao->setChat($this->Personagem->getToken(), $mensagem, "M") ){
            $render['status'] = true;
        }
        
        $this->json($render);
    }
    
}