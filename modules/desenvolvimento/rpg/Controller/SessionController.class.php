<?php

class SessionController extends Config
{
    
    public function __construct()
    {
        parent::__construct(false);
    }
    
    /**
     * @return view
     */
    public function loginAction()
    {
        $this->view()->setTemplate('session/login.twig')->display();
    }
    
}