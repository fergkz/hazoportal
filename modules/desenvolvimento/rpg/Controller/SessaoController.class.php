<?php
use RPG\Free\Sessao as Sessao;
use RPG\Free\Anexo as Anexo;
use RPG\Free\Personagem as Personagem;
use RPG\Free\Chat as Chat;

class SessaoController extends Config
{
    /**
     * @return View
     */
    public function salaAction()
    {
        $Sessao = new Sessao( $this->Personagem->getSessaoToken() );
        
        if( !$Sessao || !$Sessao->getToken() ){
            return 404;
        }
        
        $render['Sessao'] = $Sessao;
        $render['Personagem'] = $this->Personagem;
        
        $lista = Chat::getList(array(
            "per.sessao_token = ?" => $Sessao->getToken()
        ), null, null, null, array('dao.id asc'));
        $render['chats'] = $lista['rows'];
        
        $lista = Anexo::getList(array(
            "dao.sessao_token = ?" => $Sessao->getToken()
        ), null, null, null, array('dao.id asc'));
        $render['anexos_mestre'] = $lista['rows'];
        
        $lista = Personagem::getList(array(
            "dao.sessao_token = ?" => $Sessao->getToken(),
            "dao.token <> ?" => $this->Personagem->getToken()
        ), null, null, null, array('dao.id asc'));
        $render['outros_pers'] = $lista['rows'];
        
        $render['time'] = time();
        
        $this->view()->setTemplate('sessao/sala.twig')->display($render);
    }
    
    /**
     * @return Json
     */
    public function showRandAction( $sessaoToken, $dice )
    {
        
        $render['number'] = rand( 1, $dice );
        
        $Sessao = new Sessao( $this->Personagem->getSessaoToken() );
        
        $html = "<span class='chat-rol-dice'>d{$dice} = <b>{$render['number']}</b></span>";
        
        $Sessao->setChat($this->Personagem->getToken(), $html, "D");
        
        $this->json($render, 1);
    }
    
    /**
     * @return Json
     */
    public function mestreAddContentAction( $type = "file", $ref = null )
    {
        $render['status'] = true;
        
        $Personagem =& $this->Personagem;
        
        if( $Personagem->getTipo() !== "M" ){
            return 404;
        }
        
        $Sessao = $Personagem->getSessaoObj();
        
        if( $ref == 'I' && @empty($_FILES['arquivo']['name']) ){
            $this->error('Arquivo inválido')->json();
        }else{
            
            $Anexo = new Anexo();
            $Anexo->setContent( $ref == 'I' ? file_get_contents($_FILES['arquivo']['tmp_name']) : $this->post('content') );
            $Anexo->setName( $ref == 'I' ? $_FILES['arquivo']['name'] : "Conteúdo" );
            $Anexo->setType( $type === 'file' ? $_FILES['arquivo']['type'] : $type );
            $Anexo->setRef( $ref );
            $Anexo->setSessaoToken( $Sessao->getToken() );
            $Anexo->setPrincipal("N");
            $Anexo->setExtensao("png");
            
            if( $Anexo->save() ){
                $render['status'] = true;
                $this->success("Dados atualizados com sucesso");
            }
        }
        
        $this->json($render, 1);
    }
   
    /**
     * @return Json
     */
    public function getNextMestreAnexoAction( $id = 0 )
    {
        $render['status'] = false;
        
        $Personagem =& $this->Personagem;
        
        $Sessao = $Personagem->getSessaoObj();
        
        $lista = Anexo::getList(array(
            "dao.id > ?" => urldecode($id),
            "dao.sessao_token = ?" => $Sessao->getToken()
        ), null, 0, 1, array('dao.id asc'));
        
        if( $lista['cont_total'] > 0 ){
            
            $Anexo = $lista['rows'][0];
            
            $render = array(
                'status'    => true,
                'id'        => $Anexo->getID(),
                'content'   => $Anexo->getContent(),
                'name'      => $Anexo->getName(),
                'type'      => $Anexo->getType(),
                'url_small' => $Anexo->getImagemUrl(450),
                'url_big'   => $Anexo->getImagemUrl(600),
                'ref'       => $Anexo->getRef()
            );

        }
        
        $this->json($render);
    }
    
    /**
     * @return Json
     */
    public function convidarParticipanteAction()
    {
        $render['status'] = false;
        
        $Sessao =& $this->Personagem->getSessaoObj();
        
        $Personagem = new Personagem();
        
        $Personagem->setTipo('P');
        $Personagem->setStatus('P');
        $Personagem->setSessaoToken( $Sessao->getToken() );
        $Personagem->setEmail( $this->post('mail') );

        if( $Personagem->save() ){
            $render['status'] = true;
        }
        
        $this->json($render, true);
    }
    
    /**
     * @return Json
     */
    public function listaParticipantesAction()
    {
        $Personagem =& $this->Personagem;
        
        $lista = $Personagem->getSessaoObj()->listaPersonagens('A');
        
        $render['cont_total'] = $lista['cont_total'];
        
        if( $render['cont_total'] > 0 ){
            
            foreach( $lista['rows'] as $Personagem ){
                $data = new stdClass();

                $data->token          = $Personagem->getToken();
//                $data->imagem450      = $Personagem->getAnexoImagemObj()->getImagemUrl(450).'?'.time().'.'.$Personagem->getAnexoImagemObj()->getExtensao();
//                $data->imagem300      = $Personagem->getAnexoImagemObj()->getImagemUrl(300).'?'.time().'.'.$Personagem->getAnexoImagemObj()->getExtensao();
//                $data->imagem150      = $Personagem->getAnexoImagemObj()->getImagemUrl('crop_150').'?'.time().'.'.$Personagem->getAnexoImagemObj()->getExtensao();
                $data->imgExtensao    = $Personagem->getAnexoImagemObj()->getExtensao();
                $data->imagem450      = $Personagem->getAnexoImagemObj()->getImagemUrl(450);
                $data->imagem300      = $Personagem->getAnexoImagemObj()->getImagemUrl(300);
                $data->imagem150      = $Personagem->getAnexoImagemObj()->getImagemUrl('crop_150');
                $data->personagemNome = $Personagem->getPersonagemNome();
                $data->usuarioNome    = $Personagem->getUsuarioNome();
                $data->tipo           = $Personagem->getTipo();
                $data->contChange     = $Personagem->getAnexoImagemObj()->getContChange();
                $data->email          = $Personagem->getEmail();
                
                $render['personagens'][] = $data;
            }
            
        }
        
        $this->json($render, true);
        
    }
    
}