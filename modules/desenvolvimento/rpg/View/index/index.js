function resizeCalc(){
    
//    $('.char-chat').height('500px');
    nhei = $(window).height() - $('.tools').height() - 2/*border tools*/ - 10/*chat padding*/ + 10/*padding receive*/;
    
    $('.ex').height( nhei );
    
}

$().ready(function(){
    
    resizeCalc();
    
    setTimeout(function(){
        resizeCalc();
    }, 100);
    
    var formatDatepicker = {
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    };
    
    $('.data').datepicker(formatDatepicker).mask('99/99/9999');
    $('.hora').mask('99:99');
    
    $(".criar").ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            $('.submit').val('Criando, aguarde..');
        },
        error: function(){
            $('.submit').val('Criar');
            $('.error').text('Erro ao criar sessão').show();
        },
        success: function( data ){
            if( data.status ){
                window.location = hazoParam.url+"/rpg/sessao/cadastro/"+data.id;
            }else{
                $('.submit').val('Criar');
                $('.error').text(data.msg).show();
            }
        }
    });
    
});

$(window).resize(function() {
    resizeCalc();
});