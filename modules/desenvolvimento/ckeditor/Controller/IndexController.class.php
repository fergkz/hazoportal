<?php

class IndexController extends System\MyController
{
    /**
     * @return view
     */
    public function indexAction()
    {        
        $this->view("index/index.twig")->display();
    }
    
}