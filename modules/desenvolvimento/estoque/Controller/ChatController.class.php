<?php

use RPG\Sessao as Sessao;
use RPG\Anexo as Anexo;
use RPG\SessaoPersonagem as SessaoPersonagem;
use RPG\Chat as Chat;

use Lib\Format as Format;
use Model\Pessoa\Usuario as Usuario;
use System\User as User;

class ChatController extends Config
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function salaAction( $id )
    {
        $Sessao = new Sessao($id);
        
        if( !$Sessao || !$Sessao->getID() ){
            return 404;
        }
        
        $render['Sessao'] = $Sessao;
        $PersOnline = $this->loadPersonagem($id);
        
        $this->view()->setTemplate('sessao/sala.twig')->display($render);
    }
    
    /**
     * @return json
     */
    public function getNextAction( $sessaoID = null, $oldChatID = 0 )
    {
        $render['status'] = false;
        
        $Sessao = new Sessao($sessaoID);
        
        if( !$Sessao || !$Sessao->getID() ){
            $this->error('Sessão inválida')->json();
        }
        
        $lista = Chat::getList(array(
            "dao.id > ?" => urldecode($oldChatID),
            "per.sessao_id = ?" => $sessaoID
        ), null, 0, 1, array('dao.id asc'));
        
        if( $lista['cont_total'] > 0 ){
            
            $Chat = $lista['rows'][0];
            
            $render = array(
                'status'          => true,
                'id'              => $Chat->getID(),
                'tipo'            => $Chat->getTipo(),
                'message'         => $Chat->getMessage(),
                'autor_nome'      => $Chat->getSessaoPersonagemObj()->getNome(),
                'autor_imagem_id' => $Chat->getSessaoPersonagemObj()->getAnexoImagemID(),
                'data'            => $Chat->getData()
            );

        }
        
        $this->json($render);
    }
    
    /**
     * @return json
     */
    public function sendAction( $sessaoID = null )
    {
        $render['status'] = false;
        
        $Sessao = new Sessao($sessaoID);
        
        if( !$Sessao || !$Sessao->getID() ){
            $this->error('Sessão inválida')->json();
        }
        
        $PersOnline = $this->loadPersonagem($sessaoID);
        
        if( $Sessao->setChat($PersOnline->getID(), $this->post('message'), "M") ){
            $render['status'] = true;
        }
        
        $this->json($render);
    }
    
}