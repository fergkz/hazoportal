<?php

use Core\Model\User as User;

use RPG\SessaoPersonagem as SessaoPersonagem;

class Config extends \System\MyController
{
    
    public function __construct($requireOnline = true)
    {
        parent::__construct();
        if ( $requireOnline && !User::online() ){
            $this->redirect(url_mode."/session/login");
        }
        
//        debug($GLOBALS['request']['mode']);
        
    }
    
    protected function loadPersonagem( $sessaoID )
    {
        $lista = SessaoPersonagem::getList(array(
            'dao.sessao_id = ?' => $sessaoID,
            'dao.user_id = ?' => User::online()->getID()
        ), null, 0, 1);
        
        if( $lista['cont_total'] > 0 ){
            
            return $lista['rows'][0];
            
        }else{
            _includeError(404);
            exit;
        }
        
    }
    
    
}