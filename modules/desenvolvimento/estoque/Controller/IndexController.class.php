<?php

use RPG\Sessao as Sessao;
use System\User as User;

class IndexController extends Config
{
    
    public function __construct()
    {
//        parent::__construct();
    }
    
    /**
     * @return view
     */
    public function indexAction()
    {   
        die("here");
        $sessoes = Sessao::getList(array(
            "dao.user_id_cad = ? or per.user_id = ?" => array( User::online()->getID(), User::online()->getID() )
        ));
        
        $render['sessoes'] = $sessoes['rows'];
        
        $this->view()->setTemplate('index/index.twig')->display($render);
    }
    
}