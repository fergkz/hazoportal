<?php

class PeterController extends System\MyController
{
    /**
     * @return view
     */
    public function indexAction()
    {
        $this->view()->setTemplate('peter/index.twig')->display();
    }
    
}