<?php

use MVC\Controller as Controller;

use Core\Model\User as User;
use Model\System\Group as Grupo;
use Model\Pessoa\Usuario as Usuario;

class UsuarioController extends Controller{
    
    public function indexAction(){
        
        $render = array();
        
        if( @$_GET ){
            $get = $_GET;
            $dados = Usuario::pesquisa( $get, 'cad-usuario' );
            $render['usuarios'] = @$dados['usuarios'];
            $render['link_atual'] = @$_GET['pg'];
            unset($_GET['pg']);
            foreach( $_GET as $ind => $row ){
                $nGet[] = $ind."=".$row;
            }
            $render['link_next'] = "?".implode("&", $nGet)."&pg=";
            $render['link_cont'] = $dados['cont'];
            $render['max_links'] = $dados['max_links'];
            
        }
        
        $this->view()->display($render);
        
    }
    
    public function searchGrupoAction(){
        echo Grupo::search('cad-usuario');
    }
    
    public function cadastroAction( $usuarioID = null ){
        
        $render['usuario_id'] = $usuarioID;
        
        if( $usuarioID ){
            $User = User::getByID($usuarioID);
            if( !$User ){
                return 404;
            }
            if( $User->getGroupObj()->getMode() === 'S' ){
                return 404;
            }
            if( in_array($User->getGroupID(), $GLOBALS['config']['groups']) ){
                return 404;
            }
        }else{
            $User = new User();
        }
        
        $render['Usuario'] = $User;
        
        if( $_POST ){
            
            if( !$User->getID() ){
                $User->setRequireActivation(true);
            }
            
            $User->setEmail($_POST['email']);
            $User->setGroupID( $_POST['grupo'] );
            $User->setLogin($_POST['login']);
            $User->setName($_POST['nome']);
            $User->setStatus($_POST['status']);
            
            if( $_POST['senha'] and $_POST['senha'] !== $_POST['r_senha'] ){
                _setError("As senhas informadas não conferem");
            }elseif( !$User->getID() and !$_POST['senha'] ){
                _setError("A senha deve ser informada");
            }elseif( $_POST['senha'] ){
                $User->setPassword($_POST['senha']);
            }
            
            $User->save();
            
            if( !_getErrors() ){
                _setSuccess("Cadastro salvo com sucesso!");
                $this->redirect(url."/cadastros/usuario/visualizacao/".$User->getID());   
            }

        }
                
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $usuarioID = null ){
        
        $User = User::getByID($usuarioID);
        if( !$User ){
            return 404;
        }
        if( $User->getGroupObj()->getMode() === 'S' ){
            return 404;
        }
        if( in_array($User->getGroupID(), $GLOBALS['config']['groups']) ){
            return 404;
        }
        
        $render['Usuario'] = $User;
                        
        $this->view()->display($render);
    }
    
}