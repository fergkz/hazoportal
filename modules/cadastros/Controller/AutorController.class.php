<?php

use MVC\Controller as Controller;

use Model\Pessoa\Autor as Autor;
use Model\Pessoa\Pessoa as Pessoa;
use Model\Pessoa\Usuario as Usuario;

class AutorController extends Controller{
    
    public function indexAction(){
        $render['autores'] = Autor::listAll();
        $this->view()->display($render);
    }
    
    public function cadastroAction( $autorID = null ){
        
        $render['autor_id'] = $autorID;
        
        if( !$autorID ){
            $Autor = new Autor();
        }else{
            $Autor = Autor::getByID($autorID);
        }

        if( !$Autor ){
            return 404;
        }

        if( !empty($_POST) ){
            
            if( !$autorID ){
                $Autor->setPessoaID($_POST['pessoa']);
                $Autor->setUserID($_POST['usuario']);
            }
            $Autor->setStatus($_POST['status']);
            
            if( $Autor->save() ){
                $this->redirect(url."/cadastros/autor/visualizacao/".$Autor->getID());
            }
            
        }
        $render['Autor'] = $Autor;
        
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $autorID = null ){
        
        $render['autor_id'] = $autorID;
        $Autor = Autor::getByID($autorID);
        
        if( !$Autor ){
            return 404;
        }
        
        $render['Autor'] = $Autor;
        
        $this->view()->display($render);
    }
    
    public function searchUsuarioAction(){
        echo Usuario::search('simple-with-group');
    }
    
    public function searchPessoaAction(){
        echo Pessoa::search('cad-autor');
    }
    
}