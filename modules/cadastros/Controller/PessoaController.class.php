<?php

use MVC\Controller as Controller;

use Model\Pessoa\Pessoa         as Pessoa;
use Model\Pessoa\PessoaEmail    as PessoaEmail;
use Model\Pessoa\PessoaTelefone as PessoaTelefone;

class PessoaController extends Controller{
    
    public function indexAction(){
        $render = array();
        
        if( @$_GET ){
            $get = $_GET;
            $dados = Pessoa::pesquisa( $get );
            $render['pessoas'] = @$dados['pessoas'];
            $render['link_atual'] = @$_GET['pg'];
            unset($_GET['pg']);
            foreach( $_GET as $ind => $row ){
                $nGet[] = $ind."=".$row;
            }
            $render['link_next'] = "?".implode("&", $nGet)."&pg=";
            $render['link_cont'] = $dados['cont'];
            $render['max_links'] = $dados['max_links'];
            
        }
        
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $pessoaSlug ){

        if( $pessoaSlug ){
            $Pessoa = Pessoa::getBySlug($pessoaSlug);
            if( !$Pessoa ){
                return 404;
            }
        }
        
        $render['Pessoa'] = $Pessoa;
        
        $this->view()->display($render);
    }
    
    public function cadastroAction( $pessoaSlug = null ){

        if( $pessoaSlug ){
            $Pessoa = Pessoa::getBySlug($pessoaSlug);
            if( !$Pessoa ){
                return 404;
            }
        }else{
            $Pessoa = new Pessoa();
        }

        $render['pessoa_slug'] = $pessoaSlug;
        
        $render['emails']    = $Pessoa->getEmails();
        $render['telefones'] = $Pessoa->getTelefones();
        
        if( !empty($_POST) ){
            if( !empty($_POST['cpf']) ){
                $Pessoa->setCpf($_POST['cpf']);
            }
            $Pessoa->setRazao($_POST['nome']);
            $Pessoa->setFantasia($_POST['nome']);
            $Pessoa->setDataNascimento( $_POST['dt_nasc'] ?: null );
            $Pessoa->save();
            PessoaEmail::deleteByPessoa($Pessoa);
            PessoaTelefone::deleteByPessoa($Pessoa);

            $render['emails'] = array();
            if( $_POST['email'] ){
                foreach( $_POST['email'] as $row ){
                    if( $row ){
                        $Obj = new PessoaEmail();
                        $Obj->setEmail($row);
                        $Obj->setPessoaObj($Pessoa);
                        if( $Pessoa->getID() ){
                            $Obj->save();
                        }
                        $render['emails'][] = $Obj;
                    }
                }
            }

            $render['telefones'] = array();
            if( $_POST['telefone'] ){
                foreach( $_POST['telefone'] as $row ){
                    if( $row ){
                        $Obj = new PessoaTelefone();
                        $Obj->setTelefone($row);
                        $Obj->setPessoaObj($Pessoa);
                        if( $Pessoa->getID() ){
                            $Obj->save();
                        }
                        $render['telefones'][] = $Obj;
                    }
                }
            }
            if( !_getErrors() ){
                _setSuccess("Alterações de dados em ".$Pessoa->getRazao()." efetuadas com sucesso!");
                $this->redirect(url."/cadastros/pessoa/visualizacao/".$Pessoa->getSlug());
            }
        }
        
        $render['Pessoa'] = $Pessoa;

        $this->view()->display($render);
    }
    
}