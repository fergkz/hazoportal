<?php

use MVC\Controller as Controller;
use Model\Projetos\Atividade as Atividade;
use Model\Projetos\Demanda as Demanda;
use Model\Projetos\Operacao as Operacao;
use Model\Projetos\Anexo as Anexo;
use Core\Model\User as User;
use Model\Projetos\AtividadeGrupo as AtividadeGrupo;

class ExecucaoAtividadeController extends Controller{
    
    public function indexAction(){
        $this->redirect(url."/projetos/execucao-atividade/minhas-atividades");
    }
    
    public function minhasAtividadesAction(){
        $render['atividades_desenvolvimento']    = Atividade::listaAtividadesDesenvolvimentoLogado();
        $render['atividades_homologacao']        = Atividade::listaAtividadesHomologacaoLogado();
        $render['atividades_av_desenvolvimento'] = Atividade::listaAtividadesDesenvolvimentoLogado(false);
        $render['atividades_av_homologacao']     = Atividade::listaAtividadesHomologacaoLogado(false);
        $this->view()->display($render);
    }
    
    public function executarAction( $demandaSlug = null, $atividadeID = null ){
        $Demanda = null;
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        if( !$Atividade or !$Atividade->getID() or ($Atividade->getDemandaID() and !$demandaSlug) ){
            return 404;
        }
        
        $permiteDesenvolver = Atividade::permiteLogadoDesenvolver( $Atividade );
        $permiteHomologar   = Atividade::permiteLogadoHomologar( $Atividade );
        if( !$permiteDesenvolver && !$permiteHomologar ){
            return 404;
        }
        
        $requeridas = $Atividade->getAtividadesRequeridas();
        if( $requeridas ){
            foreach( $requeridas as $Requerida ){
                if( $Requerida->getAtividadeRequeridaObj()->getEtapaID() != 'U' ){
                    return 404;
                }
            }
        }
        
        $EmAberto = Operacao::getEmAbertoByAtividadeLogado($Atividade);
        $render['Operacao'] = $EmAberto;
        
        if( !$EmAberto and empty($_POST['acao']) ){
            $Operacao = new Operacao();
            $Operacao->setAtividadeObj($Atividade);
            $Operacao->setDescricao(@$_POST['descricao']);
            $Operacao->abre();
            $render['Operacao'] = $Operacao;
        }
        
        if( $EmAberto ){
            if( !empty($_FILES['anexo_add']) && count($_FILES['anexo_add']) > 0 ){

                foreach( $_FILES['anexo_add']['name'] as $ind => $row ){

                    if( empty($_FILES['anexo_add']['name'][$ind]) ){
                        continue;
                    }

                    $Anexo = new Anexo();
                    $Anexo->setOperacaoObj($EmAberto);
                    $Anexo->setDescricao(@$_POST['anx_descr'][$ind]);
                    $Anexo->setTitulo(@$_POST['anx_titulo'][$ind]);
                    $Anexo->setPrincipal(@$_POST['principal'][$ind]);

                    $Anexo->setFileContent( file_get_contents($_FILES['anexo_add']['tmp_name'][$ind]) );
                    $Anexo->setFileName( $_FILES['anexo_add']['name'][$ind] );
                    $Anexo->setFileSize( $_FILES['anexo_add']['size'][$ind] );
                    $Anexo->setFileType( $_FILES['anexo_add']['type'][$ind] );

                    $Anexo->save();
                }
            }
            if( !empty($_POST['anx_excl']) && count($_POST['anx_excl']) > 0 ){
                foreach( $_POST['anx_excl'] as $id ){
                    if( _getErrors() ){
                        break;
                    }
                    $Anexo = Anexo::getByID($id);
                    if( $Anexo->getOperacaoID() == $EmAberto->getID() ){
                        $Anexo->save("D");
                    }else{
                        _setError("O anexo não pertence a esta operação");
                    }
                }
            }
        }
            
        switch( @$_POST['acao'] ){
            case "somente_salvar":
                if( $EmAberto ){
                    $EmAberto->setDescricao(@$_POST['descricao']);
                    $EmAberto->save();
                    $render['Operacao'] = $EmAberto;
                    _setSuccess("Estado salvo com sucesso");
                }
                break;
                
            case "alterar_operacao":
                if( $EmAberto ){
                    $EmAberto->setDescricao(@$_POST['descricao']);
                    $EmAberto->fecha();
                    
                    $render['Operacao'] = $EmAberto;
                }else{
                    $Operacao = new Operacao();
                    $Operacao->setAtividadeObj($Atividade);
                    $Operacao->setDescricao(@$_POST['descricao']);
                    $Operacao->abre();
                    $render['Operacao'] = $Operacao;
                }
                
                if( !_getErrors() ){
                    $Atividade->save();
                }
                
                break;

            case "finalizar_atividade":
                if( in_array($Atividade->getEtapaID(), array("G", "H")) ){
                    $Atividade->setEtapaID("U");
                }
                elseif( in_array($Atividade->getEtapaID(), array("D", "P")) ){
                    $Atividade->setEtapaID("V");
                }

                if( $Atividade->save() ){
                    $Atividade->atualizarProximaEtapa();
                    _setSuccess("Atividade Concluída");
                    $this->redirect(url."/projetos/execucao-atividade/minhas-atividades");
                }
                break;
        }
        
        $render["Demanda"]   = $Demanda;
        $render["Atividade"] = $Atividade;
        if( $render['Operacao'] ){
            $render['anexos'] = $render['Operacao']->getAnexos();
        }
        
        $this->view()->setTemplate("execucao-atividade/executar.html")->display($render);
    }

    public function iniciarAvulsaAction(){
        
        $Atividade = Atividade::getIndependenteAberta();
        
        if( !$Atividade ){
            if( @$_POST['acao'] === 'criar_atividade' ){
                $Atividade = new Atividade();
                $Atividade->setDemandaID( @$_POST['demanda'] ?: null )
                          ->setClienteID( @$_POST['cliente'] ?: null )
                          ->setDuracaoEstimada( @$_POST['duracao'] ?: 0 )
                        ->setTitulo( @$_POST['titulo'] ?: null )
                        ->setEtapaID("I");
                
                if( !$Atividade->save() ){
                    $this->view()->setTemplate("execucao-atividade/iniciar-avulsa-info-atv.html")->display();
                    exit;
                }

                $AtividadeGrupo = new AtividadeGrupo();
                $AtividadeGrupo->setAtividadeID( $Atividade->getID() );
                $AtividadeGrupo->setExecucao( "D" );
                $AtividadeGrupo->setUserID( User::online()->getID() );
                $AtividadeGrupo->save();
            }else{
                $this->view()->setTemplate("execucao-atividade/iniciar-avulsa-info-atv.html")->display();
                exit;
            }
        }
        
        $Operacao = Operacao::getEmAbertoByAtividadeLogado($Atividade);
        
        $render['Operacao'] = $Operacao;
        
        if( !$Operacao ){
            $Operacao = new Operacao();
            $Operacao->setAtividadeObj($Atividade);
            $Operacao->setDescricao(@$_POST['descricao']);
            $Operacao->abre();
            $render['Operacao'] = $Operacao;
        }
        
        if( $Operacao ){
            if( !empty($_FILES['anexo_add']) && count($_FILES['anexo_add']) > 0 ){

                foreach( $_FILES['anexo_add']['name'] as $ind => $row ){

                    if( empty($_FILES['anexo_add']['name'][$ind]) ){
                        continue;
                    }

                    $Anexo = new Anexo();
                    $Anexo->setAtividadeObj($Atividade);
                    $Anexo->setDescricao(@$_POST['anx_descr'][$ind]);
                    $Anexo->setTitulo(@$_POST['anx_titulo'][$ind]);
                    $Anexo->setPrincipal(@$_POST['principal'][$ind]);

                    $Anexo->setFileContent( file_get_contents($_FILES['anexo_add']['tmp_name'][$ind]) );
                    $Anexo->setFileName( $_FILES['anexo_add']['name'][$ind] );
                    $Anexo->setFileSize( $_FILES['anexo_add']['size'][$ind] );
                    $Anexo->setFileType( $_FILES['anexo_add']['type'][$ind] );

                    $Anexo->save();
                }
            }

            if( !empty($_POST['anx_excl']) && count($_POST['anx_excl']) > 0 ){
                foreach( $_POST['anx_excl'] as $id ){
                    if( _getErrors() ){
                        break;
                    }
                    $Anexo = Anexo::getByID($id);
                    if( $Anexo->getAtividadeID() == $Atividade->getID() ){
                        $Anexo->save("D");
                    }else{
                        _setError("O anexo não pertence a esta atividade");
                    }
                }
            }
        }
            
        switch( @$_POST['acao'] ){
            case "somente_salvar":
                if( $Operacao ){
                    $Operacao->setDescricao(null);
                    $Operacao->save();
                    $render['Operacao'] = $Operacao;
                    _setSuccess("Estado salvo com sucesso");
                }
                $Atividade->setTitulo(@$_POST['titulo'] ?: null);
                $Atividade->setDemandaID(@$_POST['demanda'] ?: null);
                $Atividade->setClienteID( @$_POST['cliente'] ?: null );
                $Atividade->setDescricao(@$_POST['descricao']);
                $Atividade->save();
                break;
                
            case "finalizar_atividade":
                $Operacao->setDescricao(null);
                $Operacao->fecha();

                $Atividade->setEtapaID("U");
                $Atividade->setDemandaID(@$_POST['demanda'] ?: null);
                $Atividade->setClienteID( @$_POST['cliente'] ?: null );
                $Atividade->setDescricao(@$_POST['descricao']);

                if( @!$_POST['titulo'] or @trim($_POST['titulo']) == "" ){
                    _setError("O título deve ser informado para conluir a atividade");
                }else{
//                    $Atividade->setTitulo( Format::dataToBr($Operacao->getDataInicio())." - ".User::online()->getName()." - ".@$_POST['titulo']);
                    $Atividade->setTitulo(@$_POST['titulo'] ?: null);
                    if( $Atividade->save() ){
                        $Atividade->atualizarProximaEtapa();
                        _setSuccess("Atividade Concluída");
                        $this->redirect(url."/projetos/execucao-atividade/minhas-atividades");
                    }
                }
                break;
        }
        
        $render["Atividade"] = $Atividade;
        $render["Atividade"]->setTitulo( trim($Atividade->getTitulo()) );
        if( $render['Operacao'] ){
            $render['anexos'] = $render['Atividade']->getAnexos();
        }
        
        $this->view()->display($render);
    }
    
}