<?php

use MVC\Controller as Controller;
use Model\System\Group as Group;
use Model\Projetos\Demanda as Demanda;
use Model\Projetos\Atividade as Atividade;
use Model\Projetos\AtividadeRequerida as AtividadeRequerida;
use Model\Projetos\AtividadeGrupo as AtividadeGrupo;
use Model\Projetos\AtividadeEtapa as Etapa;
use Model\Projetos\Operacao as Operacao;
use Model\Projetos\AtividadePadroes as Padroes;
use Model\Pessoa\Usuario as Usuario;
use Model\Projetos\Anexo as Anexo;
use Core\Model\User as User;
use Lib\Format as Format;

class AtividadesController extends Controller{
    
    public function indexAction( $demandaSlug = null ){
        $render['Demanda'] = Demanda::getInstanceBySlug($demandaSlug);
        
        if( !$render['Demanda'] || !$render['Demanda']->getID() ){
            return 404;
        }
        
        $render['atividades'] = $render['Demanda']->getAtividades();
        $render['atividades_selecionaveis'] = Etapa::listaProximoPermiteSelecionar();

        $this->view()->display($render);
    }
    
    public function detalhesAction( $demandaSlug = null, $atividadeID = null ){
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        if( !$Atividade or !$Atividade->getID() or ($Atividade->getDemandaID() and !$demandaSlug) ){
            return 404;
        }
        $render['Atividade'] = $Atividade;
        $this->view()->display($render);
    }
    
    public function cadastroAction( $demandaSlug = null, $atividadeID = null ){
        $Demanda = null;
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            $render['demandaSlug'] = $demandaSlug;
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        $Padroes = Padroes::getByLogado();
        
        if( $_POST && @$_POST['salvar'] ){
            
            if( !$Atividade ){
                $Atividade = new Atividade();
            }
            
            $padroesData = $Padroes->getContent();
            
            if( @$_POST['padrao_etapa'] == 'S' ){
                $padroesData['etapa'] = @$_POST['etapa'];
            }
            if( @$_POST['padrao_duracao'] == 'S' ){
                $padroesData['duracao_estimada'] = @$_POST['duracao_estimada'];
            }
            if( @$_POST['padrao_grupos_desenvolvimento'] == 'S' ){
                $padroesData['grupos_desenvolvimento'] = @$_POST['grupos_desenvolvimento'];
            }
            if( @$_POST['padrao_usuarios_desenvolvimento'] == 'S' ){
                $padroesData['usuarios_desenvolvimento'] = @$_POST['usuarios_desenvolvimento'];
            }
            if( @$_POST['padrao_grupos_homologacao'] == 'S' ){
                $padroesData['grupos_homologacao'] = @$_POST['grupos_homologacao'];
            }
            if( @$_POST['padrao_usuarios_homologacao'] == 'S' ){
                $padroesData['usuarios_homologacao'] = @$_POST['usuarios_homologacao'];
            }
            $Padroes->setContent($padroesData)->save();
            
            if( !$Demanda or !$Atividade->getDemandaID() or ($Demanda->getID() !== @$_POST['demanda']) ){
                $Atividade->setDemandaID( @$_POST['demanda'] ?: null );
            }/*else{
                $Atividade->setDemandaID($Demanda ? $Demanda->getID() : (@$_POST['demanda'] ?: null) );
            }*/
            if( !$Atividade->getClienteID() or $Atividade->getClienteID() !== @$_POST['cliente'] ){
                $Atividade->setClienteID( @$_POST['cliente'] ?: null );
            }
            
            $Atividade->setDescricao($_POST['descricao'])
                    ->setDuracaoEstimada($_POST['duracao_estimada'])
                    ->setParentAtividadeID($_POST['atividade_pai'] ? $_POST['atividade_pai'] : null)
                    ->setTitulo($_POST['titulo']);
            if( $Atividade->getEtapaID() !== 'I' ){
                $Atividade->setEtapaID( _coalesce($_POST['etapa'], "A") );
            }
            if( $Atividade->save() ){
                AtividadeRequerida::deleteByAtividadeDependente( $Atividade->getID() );
                if( !empty($_POST['atividades_requeridas']) ){
                    $requeridas = array_unique($_POST['atividades_requeridas']);
                    foreach( $requeridas as $row ){
                        if( !$row || $row == $Atividade->getID() ){
                            continue;
                        }
                        $AtividadeRequerida = new AtividadeRequerida();
                        $AtividadeRequerida->setAtividadeDependenteID( $Atividade->getID() );
                        $AtividadeRequerida->setAtividadeRequeridaID( $row );
                        if( !$AtividadeRequerida->save() ){
                            _setError("Não foi possível definir a atividade requerida");
                            break;
                        }
                    }
                }
                AtividadeGrupo::deleteByAtividade( $Atividade->getID() );
                if( !empty($_POST['grupos_desenvolvimento']) ){
                    $grupos = array_unique($_POST['grupos_desenvolvimento']);
                    foreach( $grupos as $row ){
                        if( !$row ){
                            continue;
                        }
                        $AtividadeGrupo = new AtividadeGrupo();
                        $AtividadeGrupo->setAtividadeID( $Atividade->getID() );
                        $AtividadeGrupo->setExecucao( "D" );
                        $AtividadeGrupo->setGroupID( $row );
                        if( !$AtividadeGrupo->save() ){
                            _setError("Não foi possível definir o grupo de desenvolvimento da atividade");
                            break;
                        }
                    }
                }
                if( !empty($_POST['usuarios_desenvolvimento']) ){
                    $usuarios = array_unique($_POST['usuarios_desenvolvimento']);
                    foreach( $usuarios as $row ){
                        if( !$row ){
                            continue;
                        }
                        $AtividadeGrupo = new AtividadeGrupo();
                        $AtividadeGrupo->setAtividadeID( $Atividade->getID() );
                        $AtividadeGrupo->setExecucao( "D" );
                        $AtividadeGrupo->setUserID( $row );
                        if( !$AtividadeGrupo->save() ){
                            _setError("Não foi possível definir o usuário de desenvolvimento da atividade");
                            break;
                        }
                    }
                }
                if( !empty($_POST['grupos_homologacao']) ){
                    $grupos = array_unique($_POST['grupos_homologacao']);
                    foreach( $grupos as $row ){
                        if( !$row ){
                            continue;
                        }
                        $AtividadeGrupo = new AtividadeGrupo();
                        $AtividadeGrupo->setAtividadeID( $Atividade->getID() );
                        $AtividadeGrupo->setExecucao( "H" );
                        $AtividadeGrupo->setGroupID( $row );
                        if( !$AtividadeGrupo->save() ){
                            _setError("Não foi possível definir o grupo de homologação da atividade");
                            break;
                        }
                    }
                }
                if( !empty($_POST['usuarios_homologacao']) ){
                    $usuarios = array_unique($_POST['usuarios_homologacao']);
                    foreach( $usuarios as $row ){
                        if( !$row ){
                            continue;
                        }
                        $AtividadeGrupo = new AtividadeGrupo();
                        $AtividadeGrupo->setAtividadeID( $Atividade->getID() );
                        $AtividadeGrupo->setExecucao( "H" );
                        $AtividadeGrupo->setUserID( $row );
                        if( !$AtividadeGrupo->save() ){
                            _setError("Não foi possível definir o usuários de homologação da atividade");
                            break;
                        }
                    }
                }
                
                if( !$Atividade->getGruposDesenvolvimento() && $Atividade->getGruposHomologacao() ){
                    if( in_array($Atividade->getEtapaID(), array('D','P')) ){
                        $Atividade->setEtapaID("H")->save();
                    }
                }
                
                if( !empty($_FILES['anexo_add']) && count($_FILES['anexo_add']) > 0 ){

                    foreach( $_FILES['anexo_add']['name'] as $ind => $row ){

                        if( empty($_FILES['anexo_add']['name'][$ind]) ){
                            continue;
                        }

                        $Anexo = new Anexo();
                        $Anexo->setAtividadeObj($Atividade);
                        $Anexo->setDescricao(@$_POST['anx_descr'][$ind]);
                        $Anexo->setTitulo(@$_POST['anx_titulo'][$ind]);
                        $Anexo->setPrincipal(@$_POST['principal'][$ind]);

                        $Anexo->setFileContent( file_get_contents($_FILES['anexo_add']['tmp_name'][$ind]) );
                        $Anexo->setFileName( $_FILES['anexo_add']['name'][$ind] );
                        $Anexo->setFileSize( $_FILES['anexo_add']['size'][$ind] );
                        $Anexo->setFileType( $_FILES['anexo_add']['type'][$ind] );

                        $Anexo->save();
                    }
                }

                if( !empty($_POST['anx_excl']) && count($_POST['anx_excl']) > 0 ){
                    foreach( $_POST['anx_excl'] as $id ){
                        if( _getErrors() ){
                            break;
                        }
                        $Anexo = Anexo::getByID($id);
                        if( $Anexo->getAtividadeID() == $Atividade->getID() ){
                            $Anexo->save("D");
                        }else{
                            _setError("O anexo não pertence a esta atividade");
                        }
                    }
                }
                
                if( !empty($_POST['exc_op_anexo']) && count($_POST['exc_op_anexo']) > 0 ){
                    foreach( $_POST['exc_op_anexo'] as $id ){
                        Anexo::getInstance($id)->save("D");
                    }
                }
                
                if( @$_POST['op_data_inicio'] ){
                    foreach( $_POST['op_data_inicio'] as $id => $data ){
                        $Operacao = Operacao::getByID($id);
                        $Operacao->setDataInicio( Format::dataToDb($_POST['op_data_inicio'][$id]) )
                                 ->setDataFinal( Format::dataToDb($_POST['op_data_final'][$id]) )
                                 ->save();
                    }
                }
                
                if( !_getErrors() ){
                    if( $Atividade->getDemandaObj() ){
                        $this->redirect(url."/projetos/demandas/visualizacao/$/".$Atividade->getDemandaObj()->getSlug());
                    }else{
                        $this->redirect(url."/projetos/atividades/avulsas-lista");
                    }
                }else{
                    _rollback();
                }
            }
        }
        
        $render['Demanda']         = $Demanda;
        $render['Atividade']       = $Atividade;
        $render['padroes_content'] = $Padroes->getContent();
        if( $Atividade ){
            $render['operacoes']   = Operacao::listaByAtividade($Atividade);
            $render['anexos']          = $Atividade->getAnexos();
        }
        $this->view()->declareFunction("in_array")->display($render);
    }
    
    public function excluirAction( $demandaSlug = null, $atividadeID = null ){
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        if( !$Atividade or 
            !$Atividade->getID() or 
            $Atividade->getCadastroUserID() != User::online()->getID() or
            ($Atividade->getDemandaID() and !$demandaSlug) ){
            return 404;
        }
        
        if( $demandaSlug ){
            if( $Atividade->save("D") ){
                $this->redirect(url."/projetos/demandas/visualizacao/$/".$Demanda->getSlug());
            }else{
                $this->redirect(url."/projetos/atividades/cadastro/$/".$Demanda->getSlug()."/".$Atividade->getID());
            }
        }else{
            if( $Atividade->save("D") ){
                $this->redirect(url."/projetos/atividades/avulsas-lista");
            }else{
                $this->redirect(url."/projetos/atividades/cadastro/$/0/".$Atividade->getID());
            }
        }

    }
    
    public function alterarProximaEtapaAction( $demandaSlug = null, $atividadeID = null, $etapaID = null ){
        
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        if( !$Atividade || !$Atividade->getID() ){
            return 404;
        }
        
        if( !$etapaID ){
        
            $etapas = Etapa::listaProximoPermiteSelecionar();

            $etapas = array_reverse($etapas);
            $ProximaEtapa = null;
            foreach( $etapas as $Etapa ){
                if( $Atividade->getEtapaID() == $Etapa->getID() ){
                    break;
                }
                $ProximaEtapa = $Etapa;
            }

            if( !$ProximaEtapa ){
                $ProximaEtapa = end($etapas);
            }

            $Atividade->setEtapaObj( $ProximaEtapa );
            
        }else{
            $Atividade->setEtapaObj( Etapa::getInstance($etapaID) );
        }
        
        header('Content-Type: text/html; charset=UTF-8');
        if( $Atividade->save() ){
            $data['status']    = true;
            $data['mensagem']  = utf8_encode(htmlentities("Etapa da atividade alterada com sucesso"));
            $data['descricao'] = $Atividade->getEtapaObj()->getDescricao();
        }else{
            $data['status']    = false;
            $data['mensagem']  = utf8_encode(htmlentities("Não foi possível alterar a situação da atividade"));
        }
        echo json_encode($data);
    }
    
    public function searchAtividadeDemandaAction( $demandaSlug = null ){
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            echo Atividade::search( "simple", $Demanda->getID() );
        }else{
            echo Atividade::search( "simple-sem-demanda" );
        }
    }
    
    public function searchGroupAction(){
        echo Group::search();
    }
    
    public function searchUserAction(){
        echo Usuario::search("simple-with-group");
    }
    
    public function searchEtapaAction(){
        echo Etapa::searchPermiteSelecionar();
    }
    
    public function searchDemandaAction(){
        echo Demanda::search('simple_ger');
    }
    
    public function alterarOrdenacaoAction( $demandaSlug = null ){
        $data['log'] = $_POST;
        
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);

            if( !$Demanda || !$Demanda->getID() ){
                $data['mensagem'] = "A demanda está incorreta";
                die(json_encode($data));
            }
        }
        
        $atividades = $demandaSlug ? $Demanda->getAtividades() : Atividade::listaSemDemanda();
        
        if( !$atividades ){
            $data['mensagem'] = "Atividades inexistentes";
            die(json_encode($data));
        }
        
        foreach( $atividades as $Atividade ){
            if( isset( $_POST[$Atividade->getID()] ) ){
                $Atividade->setPrioridade( $_POST[$Atividade->getID()] );
                if( !$Atividade->save() ){
                    $data['mensagem'] = "Não foi possível alterar a prioridade da atividade";
                    die(json_encode($data));
                }
            }else{
                $data['mensagem'] = "Atividade não cadastrada";
                die(json_encode($data));
            }
        }
        $data['status'] = true;
        echo json_encode($data);
    }
    
    public function listaHistoricoAction( $demandaSlug = null, $atividadeID = null ){
        $Demanda = null;
        if( $demandaSlug ){
            $Demanda = Demanda::getInstanceBySlug($demandaSlug);
            if( !$Demanda || !$Demanda->getID() ){
                return 404;
            }
            $Atividade = $Demanda->getAtividade($atividadeID);
        }else{
            $Atividade = Atividade::getByID($atividadeID);
        }
        
        if( !$Atividade or !$Atividade->getID() and ($Atividade->getDemandaID() and !$demandaSlug) ){
            return 404;
        }
        
        $render['Demanda']   = $Demanda;
        $render['Atividade'] = $Atividade;
        $render['operacoes'] = Operacao::listaByAtividade($Atividade);
        
        $this->view()->display($render);
    }

    public function avulsasListaAction(){
        $render['atividades'] = Atividade::listaSemDemanda();
        $render['atividades_selecionaveis'] = Etapa::listaProximoPermiteSelecionar();
        $this->view()->display($render);
    }
    
}