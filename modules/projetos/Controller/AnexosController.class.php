<?php

use Projeto\Anexo   as Anexo;
use Projeto\Demanda as Demanda;

class AnexosController extends \System\MyController
{   
    public function visualizacaoAction( $slug )
    {
        $Anexo = Anexo::getBySlug($slug);
        
        if( !$Anexo ){
            return 404;
        }
        
//        header("Content-disposition: attachment; filename={$slug}.".$Anexo->getFileExtension());
        header('Content-type: '.$Anexo->getFileType());
        die($Anexo->getFileContent());
        
        
//        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
//        header("Pragma: public");
//        header("Expires: 0");
//        header("Cache-Control: private");
//        header("Content-type: ".$Edicao->getCapaFileType());
//        header("Content-disposition: inline; filename=".$Edicao->getCapaFileName());
//        header("Content-length: ".$Edicao->getCapaFileSize());
        
        
    }
    
    public function listaAction( $modo = null, $slug = null, $atividadeID = null )
    {
        $render = array();
        switch( $modo ){
            case "demanda":
                $Demanda = Demanda::getInstanceBySlug($slug);
                $render['anexos']  = $Demanda->listaAnexos();
                break;
            case "atividade":
                $Demanda = Demanda::getInstanceBySlug($slug);
                $Atividade = $Demanda->getAtividade($atividadeID);
                $render['anexos']  = $Atividade->listaAnexos();
                break;
            default:
                return 404;
                break;
        }
        $this->view()->display($render);
    }
    
}