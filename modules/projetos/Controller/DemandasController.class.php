<?php

use MVC\Controller as Controller;
use Model\Pessoa\Cliente as Cliente;
use Model\Projetos\TipoDemanda as TipoDemanda;
use Model\Projetos\Demanda as Demanda;
use Model\Pessoa\Usuario as Usuario;
use Model\Projetos\DemandaGerente as Gerente;
use Model\Projetos\Anexo as Anexo;
use Model\Projetos\Atividade as Atividade;
use Core\Model\User as User;

class DemandasController extends Controller{
    
    public function indexAction(){
        $render = array();
        if( $_POST ){
            $status = array(
                @$_POST['status_a'],
                @$_POST['status_c'],
                @$_POST['status_n'],
                @$_POST['status_s']
            );
            $demandas = Demanda::pesquisa( @$_POST['cliente'], User::online(), @$_POST['texto'], @$status );
            if( $demandas ){
                foreach( $demandas as $Demanda ){
                    $render['demandas'][$Demanda->getStatus()][] = $Demanda;
                }
            }
        }else{
            $demandas = Demanda::pesquisa( null, User::online(), null, "A" );
            if( $demandas ){
                foreach( $demandas as $Demanda ){
                    $render['demandas'][$Demanda->getStatus()][] = $Demanda;
                }
            }
        }
        
        function gerenteValido( $demanda ){
            return Gerente::isGerenteLogadoByDemanda( $demanda );
        }
        
        $this->view()->declareFunction("gerenteValido")->display($render);
    }
    
    public function cadastroAction( $demandaSlug = null ){
        $Demanda = Demanda::getInstanceBySlug($demandaSlug);
        $render['Demanda']  = $Demanda;
        $render['gerentes'] = Gerente::listaByDemanda($Demanda);
        
        if( !$this->validaUsuarioAlteracao($Demanda) ){
            return 403;
        }

        if( $_POST ){
            
            if( @$_POST['ALTERACAO'] ){
                
                $Demanda->setTitulo($_POST['titulo'])
                        ->setClienteID($_POST['cliente'])
                        ->setDescricao($_POST['descricao'])
                        ->setTipoDemandaID($_POST['tipo']);
                if( $Demanda->save() ){
                        
                    if( !empty($_FILES['anexo_add']) && count($_FILES['anexo_add']) > 0 ){

                        foreach( $_FILES['anexo_add']['name'] as $ind => $row ){
                            
                            if( empty($_FILES['anexo_add']['name'][$ind]) ){
                                continue;
                            }
                            
                            $Anexo = new Anexo();
                            $Anexo->setDemandaObj($Demanda);
                            $Anexo->setDescricao(@$_POST['anx_descr'][$ind]);
                            $Anexo->setTitulo(@$_POST['anx_titulo'][$ind]);
                            $Anexo->setPrincipal(@$_POST['principal'][$ind]);
                            
                            $Anexo->setFileContent( file_get_contents($_FILES['anexo_add']['tmp_name'][$ind]) );
                            $Anexo->setFileName( $_FILES['anexo_add']['name'][$ind] );
                            $Anexo->setFileSize( $_FILES['anexo_add']['size'][$ind] );
                            $Anexo->setFileType( $_FILES['anexo_add']['type'][$ind] );
                            
                            $Anexo->save();
                        }
                    }
                    
                    if( !empty($_POST['anx_excl']) && count($_POST['anx_excl']) > 0 ){
                        foreach( $_POST['anx_excl'] as $id ){
                            if( _getErrors() ){
                                break;
                            }
                            $Anexo = Anexo::getByID($id);
                            if( $Anexo->getDemandaID() == $Demanda->getID() ){
                                $Anexo->save("D");
                            }else{
                                _setError("O anexo não pertence a esta demanda");
                            }
                        }
                    }

                    if( !_getErrors() ){
                        if( !empty($_POST['gerentes']) ){
                            Gerente::clearByDemanda( $Demanda->getID() );
                            $post_gerentes = array_unique($_POST['gerentes']);
                            $gerentes = array();
                            $Gerente = Gerente::getInstance()
                                              ->setUserID( User::online()->getID() )
                                              ->setDemandaID($Demanda->getID())
                                              ->save();
                            $gerentes[] = $Gerente;
                            foreach( $post_gerentes as $userID ){
                                if( !$userID ){
                                    continue;
                                }
                                $Gerente = Gerente::getInstance()
                                                  ->setUserID($userID)
                                                  ->setDemandaID($Demanda->getID())
                                                  ->save();
                                $gerentes[] = $Gerente;
                            }
                            $render['gerentes'] = $gerentes;
                        }

                        $this->redirect(url."/projetos/demandas/visualizacao/$/".$Demanda->getSlug());
                    }
                }
                
            }elseif( @$_POST['EXCLUSAO'] ){
                if( $Demanda->save("D") ){
                    $this->redirect(url."/projetos/demandas");
                }
            }
        }
        $render['anexos'] = $Demanda->getAnexos();
        $this->view()->display($render);
    }
    
    public function historicoAction( $demandaSlug = null ){
        $Demanda = Demanda::getInstanceBySlug($demandaSlug);
        $render['Demanda'] = $Demanda;
        if( $_POST ){
            $Demanda->setTitulo($_POST['titulo'])
                    ->setClienteID($_POST['cliente'])
                    ->setDescricao($_POST['descricao'])
                    ->setTipoDemandaID($_POST['tipo']);
            if( $Demanda->save() ){
                $this->redirect(url."/projetos/demandas/visualizacao/$/".$Demanda->getSlug());
            }
        }
        $this->view()->display($render);
    }
    
    public function visualizacaoAction( $demandaSlug = null ){
        $Demanda = Demanda::getInstanceBySlug($demandaSlug);
        if( !$Demanda->getID() ){
            return 404;
        }
        $render['Demanda'] = $Demanda;
        $this->view()->display($render);
    }
    
    public function cadastroTiposDemandaAction( $tipoID = false, $excluir = false ){
        $render['TipoDemanda'] = TipoDemanda::getInstance($tipoID);
        if( $excluir ){
            if( $render['TipoDemanda']->save("D") ){
                _setSuccess("Tipo excluído com sucesso");
            }
            $this->redirect(url."/projetos/demandas/cadastro-tipos-demanda");
        }
        if( $_POST ){
            $TipoDemanda = TipoDemanda::getInstance($tipoID ? $tipoID : $_POST['id']);
            $TipoDemanda->setID($_POST['id'])
                    ->setTitulo($_POST['titulo'])
                    ->setDescricao($_POST['descricao']);
            if( $TipoDemanda->save() ){
                _setSuccess("Tipo salvo com sucesso!");
                $this->redirect(url."/projetos/demandas/cadastro-tipos-demanda");
            }else{
                $render['TipoDemanda'] = $TipoDemanda;
            }
        }
        $render['tipos_demanda'] = TipoDemanda::listAll();
        $this->view()->display($render);
    }
    
    public function alterarOrdenacaoAction(){
        $demandas = Demanda::listaByStatus( $_POST['status'], User::online() );
        if( !$demandas ){
            $data['mensagem'] = "Demandas inexistentes";
            die(json_encode($data));
        }
        foreach( $demandas as $Demanda ){
            if( isset( $_POST["demandas"][$Demanda->getID()] ) ){
                $Demanda->setPrioridade( $_POST["demandas"][$Demanda->getID()] );
                if( !$Demanda->save() ){
                    $data['mensagem'] = "Não foi possível alterar a prioridade da demanda";
                    die(json_encode($data));
                }
            }else{
                $data['mensagem'] = "Para alterar a prioridade, todas as demandas abertas devem estar listadas";
                die(json_encode($data));
            }
        }
        $data['status'] = true;
        echo json_encode($data);
    }
    
    public function searchClienteAction(){
        echo Cliente::search();
    }
    
    public function searchTipoDemandaAction(){
        echo TipoDemanda::search();
    }
    
    public function searchGerenteAction(){
        echo Usuario::search("nao-logado");
    }
    
    private function validaUsuarioAlteracao( $demanda ){
        $Demanda = is_object($demanda) ? $demanda : Demanda::getInstance($demanda);
        if( !$Demanda->getID() ){
            return true;
        }
        if( $Demanda->getCadastroUserID() == User::online()->getID() ){
            return true;
        }
        return Gerente::isGerenteLogadoByDemanda( $demanda );
    }
    
    public function relatorioAction( $demandaSlug = null )
    {
        $Demanda = Demanda::getInstanceBySlug($demandaSlug);
        if( !$Demanda->getID() ){
            return 404;
        }
        $render['Demanda'] = $Demanda;
        
        $render['atividades'] = Atividade::listaByDemanda($Demanda);
        $render['data_atual'] = date('d/m/Y H:i:s');
        
        $nameCliente = $Demanda->getClienteObj()->getPessoaObj()->getRazao();
        $nameData = date("Y-m-d")." ".date("H")."h".date("m")."m";
        
        if( !empty($_GET['to_pdf']) ){
            $render['pdf'] = true;
            ob_start();
            $this->view()->setTemplate("demandas/relatorio-pdf.twig")->display($render);
            $content = ob_get_contents();
            ob_end_clean();
            
            require_once(path."/library/PDF/html2pdf/html2pdf.class.php");
            $html2pdf = new HTML2PDF('P','A4','pt');
            $html2pdf->WriteHTML($content);
            $html2pdf->Output('exemple.pdf');
        }else{
            $this->view()->setTemplate("demandas/relatorio-html.twig")->display($render);
        }
    }

}
