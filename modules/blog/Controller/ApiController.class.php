<?php

use MVC\Controller as Controller;

use Model\Pagina\Pagina as Pagina;
use Core\Model\User as User;

class ApiController extends Controller{
    
    public function contadorAction( $paginaID = null ){
        
        if( !$paginaID ){
            return 404;
        }
        
        $Pagina = Pagina::getByID($paginaID);
        
        if( !$Pagina || $Pagina->getStatus() !== 'A' ){
            return 404;
        }
        
        $indice = 'pag_'.$paginaID;
        
        if( !@$_COOKIE[$indice] ){
            if( $Pagina->adicionaContador() ){
                setcookie($indice, true);
            }
        }
    }
    
    public function getDadosUsuarioOnlineAction(){
        
        $User = User::online();
        
        if( !$User ){
            $response = array(
                'status' => false,
                'message' => 'Usuário offline'
            );
        }else{
            $response = array(
                'status'  => true,
                'message' => 'Usuário online',
                'usuario' => array(
                    'nome'       => $User->getName(),
                    'login'      => $User->getLogin(),
                    'group_mode' => strtolower($User->getGroupObj()->getMode()),
                )
            );

            if( _isGranted("/editor/modal-artigo/selecionar-editoria") ){
                
                $response['usuario']['permite_criar_artigo'] = true;
            }
        }
        
        $response['message'] = utf8_encode($response['message']);
        
        die( json_encode($response) );
    }
    
    public function usuarioLoginAction(){
        
        $User = User::login($_POST['log_login'], $_POST['log_senha']);
        
        $response = null;
        
        if( !$User ){
            $response['msg'] = "Dados de login inválidos";
        }
        
        if( !$response ){
            $response = array(
                'status'   => true
            );
        }else{
            $response['status'] = false;
        }
        
        die( json_encode($response) );

    }
    
    public function usuarioLogoffAction(){
        $User = User::online();
        if( $User ){
            $User->logout();
        }
        return true;
    }
    
    
    
    public function usuarioPermiteEditarPaginaAction( $paginaID = null ){

        $User = User::online();
        
        if( !$User || !$paginaID ){
            $response = array(
                'status' => false,
                'message' => 'Usuário offline'
            );
        }else{
            
            $Pagina = Pagina::getByID($paginaID);
            
            if( !$Pagina ){
                $response = array(
                    'status' => false,
                    'message' => 'A página informada é inválida'
                );
            }else{
                
                $grant = $Pagina->permiteLogadoAlteracao();

                if( $grant && _isGranted('/editor/modal-artigo/cadastro') ){
                    $response = array(
                        'status'  => true,
                        'message' => '',
                        'href'    => url."/editor/modal-artigo/cadastro/".$Pagina->getEditoriaID()."/".$Pagina->getID()
                    );
                }else{
                    $response = array(
                        'status' => false,
                        'message' => 'Sem permissão para editar esta página'
                    );
                }
                
            }
            
        }
        
        $response['message'] = utf8_encode($response['message']);
        
        die( json_encode($response) );
    }
    
}