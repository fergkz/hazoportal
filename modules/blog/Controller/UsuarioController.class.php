<?php

use MVC\Controller as Controller;

use Core\Model\User as User;
use Core\System\Mail as Mail;
use Model\Pagina\Pagina as Pagina;

use Lib\Validate as Validate;
use Lib\Format as Format;

class UsuarioController extends Controller{
    
    public function cadastroAction( $paginaID = null ){
        
        $response = false;
        
        if( !$_POST ){
            $response = array(
                'status' => false,
                'msg'    => 'Nenhum dado foi recebido. Aguarde alguns instantes e tente novamente'
            );
        }
        
        $email = trim($_POST['cad_email']); 
        $nome  = Format::formNomeProprio(trim($_POST['cad_nome']));
        
        if( !defined('group_blog') ){
            $response['msg'] = 'Nenhum grupo de usuários está disponível para cadastro';
        }elseif( !$email ){
            $response['msg'] = 'O e-mail deve ser informado';
        }elseif( !Validate::isEmail($email) ){
            $response['msg'] = 'O e-mail informado é inválido';
        }elseif( !$nome ){
            $response['msg'] = 'O nome deve ser informado';
        }elseif( !$_POST['cad_senha'] ){
            $response['msg'] = 'A senha deve ser informada';
        }elseif( $_POST['cad_senha'] !== $_POST['cad_rsenha'] ){
            $response['msg'] = 'As senhas informadas devem ser idênticas';
        }
        
        if( !$response ){
            $User = new User();
            $User->setEmail($email);
            $User->setLogin($email);
            $User->setGroupID( group_blog );
            $User->setName($nome);
            $User->setPassword($_POST['cad_senha']);
            $User->setRequireActivation(true);
            $User->setStatus("A");
            
            if( $User->save() ){
                
                $Mail = new Mail();
                $Mail->setAddress($email);
                
                $link = url."/blog/usuario/ativacao/".$User->getActivationCode()."/{$paginaID}";
                
                $Mail->setMessage("Confirmação de E-mail Comentários: <a href='{$link}'>{$link}</a>");
                
                $Mail->setSubject("Confirmação de cadastro de usuário Hazo");

                if( $Mail->send() ){
                    $response = array(
                        'status' => true,
                        'msg'    => 'Usuário cadastrado com sucesso. Ative o seu login com o link enviado no seu e-mail'
                    );
                }else{
                    $response = array(
                        'status' => false,
                        'msg'    => 'Não foi possível enviar o e-mail de confirmação. Aguarde alguns minutos e tente novamente'
                    );
                }
            }else{
                $response = array(
                    'status' => false,
                    'msg'    => implode("<br/>", _getErrors())
                );
                _clearErrors();
            }
        }else{
            $response['status'] = false;
        }
//        $response['msg'] = utf8_encode($response['msg']);
        
//        debug($response, 1);
//        debug($_POST);
        
        die( json_encode($response) );
    }
    
    public function ativacaoAction( $codigoAtivacao = null, $paginaID = null ){
     
        if( !$codigoAtivacao ){
            return 404;
        }
        
        $User = User::getByActivationCode($codigoAtivacao);
        
        if( !$User ){
            return 404;
        }
        
        $User->setActivationCode(null);
        
        $User->save();
        
        $User->setTransactionUser();
        
        if( $paginaID ){
            $Pagina = Pagina::getByID($paginaID);
            if( $Pagina ){
                
                $editorias = $Pagina->getArrayEditorias();
                
                $link = url."/artigo";
                
                foreach( $editorias as $Editoria ){
                    $link .= "/".$Editoria->getSlug();
                }
                
                $this->redirect($link."/".$Pagina->getSlug());
                
            }
        }
        
        $this->redirect(url);
        
    }
    
    public function loginAction( $paginaID = null ){
        
    }
    
    public function modalUsuarioLoginAction(){
        $this->view()->display();
    }
    
    public function modalUsuarioCadastroAction( $action = 'cadastro', $paginaID = null ){
        $render['paginaID'] = $paginaID;
        switch( $action ){
            case 'cadastro':
                $this->view()->display($render);
                break;
            case 'sucesso':
                $this->view()->setTemplate('usuario/modal-usuario-cadastro-sucesso.html')->display($render);
                break;
        }
    }
    
}