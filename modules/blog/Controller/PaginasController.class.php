<?php

use Model\Pagina\Pagina as Pagina;
use Model\Pagina\Editoria as Editoria;
use Model\Pagina\Comentario as Comentario;
use Lib\Format as Format;

class PaginasController extends \System\MyController
{
    public function artigoAction()
    {
//        $this->viewCategorias();exit;
        $view = $this->view()->setTemplate('paginas/artigo.twig')->cache(60);
//        $view = $this->view()->setTemplate('paginas/artigo.twig');
        
        $params = func_get_args();

        if( !$params || count($params) < 1 ){
            return 404;
        }

        $Pagina = $this->getPagina($params);

        if( !$Pagina || $Pagina->getStatus() !== 'A' || !$Pagina->getEditoriaObj() || $Pagina->getEditoriaObj()->getStatus() !== 'A' ){
            return 404;
        }

        $render['Pagina'] = $Pagina;

        if( defined('group_blog') ){
            $render['cad_usuario'] = true;
        }

        $render['parent_editorias'] = Editoria::getArrayParents($Pagina->getEditoriaObj());

        $render['top5'] = Pagina::getTopContador(5);
        
//        debug($render['parent_editorias']);
        
        $render['this'] =& $this;
        
        $view->display($render);
    }
    
    public function viewCategorias( $editoriaParent = null, $editoriaID = null )
    {
        $render['editoriaID'] = $editoriaID;
        
        $render['editorias'] = $this->listaEditorias( $editoriaParent );
        $render['top5'] = Pagina::getTopContador(5);
        
//        $this->view()->setTemplate('paginas/sidebar.twig')->display($render);
        $this->view()->setTemplate('paginas/sidebar.twig')->cache(60)->display($render);
    }
    
    public function listaEditorias( $editoriaParent = null )
    {
        $render = array();
        
        $editorias = Editoria::listaByParent($editoriaParent);
        
        if( $editorias ){
            foreach( $editorias as $Editoria ){
                $Editoria->filhos = $this->listaEditorias($Editoria->getID());
            }
        }else{
            $editorias = null;
        }
        return $editorias;
    }

    private function getPagina( $params, $parent = null )
    {

        $ind = array_shift($params);

        $Editoria = Editoria::getBySlug($ind);

        if( $Editoria && $parent && $Editoria->getParentEditoriaID() !== $parent->getID() ){
            return false;
        }

        if( !$Editoria && !empty($params[0]) ){
            return false;
        }elseif( !$Editoria ){
            $Pagina = Pagina::getBySlug($ind);
            if( !$Pagina ){
                return false;
            }else{
                if( $parent && $Pagina->getEditoriaID() !== $parent->getID() ){
                    return false;
                }
                return $Pagina;
            }
        }

        return $this->getPagina($params, $Editoria);
    }
    
    /*
    public function loadCategoriasAction( $editoriaParent = null )
    {
        $url_load = url."/blog/paginas/load-categorias";

        $EditoriaParent = Editoria::getByID($editoriaParent);

        $response = array(
            'categoriasAnteriores' => array(
                array(
                    'nome' => 'Todas as categorias',
                    'id' => null,
                    'url' => $url_load,
                    'subs' => true
                )
            )
        );

        $url_editoria = url."/artigos";

        if( $EditoriaParent ){

            $listaParents = Editoria::getArrayParents($editoriaParent);


            if( $listaParents && is_array($listaParents) && count($listaParents) > 0 ){

                foreach( $listaParents as $Editoria ){

                    if( $Editoria->getStatus() !== "A" ){
                        continue;
                    }

                    $Subs = $Editoria->getSubEditorias() ? true : false;
                    $url_editoria .= "/".$Editoria->getSlug();

//                    $qtde = $Editoria->listaPaginas(true, true);
                    $qtde = Pagina::listaTodasByEditoria($Editoria, true, array( 'somente_qtde' => true ));
                    if( $qtde !== 0 ){
                        $response['categoriasAnteriores'][] = array(
                            'nome' => $Editoria->getNome(),
                            'id' => $Editoria->getID(),
                            'url' => $url_load.'/'.$Editoria->getID(),
                            'subs' => $Subs,
                            'qtde' => $qtde
                        );
                    }
                }
            }

            $editorias = ($EditoriaParent) ? $EditoriaParent->getSubEditorias() : null;

            if( $editorias ){
                foreach( $editorias as $Editoria ){

                    if( $Editoria->getStatus() !== "A" ){
                        continue;
                    }

                    $Subs = $Editoria->getSubEditorias() ? true : false;

//                    $qtde = $Editoria->listaPaginas(true, true);
                    $qtde = Pagina::listaTodasByEditoria($Editoria, true, array( 'somente_qtde' => true ));

                    if( $qtde !== 0 ){
                        $response['rows'][$Editoria->getNome()] = array(
                            'id' => $Editoria->getID(),
                            'slug' => $Editoria->getSlug(),
                            'url' => $url_load.'/'.$Editoria->getID(),
                            'subs' => $Subs,
                            'url_redirect' => $url_editoria.( $EditoriaParent ? "/".$EditoriaParent->getSlug() : "" )."/".$Editoria->getSlug(),
                            'qtde' => $qtde
                        );
                    }
                }
            }

            $Subs = $EditoriaParent->getSubEditorias() ? true : false;

//            $qtde = $EditoriaParent->listaPaginas(true, true);
            $qtde = Pagina::listaTodasByEditoria($EditoriaParent, true, array( 'somente_qtde' => true ));

            if( $qtde !== 0 ){

                $response['categoriasAnteriores'][] = array(
                    'nome' => $EditoriaParent->getNome(),
                    'id' => $EditoriaParent->getID(),
                    'url' => $url_load.'/'.$EditoriaParent->getID(),
                    'subs' => $Subs,
                    'qtde' => $qtde
                );
            }
        }else{

            $editorias = Editoria::listaByParent();

            foreach( $editorias as $Editoria ){

                if( $Editoria->getStatus() !== "A" ){
                    continue;
                }

                $Subs = $Editoria->getSubEditorias() ? true : false;

//                $qtde = $Editoria->listaPaginas(true, true);
                $qtde = Pagina::listaTodasByEditoria($Editoria, true, array( 'somente_qtde' => true ));

                if( $qtde === 0 ){
                    continue;
                }

                $response['rows'][$Editoria->getNome()] = array(
                    'id' => $Editoria->getID(),
                    'slug' => $Editoria->getSlug(),
                    'url' => $url_load.'/'.$Editoria->getID(),
                    'subs' => $Subs,
                    'url_redirect' => $url_editoria."/".$Editoria->getSlug(),
                    'qtde' => $qtde
                );
            }
        }

        die(json_encode($response));
    }
    */
    
    public function artigosAction( $params = null )
    {
//        $view = $this->view()->cache(60)->setTemplate('paginas/lista.twig');
        $view = $this->view()->setTemplate('paginas/lista.twig');

        $params = func_get_args();

        $paramPaginacao = $render = array( );

        $paramPaginacao['pagina'] = empty($_GET['pag']) ? 1 : $_GET['pag'];
        $paramPaginacao['qtde_pag'] = '5';

        ## Paginacao

        if( $_GET && count($_GET) > 0 ){
            $uri = str_replace("pag=".(empty($_GET['pag']) ? '' : $_GET['pag']), "pag=", $_SERVER['REQUEST_URI']);
        }else{
            $uri = rtrim($_SERVER['REQUEST_URI'], "/")."?pag=";
        }

        $uri = "/".ltrim(str_replace(url_folder, "", $uri), "\\..\/");
        $render['link_paginas'] = $uri;

        ## endPaginacao

        if( $params && count($params) > 0 && !empty($params[0]) ){

            $cont = 0;
            $subEditorias = null;

            foreach( $params as $slugEditoria ){

                if( $cont === 0 ){
                    $Editoria = Editoria::getBySlug($slugEditoria);
                }elseif( !$subEditorias && count($params) !== $cont ){
                    return 404;
                }else{
                    $sub = null;
                    foreach( $subEditorias as $Editoria ){
                        if( $Editoria->getSlug() == $slugEditoria ){
                            $sub = $Editoria;
                        }
                    }
                    $Editoria = $sub;
                }
                $cont++;

                if( !$Editoria ){
                    return 404;
                }

                $subEditorias = $Editoria->getSubEditorias();
            }

//            $lista = $Editoria->listaPaginas(true, false, $paramPaginacao);
            $lista = Pagina::listaTodasByEditoria($Editoria, true, $paramPaginacao);

            $render['paginas'] = $lista['paginas'];
            $render['qtde_paginas'] = $lista['qtde_paginas'];

            $render['editoria_atual_id'] = $Editoria->getID();
            $render['editoria_atual_Obj'] = $Editoria;
        }else{
//            $lista = Pagina::listaByEditoria(null, true, $paramPaginacao, true);
            $lista = Pagina::listaTodasByEditoria(null, true, $paramPaginacao);
            $render['paginas'] = $lista['paginas'];
            $render['qtde_paginas'] = $lista['qtde_paginas'];
        }

//        $render['paginas'] = null;

        $render['top5'] = Pagina::getTopContador(5);
        
        $render['this'] =& $this;

        $view->display($render);
    }
    
    public function enviarComentarioAction( $paginaID = null )
    {

        if( !$paginaID ){
            return false;
        }

        $response = null;

        if( !$response ){

            $Comentario = new Comentario();

            $Comentario->setPaginaID($paginaID);
            $Comentario->setTexto($_POST['msg_texto']);
            $Comentario->setStatus("A");

            if( $Comentario->save() ){
                $response = array(
                    'status' => true,
                    'msg' => "Comentário enviado com sucesso. Aguarde a aprovação da moderação."
                );
            }else{
                $response = array(
                    'status' => false,
                    'msg' => implode("<br/>", _getErrors())
                );
                _clearErrors();
            }
        }else{
            $response['status'] = false;
        }

        die(json_encode($response));
    }
    
    public function carregaComentariosAction( $paginaID = null, $ultimoComentario = null )
    {
        if( !$paginaID ){
            return 404;
        }

        $comentarios = Comentario::listaComentariosAtivosByPagina($paginaID, null);

        $response = array( );

        $response['qtde'] = 0;

        if( $comentarios ){
            $response['status'] = true;
            $response['qtde'] = count($comentarios);
            foreach( $comentarios as $Comentario ){
                $response['rows'][$Comentario->getID()] = array(
                    'id' => $Comentario->getID(),
                    'data' => substr(Format::dataToBr($Comentario->getData()), 0, 16),
                    'autor_nome' => $Comentario->getUserObj()->getName(),
                    'msg' => $Comentario->getTexto(),
//                    'remove' => true
                );
            }
        }

        die(json_encode($response));
    }

}