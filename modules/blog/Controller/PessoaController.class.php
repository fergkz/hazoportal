<?php

use Model\Novo\Pessoa as Pessoa;

class PessoaController extends \MVC\Controller
{

    public function novoAction ($paginaID = null)
    {
        
        $Pessoa = new Pessoa();
        
//        $Pessoa->ID = 284;
//        if( $Pessoa->load(array('razao','fantasia')) ){
//            debug($Pessoa, 0, '$Pessoa');
//        }
//        debug($Pessoa, 0, 'Erro');
        
        $Pessoa->cnpj           = null;
        $Pessoa->cpf            = '49611222203';
        $Pessoa->dataNascimento = "2013-12-15";
        $Pessoa->fantasia       = "Pessoa 01 Apelido";
        $Pessoa->razao          = "Pessoa 01 Razão (novo)";
        $Pessoa->slug           = "slug-slug";
        
        $Pessoa->save();
        
        if( _getErrors() ){
            debug( _getErrors(), 1, 'Erro ao salvar Pessoa' );
            _clearErrors();
        }
        
        echo "<pre>";
        
        $lista = Pessoa::getList(array(
            'razao' => "%(novo)%"
        ), array(
            'ID', 'razao', 'cpf'
        ), 0, null, array(
            "ID desc"
        ));
        
        foreach( $lista['rows'] as $Obj ){
//            $Obj->delete();
        }
        
        $lista = Pessoa::getList(array(
            'razao' => "%(novo)%"
        ), array(
            'ID', 'razao', 'cpf'
        ), 0, null, array(
            "ID desc"
        ));
        
        print_r($lista);
        
//        _rollback();

    }

    public function pesquisaAction()
    {
        $this->view()->setTemplate("modules/blog/Controller/PessoaController.view.twig")->display();
    }

    public function pesquisaJsonAction()
    {
        $render = array(
            array( 'value' => 'teste', 'id' => '021' ),
        );
        
        die(json_encode($render) );
    }
    
}