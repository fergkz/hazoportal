<?php

namespace Lib\PDF\FPDI;

class FPDI{
    
    public $obj;
    
    function __construct($p1){
        include_once(PATH."/class/Lib/PDF/PFDI/fpdi.php");
        $this->obj = new \FPDI($p1);
        return $this->obj;
    }
    
}