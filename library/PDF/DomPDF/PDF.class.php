<?

namespace Lib\PDF\DomPDF;

require_once("class/Lib/PDF/DomPDF/dompdf_config.inc.php");

class PDF extends \DOMPDF{
    
    public function __construct(){
        $path = add_include_path("/class/Lib/PDF/DomPDF/include");
        spl_autoload_register("DOMPDF_autoload");
        parent::__construct();
        
    }
    
}