<?php

/**
 * Retirado de http://www.paulfp.net/blog/2010/10/how-to-add-and-upload-an-image-using-ckeditor/
 * 
 * CKEditor: http://ckeditor.com/
 */

namespace Lib\CKEditor;

class Upload{

    public static function uploadImagem( $filepath = null ){

        $filepath = trim(str_replace(path, "", $filepath), "\\..\/").ds.date("Y").ds.date("m").ds.date("d");

        @$filename = path.ds.str_replace(array( "\\", "/" ), ds, $filepath).ds.$_FILES['upload']['name'];
        @$fileurl  = url.'/'.str_replace(array( "\\", "/" ), "/", $filepath).'/'.$_FILES['upload']['name'];

        $message = null;
        if( ($_FILES['upload'] == 'none') or (empty($_FILES['upload']['name'])) ){
            $message = "O arquivo não foi informado.";
        }elseif( $_FILES['upload']["size"] == 0 ){
            $message = "O tamanho do arquivo é zero.";
        }elseif( !in_array($_FILES['upload']["type"], array("image/pjpeg","image/jpeg","image/png","image/gif")) ){
            $message = "A imagem informada deve ser JPG, JPEG, PNG OU GIF. Por favor, informa uma destas.";
        }elseif( !is_uploaded_file($_FILES['upload']["tmp_name"]) ){
            $message = "Você pode estar tentando hackear nosso servidor.";
        }else{
            $message = "";
            $tmp = explode(ds,str_replace(array("\\","/"),ds, $filepath));
            if( $tmp and count($tmp) > 0 ){
                $route = path;
                foreach( $tmp as $row ){
                    $route .= ds.$row;
                    if( !file_exists($route) ){
                        mkdir($route);
                    }
                }   
            }
            $move = @ move_uploaded_file($_FILES['upload']['tmp_name'], $filename);
            if( !$move ){
                $message = "Erro ao mover arquivo. Verifique as permissões de pastas.($filename)";
            }
        }
        if( $message ){
            $fileurl = "";
        }
        $funcNum = $_GET['CKEditorFuncNum'];
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$fileurl', '$message');</script>";
        exit;
    }

}
