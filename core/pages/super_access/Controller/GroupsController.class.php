<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\Model\Group as Group;

class GroupsController extends Controller{
    
    public function indexAction(){
        $render['groups'] = Group::listAll();
        $Twig = new Twig();
        $Twig->declareFunction("_getModeDescr");
        $Template = $Twig->loadTemplate("groups/index.html");
        echo $Template->render($render);
    }
    
    public function changeAction(){
        $Group = Group::getByID(@$_GET['id']);
        if( !$Group ){
            $Group = new Group();
        }
        if( @$_POST ){
            $Group->setName($_POST['name']);
            $Group->setMode($_POST['mode']);
            $Group->setStatus($_POST['status']);
            if( $Group->save() ){
                _commit();
                header("Location: ".url."/s/groups");
                exit;
            }
        }
        $Twig = new Twig();
        $Twig->declareFunction("_getModeDescr");
        $Template = $Twig->loadTemplate("groups/change.html");
        $render['Group'] = $Group;
        $render['modes'] = _getModeList();
        echo $Template->render($render);
    }

}