<?php

namespace Core\System;

class Functions{
/*
    public static function crypt( $variable, $password = null, $length = 0 ){
        $password = $password ? $password : $GLOBALS['config']['password']['crypt'];
        $variable .= "\x13";
        $n = strlen($variable);
        if( $n % 16 ){
            $variable .= str_repeat("\0", 16 - ($n % 16));
        }
        $i = 0;
        $enc_text = '';
        while( $length-- > 0 ){
            $enc_text .= chr(mt_rand() & 0xff);
        }
        $iv = substr($password ^ $enc_text, 0, 512);
        while( $i < $n ){
            $block = substr($variable, $i, 16) ^ pack('H*', md5($iv));
            $enc_text .= $block;
            $iv = substr($block.$iv, 0, 512) ^ $password;
            $i += 16;
        }
        $variable = base64_encode($enc_text);
        $hex = '';
        for( $i = 0; $i < strlen($variable); $i++ ){
            $hex .= dechex(ord($variable[$i]));
        }
        return $hex;
    }

    public static function decrypt( $variable, $password = null, $length = 0 ){
        $password = $password ? $password : $GLOBALS['config']['password']['crypt'];
        $string = '';
        for( $i = 0; $i < strlen($variable) - 1; $i+=2 ){
            $string .= chr(hexdec($variable[$i].$variable[$i + 1]));
        }
        $variable = base64_decode($string);
        $n = strlen($variable);
        $i = $length;
        $text = '';
        $iv = substr($password ^ substr($variable, 0, $length), 0, 512);
        while( $i < $n ){
            $block = substr($variable, $i, 16);
            $text .= $block ^ pack('H*', md5($iv));
            $iv = substr($block.$iv, 0, 512) ^ $password;
            $i += 16;
        }
        $text = preg_replace('/\\x13\\x00*$/', '', $text);
        return $text;
    }

    public static function cryptSerialize( $variable, $password = null, $length = 0 ){
        $variable = serialize($variable);
        return self::crypt($variable, $password, $length);
    }

    public static function decryptSerialize( $variable, $password = null, $length = 0 ){
        return unserialize(self::decrypt($variable, $password, $length));
    }*/
    
    public static function cryptSerialize( $variable, $password = null ){
        $variable = serialize($variable);
        return self::crypt($variable, $password);
    }

    public static function decryptSerialize( $variable, $password = null ){
        return unserialize(self::decrypt($variable, $password));
    }

    public static function crypt( $sData, $secretKey = null, $length = 0 ){
        $secretKey = $secretKey ? $secretKey : $GLOBALS['config']['password']['crypt'];
        if( $length > 0 ){
            for( $i=0; $i<$length; $i++ ){
                $sData .= rand(1,9);
            }
        }
        if( strlen($secretKey) < 1 ){ $secretKey = "defaultpass"; }
        
        $sResult = '';
        for( $i = 0; $i < strlen($sData); $i++ ){
            $sChar = substr($sData, $i, 1);
            $sKeyChar = substr($secretKey, ($i % strlen($secretKey)) - 1, 1);
            $sChar = chr(ord($sChar) + ord($sKeyChar) + $length);
            $sResult .= $sChar;
        }
        return self::encodeBase64($sResult);
    }

    public static function decrypt( $sData, $secretKey = null, $length = 0 ){
        $secretKey = $secretKey ? $secretKey : $GLOBALS['config']['password']['crypt'];
        if( strlen($secretKey) < 1 ){ $secretKey = "defaultpass"; }
        $sResult = '';
        $sData = self::decodeBase64($sData);
        for( $i = 0; $i < strlen($sData); $i++ ){
            $sChar = substr($sData, $i, 1);
            $sKeyChar = substr($secretKey, ($i % strlen($secretKey)) - 1, 1);
            $sChar = chr(ord($sChar) - ord($sKeyChar) - $length);
            $sResult .= $sChar;
        }
        return substr($sResult, 0, strlen($sResult)-$length);
    }

    private static function encodeBase64( $sData ){
        $sBase64 = base64_encode($sData);
        return str_replace('=', '', strtr($sBase64, '+/', '-_'));
    }

    private static function decodeBase64( $sData ){
        $sBase64 = strtr($sData, '-_', '+/');
        return base64_decode($sBase64.'==');
    }

}