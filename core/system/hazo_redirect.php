<?php

function _coalesce( $variable = null, $value = null ){
    return isset($variable) && $variable ? $variable : $value;
}

function _stringToCamelMVC( $string ){
    return preg_replace_callback("(-.{1})", create_function('$a', 'return str_replace("-","",strtoupper($a[0]));'), $string);
}

function _getParamsForUrl( $url ){
    $urlRequisitado = ltrim($url, "\\..\/");
    $tmp = explode('/', trim($urlRequisitado));
    
//    debug($GLOBALS['config']['directories']['mode']);
    
//    foreach( $GLOBALS['config']['routes'] as $index => $value ){
    foreach( $GLOBALS['config']['directories']['mode'] as $index => $value ){
        
        $index = ltrim($index, "\\..\/");
        
        if( strpos($index, "/") && strtolower(substr($urlRequisitado, 0, strlen($index) )) === strtolower($index) ){
            
            if( !file_exists( str_replace(array("\\", "/"), ds, path.ds.$value['path']) ) ){
                continue;
            }
            
            $mode = trim(strtolower($index), "\\..\/");
            
            $tmp = explode('/', trim(substr($urlRequisitado, strlen($index)+1)));
//            @array_shift($tmp)
        }
    }
    
//    debug($tmp);
    
    foreach( $tmp as $i => $r ){
        if( stristr($r, "?") ){
            $r = explode("?", $r);
            $new[] = $r[0];
            break;
        }
        $new[] = $r;
    }
//    debug($new);
    
    if( !isset($mode) ){
        $mode = _isMode(strtoupper($new[0])) ? strtoupper($new[0]) : "F";

        if( _isMode(strtoupper($new[0])) ){
            @array_shift($new);
        }
    }
    
    if( @$new[0] == "$" ){
        $module    = 'index';
        $submodule = "index";
    }else{
        $module = @_coalesce($new[0], "index");
        @array_shift($new);
        if( @$new[0] != "$" ){
            $submodule = @_coalesce($new[0], "index");
            @array_shift($new);
        }else{
            $submodule = "index";
        }
    }
    if( @$new[0] !== "$" ){
        @$new = array_merge( array("$"), $new );
    }
    return array( "mode" => $mode, "module" => $module, "submodule" => $submodule, "cont" => @$cont, "new" => $new );
}

function _includeError( $code ){
    switch( $code ){
        case 307:
            $msg = "Moved Permanently";
            break;
        case 404:
            $msg = "Page not found";
            break;
        case 403:
            $msg = "You not granted to access this page";
            break;
        case "sqlConnect":
            $msg = "Not connected to the database";
            break;
        default:
            $msg = "Page not found";
            break;
    }
    $method = "error{$code}";
    if( file_exists(_getModePath("ERROR")."/Controller/ErrorController.class.php") ){
        include_once(_getModePath("ERROR")."/Controller/ErrorController.class.php");
        if( class_exists("ErrorController") ){
            $Class = new ErrorController();
            $method = $method."Action";
            if( method_exists($Class, $method) ){
                $Class->$method();
                exit;
            }
        }
    }

    echo "<center><br/><br/>$msg</center>";
    exit;
}

function _isGranted( $url = null, $groupID = null ){
    if( !$url ){
        $url = $GLOBALS['url']['request'];
    }
    if( substr($url, 0, 1) === "/" ){
        $url = substr($url, 1);
    }

    $params = _getParamsForUrl($url);
//    debug($params);

    $mode = $params['mode'];
    $module = $params['module'];
    $submodule = $params['submodule'];

    $Module = new Core\System\Module();
    $Module->setMode($mode);
    $Module->setModule($module);
    $Module->load();
    $Submodule = &$Module->submodules[$submodule];

    $page = array(
        "Mode" => _getModeDescr($mode),
        "Module" => $Module,
        "Submodule" => $Submodule
    );
    if( !_isModeRestrict($mode) ){
        return $page;
    }

    $online = Core\Model\User::online();

    if( !$online ){
        return false;
    }

    if( $online->getGroupObj()->getMode() == "S" ){
        return $page;
    }

    $levelOnline = _getModeLevel($online->getGroupObj()->getMode());

    if( _getModeLevel($mode) > $levelOnline ){
        return false;
    }

    if( !$Module || $Module->status !== "O" || !$Submodule || $Submodule->status !== "O" ){
        return false;
    }

    if( $Submodule->grantRequire !== "Y" ){
        return $page;
    }

    if( Core\Model\Permission::groupIsGranted($online->getGroupID(), $mode, $module, $submodule) ){
        return $page;
    }else{
        return false;
    }
}

if( isset($GLOBALS['config']['define']['url_folder']) && $GLOBALS['config']['define']['url_folder'] ){
    $url['request'] = substr(ltrim($_SERVER['REQUEST_URI'], "\\..\/"), strlen($GLOBALS['config']['define']['url_folder']));
}else{
    $url['request'] = ltrim($_SERVER['REQUEST_URI'], "\\..\/");
}
$url['request'] = "/".ltrim($url['request'], "\\..\/");

if( !empty($GLOBALS['config']['routes']) ){
    foreach( $GLOBALS['config']['routes'] as $url['route']['index'] => $url['route']['value'] ){
        $url['route']['index'] = $url['route']['index'];
        
        $url['tmp'] = "";
        if( substr($url['request'], 0, strlen($url['route']['index']) ) === $url['route']['index'] ){
            $url['tmp'] = substr($url['request'], strlen($url['route']['index']) );
            
//            $url['request'] = $url['route']['index'];
            $url['request'] = $url['route']['value'];
            //debug($url,1);
            if( $url['tmp'] && strlen($url['tmp']) > 0 ){
                $url['request'] = rtrim($url['request'], "\\..\/")."/$/". ( substr($url['tmp'],0,1) == "/" ? substr($url['tmp'],1) : $url['tmp'] );
                //debug($url,1);
            }
        }
    }
    unset($url['routes']);
}
//debug($url);

$url['tmp'] = _getParamsForUrl($url['request']);

if( $url['tmp']['mode'] == "F" && $url['tmp']['module'] == "logout" ){
    Core\Model\User::logout();
    header("Location: ".url."");
    exit;
}

$GLOBALS['request']['mode'] = $url['tmp']['mode'];
$GLOBALS['request']['module'] = $url['tmp']['module'];
$GLOBALS['request']['submodule'] = $url['tmp']['submodule'];
$url['cont'] = $url['tmp']['cont'];
$url['new'] = $url['tmp']['new'];
//debug($url['new']);

$url['set_next_params'] = false;
for( $url['i'] = $url['cont']; $url['i'] < count($url['new']); $url['i']++ ){
    if( isset($url['new'][$url['i']]) ){
        if( stristr($url['new'][$url['i']], "?") ){
            echo "asdfasdf";
            $url['new'][$url['i']] = explode("?", $url['new'][$url['i']]);
            $url['new'][$url['i']] = $url['new'][$url['i']][0];
        }
        $GLOBALS['request']['url_params'][] = @$url['new'][$url['i']];
    }
}

/* * *** AUTO-LOGIN in "__hazo__login__" **** */
if( $_POST && isset($_POST['__hazo__login__']) ){
    if( @!Core\Model\User::login($_POST['login'], $_POST['password']) ){
        _setError("hz_user_login_incorrect");
    }else{
        if( $GLOBALS['request']['mode'] == "F" ){
            if( Core\Model\User::online()->getGroupObj()->getDefaultUrl() ){
                header("Location: ".url.Core\Model\User::online()->getGroupObj()->getDefaultUrl());
            }else{
                header("Location: ".url."/".strtolower(Core\Model\User::online()->getGroupObj()->getMode()));
            }
            exit;
        }
    }
}

$GLOBALS['page'] = _isGranted();
if( !$GLOBALS['page'] ){
    _includeError(403);
}

$url['modePath'] = trim(str_replace(PATH, "", _getModePath($GLOBALS['request']['mode'])), "\\..\/");
$url['expl_mode_path'] = @explode("/", implode("/", explode("\\", $url['modePath'])));
$url['loaded'] = PATH;
foreach( $url['expl_mode_path'] as $url['tmp'] ){
    $url['loaded'] .= ds.$url['tmp'];
    if( file_exists($url['loaded'].ds."config.php") ){
        include_once($url['loaded'].ds."config.php");
    }
}

unset($url);
/* Treatment to mvc model */

$MVCCONTROLL['modePath'] = _getModePath($GLOBALS['request']['mode']);
$MVCCONTROLL['classControllerName'] = ucfirst(_stringToCamelMVC($GLOBALS['request']['module']))."Controller";
$MVCCONTROLL['classControlerFileName'] = "{$MVCCONTROLL['modePath']}/Controller/{$MVCCONTROLL['classControllerName']}.class.php";

if( !defined("MODE") ){
    define('MODE', $GLOBALS['request']['mode']);
    define('mode', MODE);
    define('URL_MODE', URL."/".strtolower(MODE));
    define('url_mode', URL_MODE);
}

if( !defined("MODULE") ){
    define('MODULE', $GLOBALS['request']['module']);
    define('module', MODULE);
    define('URL_MODULE', URL_MODE."/".MODULE);
    define('url_module', URL_MODULE);
}

if( !defined("SUBMODULE") ){
    define('SUBMODULE', $GLOBALS['request']['submodule']);
    define('submodule', SUBMODULE);
    define('URL_SUBMODULE', URL_MODULE."/".SUBMODULE);
    define('url_submodule', URL_SUBMODULE);
}

if( !defined("PATH_MODE") ){
    define('PATH_MODE', PATH.DS.$GLOBALS['request']['mode']);
    define('path_mode', PATH_MODE);
}

add_include_path(str_replace(array( "\\", "/" ), DS, _getModePath(MODE).DS."Controller"));

if( file_exists($MVCCONTROLL['classControlerFileName']) ){
    include_once($MVCCONTROLL['classControlerFileName']);
    if( class_exists($MVCCONTROLL['classControllerName']) ){
        $MVCCONTROLL['Controller'] = new $MVCCONTROLL['classControllerName']();

        $MVCCONTROLL['methodController'] = _stringToCamelMVC($GLOBALS['request']['submodule'])."Action";

//        function _getMinMaxParam($classname, $methodname){
//            if( class_exists("ReflectionMethod") ){
//                $method = new ReflectionMethod($classname, $methodname);
//                $params = array_reverse($method->getParameters());
//                $cont = count($params);
//                if( $cont && $cont > 0 ){
//                    $min = $cont;
//                    foreach( $params as $row ){
//                        $cont--;
//                        if( $row->isOptional() ){
//                            $min = $cont;
//                        }else{
//                            break;
//                        }
//                    }
//                    return array(
//                        "min" => $min,
//                        "max" => count($params)
//                    );
//                }
//            }
//            return array(
//                "min" => 0,
//                "max" => 0
//            );
//        }
        
        if( method_exists($MVCCONTROLL['Controller'], $MVCCONTROLL['methodController']) ){
//            $MVCCONTROLL['MIN_MAX'] = _getMinMaxParam($MVCCONTROLL['classControllerName'], $MVCCONTROLL['methodController']);
//            $MVCCONTROLL['param_min'] = $MVCCONTROLL['MIN_MAX'] ? $MVCCONTROLL['MIN_MAX']['min'] : 0;
//            $MVCCONTROLL['param_max'] = $MVCCONTROLL['MIN_MAX'] ? $MVCCONTROLL['MIN_MAX']['max'] : 0;
            
//            if( @count($GLOBALS['request']['url_params']) < $MVCCONTROLL['param_min'] ){
//                $MVCCONTROLL['returnController'] = 404;
//            }elseif( @count($GLOBALS['request']['url_params']) > $MVCCONTROLL['param_max'] ){
//                $MVCCONTROLL['returnController'] = 404;
//            }else{
                if( @$GLOBALS['request']['url_params'] && count($GLOBALS['request']['url_params']) > 0 ){
                    $MVCCONTROLL['returnController'] = call_user_func_array(array( $MVCCONTROLL['Controller'], $MVCCONTROLL['methodController'] ), $GLOBALS['request']['url_params']);
                }else{
                    $MVCCONTROLL['returnController'] = $MVCCONTROLL['Controller']->$MVCCONTROLL['methodController']();
                }
//            }
        }elseif( !$GLOBALS['request']['submodule'] && method_exists($MVCCONTROLL['Controller'], "indexAction") ){
//            $MVCCONTROLL['MIN_MAX'] = _getMinMaxParam($MVCCONTROLL['classControllerName'], "indexAction");
//            $MVCCONTROLL['param_min'] = $MVCCONTROLL['MIN_MAX'] ? $MVCCONTROLL['MIN_MAX']['min'] : 0;
//            $MVCCONTROLL['param_max'] = $MVCCONTROLL['MIN_MAX'] ? $MVCCONTROLL['MIN_MAX']['max'] : 0;
            
//            if( @count($GLOBALS['request']['url_params']) < $MVCCONTROLL['param_min'] ){
//                $MVCCONTROLL['returnController'] = 404;
//            }elseif( @count($GLOBALS['request']['url_params']) > $MVCCONTROLL['param_max'] ){
//                $MVCCONTROLL['returnController'] = 404;
//            }else{
                if( @$GLOBALS['request']['url_params'] && count($GLOBALS['request']['url_params']) > 0 ){
                    $MVCCONTROLL['returnController'] = call_user_func_array(array( $MVCCONTROLL['Controller'], "indexAction" ), $GLOBALS['request']['url_params']);
                }else{
                    $MVCCONTROLL['returnController'] = $MVCCONTROLL['Controller']->indexAction();
                }
//            }
        }else{
            $MVCCONTROLL['returnController'] = 404;
        }
        if( $MVCCONTROLL['returnController'] === 404 || $MVCCONTROLL['returnController'] === false ){
            _includeError(404);
        }elseif( $MVCCONTROLL['returnController'] && is_numeric($MVCCONTROLL['returnController']) ){
            _includeError($MVCCONTROLL['returnController']);
        }
    }else{
        _includeError(404);
    }
}else{
    _includeError(404);
}