<?php

namespace Core\Model;

use MVC\Model as Model;
use Core\System\Functions as Functions;
use MVC\Session as Session;

class User extends Model{

    protected $ID;
    protected $groupID;
    protected $groupObj;
    protected $login;
    protected $password;
    protected $name;
    protected $status;
    protected $email;
    protected $dateLastAccess;
    protected $activationCode;

    protected $imageName;
    protected $imageContent;
    protected $imageSize;
    protected $imageType;
    protected $imageFilename;

    protected $requiresActivation = false;
    protected $expirationDate = false;

    private static $cryptKey = "hzDecode";

    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setGroupID( $groupID ){
        $this->groupID = $groupID;
        return $this;
    }

    public function getGroupID(){
        return $this->groupID;
    }

    public function setGroupObj( $groupObj ){
        if( is_object($groupObj) ){
            $this->groupID = $groupObj->getID();
        }
        $this->groupObj = $groupObj;
        return $this;
    }

    public function getGroupObj(){
        if( !$this->groupObj ){
            $this->groupObj = Group::getByID($this->groupID);
        }
        return $this->groupObj;
    }

    public function setLogin( $login ){
        $this->login = $login;
        return $this;
    }

    public function getLogin(){
        return $this->login;
    }

    public function setPassword( $password ){
        $this->password = $password;
        return $this;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setName( $name ){
        $this->name = $name;
        return $this;
    }

    public function getName(){
        return $this->name;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setEmail( $email ){
        $this->email = $email;
        return $this;
    }

    public function getEmail(){
        return $this->email;
    }
    
    public function setDateLastAccess( $dateLastAccess ){
        $this->dateLastAccess = $dateLastAccess;
        return $this;
    }

    public function getDateLastAccess(){
        return $this->dateLastAccess;
    }
    
    public function setActivationCode( $activationCode ){
        $this->activationCode = $activationCode;
        return $this;
    }

    public function getActivationCode(){
        return $this->activationCode;
    }
    
    public function setImageName( $imageName ){
        $this->imageName = $imageName;
        return $this;
    }

    public function getImageName(){
        return $this->imageName;
    }

    public function setImageContent( $imageContent ){
        $this->imageContent = $imageContent;
        return $this;
    }

    public function getImageContent(){
        return $this->imageContent;
    }

    public function setImageSize( $imageSize ){
        $this->imageSize = $imageSize;
        return $this;
    }

    public function getImageSize(){
        return $this->imageSize;
    }

    public function setImageType( $imageType ){
        $this->imageType = $imageType;
        return $this;
    }

    public function getImageType(){
        return $this->imageType;
    }

    public function setImageFilename( $imageFilename ){
        $this->imageFilename = $imageFilename;
        return $this;
    }

    public function getImageFilename(){
        return $this->imageFilename;
    }
    
    public function setRequireActivation( $situation ){
        $this->requiresActivation = $situation;
        return $this;
    }
    
    public function setExpirationDate( $expirationDate ){
        $this->expirationDate = $expirationDate;
        return $this;
    }
    
    public static function login($login, $password){
        self::logout();
        $password = md5($password);
        
        $sql = "select u.id from hazo_user u
                  join hazo_group g on g.id = u.group_id
                 where u.login = :login
                   and u.password = :password
                   and u.status = 'A'
                   and g.status = 'A' ";
        $bind['login'] = $login;
        $bind['password'] = $password;
        $res = _query($sql, $bind);
        
        if( @$res[0]['id'] ){
            $User = User::getByID($res[0]['id']);
            $User->setImageContent(null);
            $User->setImageSize(null);
            $User->setImageType(null);
            $User->setImageName(null);
            $User->setTransactionUser();
            return $User;
        }
        return false;
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( @!preg_match('/^[a-f0-9]{32}$/', $this->new->password) ){
            $this->new->password = md5($this->new->password);
        }
        $this->activationCode = null;
    }
    
    protected function triggerAfterInsertUpdate(){
        if( $this->requiresActivation ){
            $this->activationCode = $this->generateActivationCode($this->ID);
            $this->requiresActivation = false;
            $this->save();
        }
        if( @$GLOBALS['config']['files']['path_img_users'] ){
            $path = rtrim($GLOBALS['config']['files']['path_img_users'], "\\..\/").ds;
            if( $this->imageContent ){
                foreach( glob(path.ds.$path.$this->ID.'_*') as $row ){
                    unlink($row);
                }
                $filename = $path.$this->ID."_".$this->imageName;
                if( !file_put_contents(path.ds.$filename, $this->imageContent) ){
                    _raise("hz_user_unable_save_img");
                }else{
                    chmod(path.ds.$filename, 0777);
                    $this->imageFilename = $filename;
                    $this->imageContent = null;
                    $this->imageName = null;
                    $this->imageSize = null;
                    $this->imageType = null;
                    $this->save();
                }
            }
            if( !$this->imageFilename ){
                foreach( glob(path.ds.$path.$this->ID.'_*') as $row ){
                    unlink($row);
                }
            }
        }elseif( $this->imageContent or $this->imageFilename ){
            _raise("hz_user_not_def_img_path");
        }
    }
    
    private function generateActivationCode( $id = null ){
        $data = array();
        $data["id"] = $id ? $id : $this->ID;
        if( $this->expirationDate ){
            $data['expires'] = $this->expirationDate;
        }
        return Functions::cryptSerialize($data, self::$cryptKey);
    }
    
    public static function getByActivationCode( $activationCode ){
        if( !$activationCode ){
            return false;
        }
        $data = Functions::decryptSerialize($activationCode, self::$cryptKey);
        if( !is_array($data) || !isset($data['id']) || !$data['id'] ){
            return false;
        }
        if( isset($data['expires']) && $data['expires'] ){
            if( $data['expires'] > date('Y-m-d H:i:s') ){
                _setError("hz_user_validate_exp");
                return false;
            }
        }
        $User = self::getByID($data['id']);
        if( isset($User) && $User ){
            if( $User->getID() !== $data['id'] || $User->getActivationCode() !== $activationCode ){
                return false;
            }
            return $User;
        }
        return false;
    }
    
    public static function logout(){
        return Session::destroy();
    }
        
    public function setTransactionUser(){
        self::logout();
        $this->getGroupObj();
        Session::set("transaction_user", $this);
        $this->dateLastAccess = date("Y-m-d H:i:s");
        $this->save();
        return $this;
    }
    
    private static function getTransactionUser(){
        return Session::get("transaction_user");
    }
    
    public static function online(){
        return self::getTransactionUser();
    }
    
    public static function listByGroup($Group){
        $bind['group'] = is_object($Group) ? $Group->getID() : $Group;
        $sql = "select u.id 
                  from hazo_user u 
                  join hazo_group g on g.id = u.group_id 
                 where u.group_id = :group and g.mode <> 'S'";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( @$res as $row ){
            $dados[] = User::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : false;
    }
    
}