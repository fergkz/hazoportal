<?php

namespace System;

class Group extends MyModel{

    static $daoTable = "hazo_group";
    static $daoPrimary = array('ID' => 'id');
    static $daoCols = array(
        'ID' => 'id',
        'name' => 'name',
        'mode' => 'mode',
        'status' => 'status',
        'defaultPage' => 'default_page',
    );
    
    public $ID;
    public $name;
    public $mode;
    public $status;
    public $defaultPage;
    
    public $defaultUrl;
    
    
}