

function carregaComentarios(last_comment) {
    jQuery.ajax({
        url: hazoParam.url+"/blog/paginas/carrega-comentarios/"+hazoParam.paginaID,
        dataType: 'json',
        type: 'post',
        data: {'last_comment': last_comment},
        success: function(data) {

            jQuery(".commentlist").data('qtde-rows', data.qtde);
//            console.log('data.qtde', data.qtde);

            if (data.qtde > 0) {
                jQuery(".no-comments").hide();
                jQuery(".qtde-comentarios").html(data.qtde + " comentário" + (data.qtde > 1 ? 's' : ''));

                jQuery.each(data.rows, function(i, item) {
                    if (!jQuery(".comment-text-" + item.id).length && !item.remove) {
                        
                        li = jQuery("<li class='comment even thread-even depth-1 comment-li-" + item.id+" '></li>").attr('data-id', item.id);
                        
                        dvComment = jQuery("<div class='comment-body clearfix'></div>").appendTo(li);
                        jQuery('<div class="comment-author vcard">'+item.autor_nome+'</div>').appendTo(dvComment);
                        
                        div = jQuery('<div class="comment-meta commentmetadata" style="margin-left:0;"></div>');
                        jQuery('<span class="comment-date">'+item.data+'</span>').appendTo(div);
                        jQuery(div).appendTo(dvComment);
                        
                        dvText = jQuery('<div class="comment-inner"></div>');
                        pText = jQuery("<p/>").html(item.msg).addClass("comment-text-" + item.id).appendTo(dvText);
                        jQuery(dvComment).append(dvText);
//                        li.append(p).hide().fadeIn();

                        jQuery(".commentlist").prepend(li);
                        
                    } else if (item.remove) {
                        jQuery(".comment-li-" + item.id).remove();
                    } else if (item.msg) {
                        jQuery(".comment-text-" + item.id).html(item.msg);
                    }

                    if (!jQuery(".commentlist").data('first-id')) {
                        jQuery(".commentlist").data('first-id', item.id);
                    }

                });
            } else {
                
                jQuery(".no-comments").show();
                jQuery(".qtde-comentarios").html("Nenhum comentário");
            }

            if (jQuery(".commentlist").data('qtde-rows') > 5) {
                for (i = 0; i < jQuery(".commentlist").data('qtde-rows'); i++) {
                    li = jQuery('.commentlist').find('li')[i];
                    if (i < 5) {
                        jQuery(li).addClass('exploded');
                    }
                    if (li && !jQuery(li).hasClass('exploded') && i >= 5) {
                        jQuery(li).hide();
                    }
                }
            }

            if (data.qtde > 5 && jQuery(".exploded").length <= 5) {
                jQuery(".comments-mais-comentarios").show();
            }

            setTimeout(function() {
                carregaComentarios();
            }, 30000);
        }
    });
}

jQuery().ready(function() {
    carregaComentarios();

    jQuery(".comments-mais-comentarios").on('click', function() {
        console.log('clicked');
        cont = 0;
        for (i = 5; i < jQuery(".commentlist").data('qtde-rows'); i++) {
            li = jQuery('.commentlist').find('li')[i];
            if (li && !jQuery(li).hasClass('exploded')) {

                if (cont > 4) {
                    break;
                }

                if (jQuery(li).attr('data-id') === jQuery(".commentlist").data('first-id')) {
                    jQuery(".comments-mais-comentarios").hide();
                }

                jQuery(li).fadeIn().addClass('exploded');
                jQuery(".commentlist").data('last-exploded', jQuery(li).attr('data-id'));
                cont++;
            }
        }
    });
    
    if( hazoParam.online ){
        jQuery('.usuario-online').show();
        jQuery('.usuario-offline').hide();
    }else{
        jQuery('.usuario-online').hide();
        jQuery('.usuario-offline').show();
    }
    
    jQuery("#commentform").ajaxForm({
        dataType: 'json',
        success: function( data ){
            if( data.status ){
                jQuery("#commentform").each(function(){
                    this.reset();
                });
                
                jQuery("#commentform .error").append('<span style="color:green"><br/>'+data.msg+'</span>');
            }else{
                jQuery("#commentform .error").append('<span style="color:red"><br/>'+data.msg+'</span>');
            }
            setTimeout(function(){
                jQuery("#commentform span").first().remove();
            }, 3000);
            carregaComentarios();
        }
    });
    
    
});