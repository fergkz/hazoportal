function lgLoad( status, msg ){
    if( status ){
        $('#frmlg').find('input').attr('readonly', true);
        $('#frmlg').find('.msg').text('Verificando. Aguarde...').css('color','#3071a9').show();
    }else{
        $('#frmlg').find('input').attr('readonly', false);
        $('#frmlg').find('.msg').hide();
    }
    if( msg ){
        $('#frmlg').find('input').attr('readonly', false);
        $('#frmlg').find('.msg').css('color','red').text(msg).show();
    }
}

function usuLog( status, data ){
    if( status ){
        $(".lg-on").show();
        $(".lg-on").find('.name').html(data.name);
        $(".lg-on").find('.link_pannel').attr('href',data.link_pannel);
        $(".lg-off").hide();
        
        jQuery('.usuario-online').show();
        jQuery('.usuario-offline').hide();
    }else{
        $(".lg-on").hide();
        $(".lg-off").show();
        jQuery('.usuario-online').hide();
        jQuery('.usuario-offline').show();
    }
}

jQuery().ready(function(){
    jQuery('.btn-login').bind('click', function(e){
        e.preventDefault();
        $.fancybox('#frmlg');
    });
    
    $("#frmlg").find("form").ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            lgLoad(true);
        },
        success: function( data ){
    
            if( data.status ){
                lgLoad(false);
                $.fancybox.close();
//                usuLog(true, {
//                    name: data.name,
//                    link_pannel: data.link_pannel
//                });
                window.location = hazoParam.url+"/u";
            }else{
                lgLoad(false, data.msg);
            }
    
        }
    });
    
    $(".logoff").bind('click', function(e){
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: hazoParam.url+"/session/sair",
            success: function(){
                usuLog(false);
            }
        });
    });
    
});