
function loadToolbarOnline( usuario ){
    jQuery(".toolbar-online").show();
    jQuery(".botao-login").hide();

    jQuery(".toolbar-online").find(".nome").html( usuario.nome );
    jQuery(".usuario-nome").html( usuario.nome );
    jQuery(".usuario-painel-href").attr( 'href', hazoUrlPath+"/"+usuario.group_mode );
    
    jQuery(".toolbar-online").find(".sair").click(function(){
        usuarioLogout();
    });
    
    if( usuario.permite_criar_artigo ){
        jQuery(".toolbar-li-criar-artigo").css('display', 'inline');
    }
    
    jQuery(".usuario-offline").hide();
    jQuery(".usuario-online").show();
    
    if( typeof hazoPaginaAtual !== 'undefined' ){
        usuarioPermiteEditarPagina(hazoPaginaAtual);
    }
    
}

function loadToolbarOffline(){
    jQuery(".toolbar-online").hide();
    jQuery(".botao-login").show();
    jQuery(".usuario-online").hide();
    jQuery(".usuario-offline").show();
    
    if( typeof hazoPaginaAtual !== 'undefined' ){
        usuarioPermiteEditarPagina(hazoPaginaAtual);
    }
}

function loadToolbarUsuarioStatus(){
    jQuery.ajax({
        url: hazoUrlPath+"/blog/api/get-dados-usuario-online",
        dataType: "JSON",
        data: {},
        type: 'get',
        success: function( data ){
            if( data.status ){
                loadToolbarOnline( data.usuario );
            }else{
                loadToolbarOffline();
            }
        }
    });
}
function usuarioLogout(){
    jQuery.ajax({
        url: hazoUrlPath+"/blog/api/usuario-logoff",
        success: function(){
            loadToolbarOffline();
        }
    });
}

function usuarioLogin( form ){
    jQuery.ajax({
        url: hazoUrlPath+"/blog/api/usuario-login",
        dataType: "JSON",
        data: jQuery(form).serialize(),
        type: 'post',
        success: function(){
            loadToolbarOffline();
        }
    });
}

function usuarioPermiteEditarPagina( paginaID ){    
    jQuery.ajax({
        url: hazoUrlPath+"/blog/api/usuario-permite-editar-pagina/"+paginaID,
        dataType: "JSON",
        data: {},
        type: 'get',
        success: function( data ){
            if( data.status ){
                jQuery(".dv-editar-pagina").attr('href', data.href).css('display', 'inline-block');
                jQuery(".dv-editar-pagina").click(function(){
                    modalArtigoCadastro( jQuery(this).attr('href') );
                });
            }else{
                jQuery(".dv-editar-pagina").attr('href', '').hide();
            }
        },
        error: function( data ){
            console.log('error: ', data);
        }
    });
}

function modalLogin(event){
//    event.preventDefault();
    $.fancybox({
        ajax: {
            dataType : 'html',
            headers  : { 'X-fancyBox': true }
        },
        type : 'ajax',
        href: hazoUrlPath+'/blog/usuario/modal-usuario-login',
        modal: false,
        closeBtn: true
    });
    return false;
}

function modalUsuarioCadastro( acao, redirect ){
//    event.preventDefault();
    $.fancybox({
        ajax: {
            dataType : 'html',
            headers  : { 'X-fancyBox': true }
        },
        type : 'ajax',
        href: hazoUrlPath+'/blog/usuario/modal-usuario-cadastro/'+(acao ? acao : 'cadastro')+"/"+redirect,
        modal: false,
        closeBtn: true,
        padding: 20
    });
    return false;
}

function modalArtigoSelecionaEditoriaCadastro(  ){
//    event.preventDefault();
    jQuery.fancybox({
        ajax: {
            dataType : 'html',
            headers  : { 'X-fancyBox': true }
        },
        type : 'ajax',
        href: hazoUrlPath+'/editor/modal-artigo/selecionar-editoria',
        modal: false,
        closeBtn: true,
        padding: 20
    });
    return false;
}

function modalArtigoCadastro( hrefCadastro ){
//    event.preventDefault();
    jQuery.fancybox({
//        ajax: {
//            dataType : 'html',
//            headers  : { 'X-fancyBox': true }
//        },
        type : 'ajax',
        href: hrefCadastro,
//        modal: true,
//        closeBtn: true,
        padding: 20
    });
    return false;
}

jQuery(document).ready(function(){
    
    loadToolbarUsuarioStatus();
    
    // Hack para z-index de videos do youtube
    jQuery('iframe').each(function(){
        var url = $(this).attr("src");
        jQuery(this).attr("src",url+"?wmode=transparent");
    });
    
});