var formatDatepicker = {
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior'
};

function message(message, type){
    noty({text: message, type: type, timeout: 5000});
}

function loaderHazo( show, message ){
    if( show ){
        message = message ? message : 'Carregando...';
        img = jQuery('<img/>').attr('src', hazoParam.url+'/media/images/load1.gif');
        msg = jQuery('<div/>')
                .append(img)
                .append(' &nbsp; '+message);
        if( !jQuery('#loader').length ){
            loader = jQuery('<div/>')
                    .attr('id', 'loader');
            jQuery('.container.content-all').prepend(loader);
        }
        jQuery(".loader-hide").each(function(){
            jQuery(this).data('old_text', jQuery(this).html());
            jQuery(this).data('old_value', jQuery(this).val());
            jQuery(this).html('Aguarde..').val('Aguarde..').attr('disabled', true);
        });
        jQuery('#loader').html(msg).show();
    }else{
        jQuery(".loader-hide").each(function(){
            jQuery(this).html( jQuery(this).data('old_text') );
            jQuery(this).val( jQuery(this).data('old_value') );
            jQuery(this).attr('disabled', false);
        });
        jQuery('#loader').hide();
    }
}

jQuery.fn.formAjax = function( jsonParam ){
    $(this).each(function(){
        $(this).submit(function(e){
            e.preventDefault();
            
            if( !jsonParam.url ){
                jsonParam.url = $(this).attr('action');
            }
            
            if( !jsonParam.type ){
                jsonParam.type = $(this).attr('method');
            }
            
            if( !jsonParam.dataType ){
                jsonParam.dataType = "JSON";
            }
            
            jsonParam.data = $(this).serialize();
            
            
            jsonParam.beforeSend = function() {
                loaderHazo(true, 'Salvando informações...');
            },
            jsonParam.complete = function(){
                loaderHazo(false);
            },
            
            
            jQuery.ajax( jsonParam );
        });
    });
};

jQuery().ready(function(){
    jQuery(".data").mask('99/99/9999');
});