function lgLoad(status, msg){
    if( status ){
        $('.btnsub').val("Aguarde...");
    }else{
        $('.error').text(msg).show();
        $('.btnsub').val("Criar sessão");
    }
}

$().ready(function(){
    
    $(".form-horizontal").ajaxForm({
        dataType: 'json',
        beforeSend: function(){
            lgLoad(true);
        },
        success: function( data ){
    
            if( data.status ){
                window.location = hazoParam.url_mode+"/cadastro/cadastro-efetuado-com-sucesso";
            }
            
            if( data.msg ){
                lgLoad(false, data.msg);
            }else{
                lgLoad(false);
            }
            
        },
        error: function(){
            lgLoad(false, "Falha ao cadastrar sala. Por favor, tente mais tarde.");
        }
    });
    
    $(".logoff").bind('click', function(e){
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            url: hazoParam.url+"/session/sair",
            success: function(){
                usuLog(false);
            }
        });
    });
    
});