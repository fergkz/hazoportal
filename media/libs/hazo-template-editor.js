// Verifica e adiciona o placeholder nos campos
jQuery.fn.hazoTemplateEditorAddPlaceholder = function(){
    $(this).each(function(){
        html = $.trim( $(this).find('[contenteditable]').html().replace(/<.*?>/g, '') );
        if( !html || html === '' ){
            $(this).find('[contenteditable]').css('position','absolute');
            $(this).data('place-span').show();
        }
    });
};

// Executa o editor
jQuery.fn.hazoTemplateEditor = function( jsonParam ){
    $(this).each(function(){
        
        content = $(this).html();
        
        placeholder = $("<span/>").addClass('placeholder')
                .html(jsonParam.placeholder)
                .hide();
        $(placeholder).on('click', function(){
            $(this).parent().find('[contenteditable]').focus();
        });
        $(this).html(placeholder);
        $(this).data('place-span', placeholder);
        
        textarea = $("<textarea/>");
        if( jsonParam.name ){
            $(textarea).attr('name', jsonParam.name);
        }

        $(textarea).text(content);
        $(this).append(textarea);

        $(this).find('textarea').each(function(){
            switch ( jsonParam.mode ){
                default:
                case 'simple':
                    editor = CKEDITOR.inline( this, {
                        toolbar: 'page_small',
                        height: '100px',
                        enterMode : CKEDITOR.ENTER_P,
//                        autoParagraph: false
            //            enterMode : 2
            //            shiftEnterMode : CKEDITOR.ENTER_BR
                    });
                    break;
                case 'no-html-line':
                    editor = CKEDITOR.inline( this, {
//                        toolbar: 'page_small',
                        toolbar: [ { name: 'basicstyles', items : ['Save','RemoveFormat'] } ],
                        indent: false,
//                        enterMode : CKEDITOR.ENTER_BR,
                        autoParagraph: false,
//                        disableNativeSpellChecker: false,
                        forcePasteAsPlainText: true,
//                        fullPage: false,
//                        removeFormatTags: 'br,p,b,big,code,del,dfn,em,font,i,ins,kbd',
                        allowedContent: jsonParam.allowedContent ? jsonParam.allowedContent : ['']
                    });
                    editor.on( 'key', function( event ) {
                        if ( event.data.keyCode === 13 ) {
                            event.cancel();
                        }
                    });
                    break;
                case 'line':
                    editor = CKEDITOR.inline( this, {
                        enterMode : CKEDITOR.ENTER_BR,
                        autoParagraph: false
                    });
                    break;
                case 'editor':
                case 'full':
                    editor = CKEDITOR.inline( this, {
                        toolbar: 'full',
                        autoParagraph: false
                    });
                    break;
            }
            
            if( jsonParam.config ){
                $.each(jsonParam.config, function(ind, val) {
                    editor.config[ind] = val;
                });
            }
            
        });
        
        $(this).focusin(function(){
            $(this).find('[contenteditable]').css('position','relative');
            $(this).data('place-span').hide();
        });
        $(this).focusout(function(){
            $(this).hazoTemplateEditorAddPlaceholder();
        });
        $(this).hazoTemplateEditorAddPlaceholder();
        
    });
    
};