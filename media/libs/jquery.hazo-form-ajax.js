jQuery.fn.hazoFormAjax = function( jsonParam ){
    $(this).each(function(){
        $(this).submit(function(e){
            e.preventDefault();
            
            if( !jsonParam.url ){
                jsonParam.url = $(this).attr('action');
            }
            
            if( !jsonParam.type ){
                jsonParam.type = $(this).attr('method');
            }
            
            if( !jsonParam.dataType ){
                jsonParam.dataType = "JSON";
            }
            
            jsonParam.data = $(this).serialize();
            
            jQuery.ajax( jsonParam );
        });
    });
};