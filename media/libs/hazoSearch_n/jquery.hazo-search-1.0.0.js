jQuery.fn.hazoSearch = function(jsonParam){
    jHazo = jQuery.noConflict();
    return this.each(function(){
                
        if( jsonParam && jsonParam.start !== undefined && jsonParam.end !== undefined ){
            limit = Number(jsonParam.end) - Number(jsonParam.start);
            limitParam = jsonParam.start+','+jsonParam.end;
        }else if( jsonParam && jsonParam.end !== undefined ){
            limitParam = '0,'+jsonParam.end;
            limit = Number(jsonParam.end);
        }else{
            limit = -1;
        }
        
        if( jHazo(this).attr('data-url') !== undefined && jHazo(this).attr('data-url') ){
            jHazo(this).data("url", jHazo(this).attr('data-url'));
        }else if( jsonParam && jsonParam.url !== undefined && jsonParam.url ){
            jHazo(this).data("url", jsonParam.url);
        }else{
            return;
        }
        if( limit >= 0 ){
            jHazo(this).data('url', jHazo(this).data('url') + "?limit=" + limitParam );
        }
        
        if( jHazo(this).val() !== undefined && jHazo(this).val() ){
            jHazo(this).data("id-val", jHazo(this).val());
        }else if( jsonParam && jsonParam.value !== undefined && jsonParam.value ){
            jHazo(this).data("id-val",jsonParam.value);
        }else{
            jHazo(this).data("id-val","");
        }
        
        jHazo(this).autocomplete({
            minLength: 0,
            source: jHazo(this).data("url"),
            autoFocus: false,
            scroll: true,
            create: function(){
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                text = '';
                for( var i=0; i < 40; i++ ){
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                    
                hid = jHazo("<input/>").attr("type","hidden")
                    .attr("name",jHazo(this).attr('name'))
                    .attr("id", text )
                    .val(jHazo(this).val());
                
                jHazo(this).data("real", hid );
                jHazo(this).after(hid);
                                
                jHazo(this).attr('name','')
                .attr('id-search-reference', jHazo(this).data("real").attr('id'))
                .after(jHazo(this).data("real"));
                
                if( jHazo(this).data("id-val") ){
                    jHazo(this).addClass("hazo-search-load");
                    thisElem = jHazo(this);
                    jQuery.ajax({
                        url: thisElem.data("url"),
                        data: { "id": thisElem.data("id-val") },
                        dataType: 'json',
                        success: function(data) {
                            thisElem.data("real").val(data[0].id);
                            thisElem.val(data[0].label);
                        },
                        async: false
                    });   
                }
                jHazo(this).removeClass("hazo-search-load").addClass("hazo-search");

            },
            search: function( event, ui ){
                jHazo(this).removeClass("hazo-search").addClass("hazo-search-load");
            },
            open: function( event, ui ){
                jHazo(this).removeClass("hazo-search-load").addClass("hazo-search");
            },
            select: function( event, ui ) {
                jHazo(this).data("real").val('');
                jHazo(this).val('');
                if( ui.item.id ){
                    jHazo(this).data("real").val(ui.item.id);
                    jHazo(this).val(ui.item.label);
                }
            },
            response: function( event, ui ) {
                if( limit >= 0 && ui.content.length >= limit ){
                    ui.content.push(
                    {
                        id:null, 
                        label:"........... Para mais resultados, refine a pesquisa"
                    }
                    );
                }
                if( ui.content.length == 0 ){
                    ui.content.push(
                    {
                        id:null, 
                        label:"Nenhum resultado encontrado"
                    }
                    );
                }
            }
        }).click(function(){
            jHazo(this).autocomplete("search", "%");
        }).blur( function(event) {
            jHazo(this).removeClass("hazo-search-load").addClass("hazo-search");
        }).change( function(event) {
            jHazo(this).attr("data-before-value", "");
            jHazo(this).removeClass("hazo-search-load").addClass("hazo-search");
                
            var autocomplete = jHazo(this).data("autocomplete");
            //if there was a match trigger the select event on that match
            if( autocomplete === undefined || !autocomplete.selectedItem ){
                jHazo(this).val('');
                jHazo(this).data("real").val('');
            }
        });
        return jHazo(this);
    });
};