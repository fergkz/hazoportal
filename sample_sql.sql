-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.5.34-0ubuntu0.13.04.1 - (Ubuntu)
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para crepz631_hazo_portal
DROP DATABASE IF EXISTS `crepz631_hazo_portal`;
CREATE DATABASE IF NOT EXISTS `crepz631_hazo_portal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crepz631_hazo_portal`;


-- Copiando estrutura para tabela crepz631_hazo_portal.est_almoxarifado
DROP TABLE IF EXISTS `est_almoxarifado`;
CREATE TABLE IF NOT EXISTS `est_almoxarifado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.est_item
DROP TABLE IF EXISTS `est_item`;
CREATE TABLE IF NOT EXISTS `est_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto_id` int(11) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'L',
  `almoxarifado_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_est_item_est_produto1_idx` (`produto_id`),
  KEY `fk_est_item_est_almoxarifado1_idx` (`almoxarifado_id`),
  CONSTRAINT `fk_est_item_est_almoxarifado1` FOREIGN KEY (`almoxarifado_id`) REFERENCES `est_almoxarifado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_est_item_est_produto1` FOREIGN KEY (`produto_id`) REFERENCES `est_produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.est_produto
DROP TABLE IF EXISTS `est_produto`;
CREATE TABLE IF NOT EXISTS `est_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` longtext,
  `produto_pai_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  KEY `fk_est_produto_est_produto1_idx` (`produto_pai_id`),
  CONSTRAINT `fk_est_produto_est_produto1` FOREIGN KEY (`produto_pai_id`) REFERENCES `est_produto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_group
DROP TABLE IF EXISTS `hazo_group`;
CREATE TABLE IF NOT EXISTS `hazo_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `status` char(1) NOT NULL,
  `default_page` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_menu
DROP TABLE IF EXISTS `hazo_menu`;
CREATE TABLE IF NOT EXISTS `hazo_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mode` varchar(50) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `submodule` varchar(100) DEFAULT NULL,
  `description` varchar(25) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `description_route` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_menu_hazo_group` (`group_id`),
  KEY `FK_hazo_menu_hazo_menu` (`parent`),
  CONSTRAINT `FK_hazo_menu_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`),
  CONSTRAINT `FK_hazo_menu_hazo_menu` FOREIGN KEY (`parent`) REFERENCES `hazo_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_permission
DROP TABLE IF EXISTS `hazo_permission`;
CREATE TABLE IF NOT EXISTS `hazo_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mode` varchar(50) NOT NULL,
  `module` varchar(100) NOT NULL,
  `submodule` varchar(100) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_permission_hazo_group` (`group_id`),
  CONSTRAINT `FK_hazo_permission_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_user
DROP TABLE IF EXISTS `hazo_user`;
CREATE TABLE IF NOT EXISTS `hazo_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` char(1) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `date_last_access` datetime DEFAULT NULL,
  `activation_code` varchar(2000) DEFAULT NULL,
  `image_name` varchar(200) DEFAULT NULL,
  `image_content` longblob,
  `image_size` varchar(200) DEFAULT NULL,
  `image_type` varchar(200) DEFAULT NULL,
  `image_filename` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_user_hazo_group` (`group_id`),
  CONSTRAINT `FK_hazo_user_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.mtg_carta
DROP TABLE IF EXISTS `mtg_carta`;
CREATE TABLE IF NOT EXISTS `mtg_carta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `name_pt` varchar(250) NOT NULL,
  `ind` int(11) NOT NULL,
  `cost` varchar(45) DEFAULT NULL,
  `pow` varchar(45) DEFAULT NULL,
  `tgh` varchar(45) DEFAULT NULL,
  `img_url` varchar(250) DEFAULT NULL,
  `img_name` varchar(250) DEFAULT NULL,
  `img_ext` varchar(45) DEFAULT NULL,
  `img_content` longblob,
  `type` varchar(250) DEFAULT NULL,
  `type_pt` varchar(2000) DEFAULT NULL,
  `descr` longtext,
  `descr_pt` longtext,
  `serie_img_url` varchar(250) DEFAULT NULL,
  `serie_img_name` varchar(250) DEFAULT NULL,
  `serie_img_ext` varchar(45) DEFAULT NULL,
  `serie_img_content` longblob,
  `serie_title` varchar(250) DEFAULT NULL,
  `serie_title_pt` varchar(250) DEFAULT NULL,
  `raridade` varchar(250) DEFAULT NULL,
  `raridade_pt` varchar(250) DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_autor
DROP TABLE IF EXISTS `pag_autor`;
CREATE TABLE IF NOT EXISTS `pag_autor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `pessoa_id` int(10) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `pessoa_id_UNIQUE` (`pessoa_id`),
  KEY `fk_pag_autor_hazo_user1_idx` (`user_id`),
  KEY `fk_pag_autor_pes_pessoa1_idx` (`pessoa_id`),
  CONSTRAINT `fk_pag_autor_hazo_user1` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_autor_pes_pessoa1` FOREIGN KEY (`pessoa_id`) REFERENCES `pes_pessoa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_autor_editoria
DROP TABLE IF EXISTS `pag_autor_editoria`;
CREATE TABLE IF NOT EXISTS `pag_autor_editoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `editoria_id` int(11) NOT NULL,
  `autor_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pag_autor_editoria_pag_editoria1_idx` (`editoria_id`),
  KEY `fk_pag_autor_editoria_pag_autor1_idx` (`autor_id`),
  CONSTRAINT `fk_pag_autor_editoria_pag_autor1` FOREIGN KEY (`autor_id`) REFERENCES `pag_autor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_autor_editoria_pag_editoria1` FOREIGN KEY (`editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_comentario
DROP TABLE IF EXISTS `pag_comentario`;
CREATE TABLE IF NOT EXISTS `pag_comentario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagina_id` int(10) unsigned NOT NULL,
  `user_id` int(10) NOT NULL,
  `data` datetime NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'G',
  `texto` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pag_comentario_hazo_user1_idx` (`user_id`),
  KEY `fk_pag_comentario_pag_pagina1_idx` (`pagina_id`),
  CONSTRAINT `fk_pag_comentario_hazo_user1` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_comentario_pag_pagina1` FOREIGN KEY (`pagina_id`) REFERENCES `pag_pagina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_editoria
DROP TABLE IF EXISTS `pag_editoria`;
CREATE TABLE IF NOT EXISTS `pag_editoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'R',
  `parent_editoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_pag_editoria_parent` (`parent_editoria_id`),
  CONSTRAINT `fk_pag_editoria_pag_editoria1` FOREIGN KEY (`parent_editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_pagina
DROP TABLE IF EXISTS `pag_pagina`;
CREATE TABLE IF NOT EXISTS `pag_pagina` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `pub_data_inicio` datetime NOT NULL,
  `pub_data_final` datetime DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'R',
  `cad_data` datetime NOT NULL,
  `cad_user_id` int(10) NOT NULL,
  `mod_data` datetime DEFAULT NULL,
  `mod_user_id` int(10) DEFAULT NULL,
  `parent_pagina_id` int(10) unsigned DEFAULT NULL,
  `editoria_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `conteudo_json` longtext,
  `contador` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_pag_pagina_slug` (`slug`),
  UNIQUE KEY `uk_pag_pagina_titulo` (`titulo`),
  KEY `fk_pag_pagina_hazo_user1_idx` (`cad_user_id`),
  KEY `fk_pag_pagina_hazo_user2_idx` (`mod_user_id`),
  KEY `fk_pag_pagina_pag_pagina1_idx` (`parent_pagina_id`),
  KEY `fk_pag_pagina_pag_editoria1_idx` (`editoria_id`),
  KEY `fk_pag_pagina_pag_template1_idx` (`template_id`),
  CONSTRAINT `fk_pag_pagina_hazo_user1` FOREIGN KEY (`cad_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_hazo_user2` FOREIGN KEY (`mod_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_editoria1` FOREIGN KEY (`editoria_id`) REFERENCES `pag_editoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_pagina1` FOREIGN KEY (`parent_pagina_id`) REFERENCES `pag_pagina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pag_pagina_pag_template1` FOREIGN KEY (`template_id`) REFERENCES `pag_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_pagina_bloco
DROP TABLE IF EXISTS `pag_pagina_bloco`;
CREATE TABLE IF NOT EXISTS `pag_pagina_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequence` int(11) DEFAULT NULL,
  `pagina_id` int(10) unsigned NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `titulo` varchar(150) NOT NULL,
  `conteudo_json` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pag_pagina_bloco_pag_pagina1_idx` (`pagina_id`),
  CONSTRAINT `fk_pag_pagina_bloco_pag_pagina1` FOREIGN KEY (`pagina_id`) REFERENCES `pag_pagina` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pag_template
DROP TABLE IF EXISTS `pag_template`;
CREATE TABLE IF NOT EXISTS `pag_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `descricao` longtext,
  `cad_data` datetime NOT NULL,
  `cad_user_id` int(10) NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `file_content` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_pag_template_hazo_user1_idx` (`cad_user_id`),
  CONSTRAINT `fk_pag_template_hazo_user1` FOREIGN KEY (`cad_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_cliente
DROP TABLE IF EXISTS `pes_cliente`;
CREATE TABLE IF NOT EXISTS `pes_cliente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pessoa_id` int(10) NOT NULL,
  `status` char(1) NOT NULL,
  `gerente_user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pes_cliente_pes_pessoa` (`pessoa_id`),
  KEY `fk_pes_cliente_hazo_user1_idx` (`gerente_user_id`),
  CONSTRAINT `fk_pes_cliente_hazo_user1` FOREIGN KEY (`gerente_user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_pes_cliente_pes_pessoa` FOREIGN KEY (`pessoa_id`) REFERENCES `pes_pessoa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_cliente_usuario
DROP TABLE IF EXISTS `pes_cliente_usuario`;
CREATE TABLE IF NOT EXISTS `pes_cliente_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pes_cliente_usuario_pes_cliente1_idx` (`cliente_id`),
  KEY `fk_pes_cliente_usuario_hazo_user1_idx` (`user_id`),
  CONSTRAINT `fk_pes_cliente_usuario_hazo_user1` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pes_cliente_usuario_pes_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `pes_cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_pessoa
DROP TABLE IF EXISTS `pes_pessoa`;
CREATE TABLE IF NOT EXISTS `pes_pessoa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `razao` varchar(250) NOT NULL,
  `fantasia` varchar(250) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `cnpj` varchar(14) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `data_nascimento` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_pessoa_email
DROP TABLE IF EXISTS `pes_pessoa_email`;
CREATE TABLE IF NOT EXISTS `pes_pessoa_email` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL,
  `principal` char(1) DEFAULT NULL,
  `pessoa_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_pessoa_endereco
DROP TABLE IF EXISTS `pes_pessoa_endereco`;
CREATE TABLE IF NOT EXISTS `pes_pessoa_endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logradouro` varchar(500) DEFAULT NULL,
  `numero` varchar(45) DEFAULT NULL,
  `cep` varchar(8) DEFAULT NULL,
  `bairro` varchar(500) DEFAULT NULL,
  `principal` varchar(45) DEFAULT NULL,
  `pessoa_id` int(10) NOT NULL,
  `cidade_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.pes_pessoa_telefone
DROP TABLE IF EXISTS `pes_pessoa_telefone`;
CREATE TABLE IF NOT EXISTS `pes_pessoa_telefone` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `telefone` varchar(30) NOT NULL,
  `principal` char(1) DEFAULT NULL,
  `pessoa_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_anexo
DROP TABLE IF EXISTS `proj_anexo`;
CREATE TABLE IF NOT EXISTS `proj_anexo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slug` varchar(1000) DEFAULT NULL,
  `atividade_id` int(10) DEFAULT NULL,
  `demanda_id` int(10) DEFAULT NULL,
  `atividade_operacao_id` int(10) DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `descricao` longtext,
  `file_name` varchar(250) NOT NULL,
  `file_type` varchar(50) NOT NULL,
  `file_size` varchar(50) NOT NULL,
  `file_extension` varchar(50) NOT NULL,
  `file_content` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_anexo_proj_atividade` (`atividade_id`),
  KEY `FK_proj_anexo_proj_demanda` (`demanda_id`),
  KEY `FK_proj_anexo_proj_atividade_operacao` (`atividade_operacao_id`),
  CONSTRAINT `FK_proj_anexo_proj_atividade` FOREIGN KEY (`atividade_id`) REFERENCES `proj_atividade` (`id`),
  CONSTRAINT `FK_proj_anexo_proj_atividade_operacao` FOREIGN KEY (`atividade_operacao_id`) REFERENCES `proj_atividade_operacao` (`id`),
  CONSTRAINT `FK_proj_anexo_proj_demanda` FOREIGN KEY (`demanda_id`) REFERENCES `proj_demanda` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade
DROP TABLE IF EXISTS `proj_atividade`;
CREATE TABLE IF NOT EXISTS `proj_atividade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `demanda_id` int(10) DEFAULT NULL,
  `parent_atividade_id` int(10) DEFAULT NULL,
  `titulo` varchar(250) NOT NULL,
  `descricao` longtext,
  `duracao_estimada` int(10) NOT NULL,
  `etapa_id` char(1) NOT NULL,
  `prioridade` int(10) NOT NULL,
  `cad_user_id` int(10) DEFAULT NULL,
  `cliente_id` int(11) DEFAULT NULL,
  `data_prev_inicio` datetime DEFAULT NULL,
  `data_prev_final` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_proj_demanda` (`demanda_id`),
  KEY `FK_proj_atividade_proj_atividade` (`parent_atividade_id`),
  KEY `FK_proj_atividade_proj_atividade_situacao` (`etapa_id`),
  KEY `FK_proj_atividade_pes_cliente` (`cliente_id`),
  CONSTRAINT `FK_proj_atividade_pes_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `pes_cliente` (`id`),
  CONSTRAINT `FK_proj_atividade_proj_atividade` FOREIGN KEY (`parent_atividade_id`) REFERENCES `proj_atividade` (`id`),
  CONSTRAINT `FK_proj_atividade_proj_atividade_etapa` FOREIGN KEY (`etapa_id`) REFERENCES `proj_atividade_etapa` (`id`),
  CONSTRAINT `FK_proj_atividade_proj_demanda` FOREIGN KEY (`demanda_id`) REFERENCES `proj_demanda` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade_etapa
DROP TABLE IF EXISTS `proj_atividade_etapa`;
CREATE TABLE IF NOT EXISTS `proj_atividade_etapa` (
  `id` char(1) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `proxima_etapa` char(1) DEFAULT NULL,
  `permite_selecionar` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_situacao_proj_atividade_situacao` (`proxima_etapa`),
  CONSTRAINT `FK_proj_atividade_situacao_proj_atividade_situacao` FOREIGN KEY (`proxima_etapa`) REFERENCES `proj_atividade_etapa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade_grupo
DROP TABLE IF EXISTS `proj_atividade_grupo`;
CREATE TABLE IF NOT EXISTS `proj_atividade_grupo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `atividade_id` int(10) NOT NULL,
  `group_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `execucao` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_grupo_proj_atividade` (`atividade_id`),
  KEY `FK_proj_atividade_grupo_hazo_group` (`group_id`),
  KEY `FK_proj_atividade_grupo_hazo_user` (`user_id`),
  CONSTRAINT `FK_proj_atividade_grupo_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`),
  CONSTRAINT `FK_proj_atividade_grupo_hazo_user` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`),
  CONSTRAINT `FK_proj_atividade_grupo_proj_atividade` FOREIGN KEY (`atividade_id`) REFERENCES `proj_atividade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade_operacao
DROP TABLE IF EXISTS `proj_atividade_operacao`;
CREATE TABLE IF NOT EXISTS `proj_atividade_operacao` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `atividade_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `data_inicio` datetime NOT NULL,
  `data_final` datetime DEFAULT NULL,
  `descricao` longtext,
  `etapa_id` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_operacao_proj_atividade` (`atividade_id`),
  KEY `FK_proj_atividade_operacao_hazo_user` (`user_id`),
  KEY `FK_proj_atividade_operacao_proj_atividade_etapa` (`etapa_id`),
  CONSTRAINT `FK_proj_atividade_operacao_hazo_user` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`),
  CONSTRAINT `FK_proj_atividade_operacao_proj_atividade` FOREIGN KEY (`atividade_id`) REFERENCES `proj_atividade` (`id`),
  CONSTRAINT `FK_proj_atividade_operacao_proj_atividade_etapa` FOREIGN KEY (`etapa_id`) REFERENCES `proj_atividade_etapa` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade_padroes
DROP TABLE IF EXISTS `proj_atividade_padroes`;
CREATE TABLE IF NOT EXISTS `proj_atividade_padroes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_padroes_hazo_user` (`user_id`),
  CONSTRAINT `FK_proj_atividade_padroes_hazo_user` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_atividade_requerida
DROP TABLE IF EXISTS `proj_atividade_requerida`;
CREATE TABLE IF NOT EXISTS `proj_atividade_requerida` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `atividade_requerida_id` int(10) NOT NULL,
  `atividade_dependente_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_requerida_proj_atividade` (`atividade_requerida_id`),
  KEY `FK_proj_atividade_requerida_proj_atividade_2` (`atividade_dependente_id`),
  CONSTRAINT `FK_proj_atividade_requerida_proj_atividade` FOREIGN KEY (`atividade_requerida_id`) REFERENCES `proj_atividade` (`id`),
  CONSTRAINT `FK_proj_atividade_requerida_proj_atividade_2` FOREIGN KEY (`atividade_dependente_id`) REFERENCES `proj_atividade` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_demanda
DROP TABLE IF EXISTS `proj_demanda`;
CREATE TABLE IF NOT EXISTS `proj_demanda` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descricao` longtext,
  `status` char(1) NOT NULL,
  `cad_data` datetime NOT NULL,
  `cad_user_id` int(10) NOT NULL,
  `prioridade` int(10) NOT NULL,
  `tipo_demanda_id` char(1) NOT NULL,
  `cliente_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_demanda_hazo_user` (`cad_user_id`),
  KEY `FK_proj_demanda_proj_tipo_demanda` (`tipo_demanda_id`),
  KEY `fk_proj_demanda_pes_cliente1_idx` (`cliente_id`),
  CONSTRAINT `FK_proj_demanda_hazo_user` FOREIGN KEY (`cad_user_id`) REFERENCES `hazo_user` (`id`),
  CONSTRAINT `fk_proj_demanda_pes_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `pes_cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_proj_demanda_proj_tipo_demanda` FOREIGN KEY (`tipo_demanda_id`) REFERENCES `proj_tipo_demanda` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_demanda_gerente
DROP TABLE IF EXISTS `proj_demanda_gerente`;
CREATE TABLE IF NOT EXISTS `proj_demanda_gerente` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `demanda_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_demanda_gerente_proj_demanda` (`demanda_id`),
  KEY `FK_proj_demanda_gerente_hazo_user` (`user_id`),
  CONSTRAINT `FK_proj_demanda_gerente_hazo_user` FOREIGN KEY (`user_id`) REFERENCES `hazo_user` (`id`),
  CONSTRAINT `FK_proj_demanda_gerente_proj_demanda` FOREIGN KEY (`demanda_id`) REFERENCES `proj_demanda` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.proj_tipo_demanda
DROP TABLE IF EXISTS `proj_tipo_demanda`;
CREATE TABLE IF NOT EXISTS `proj_tipo_demanda` (
  `id` char(1) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` longtext,
  `permite_cliente_selecionar` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
